#include "symtable.h"

#define HASH_MULTIPLIER 65599
#define HASH_SIZE 1500

extern int inFunction;
extern int global_offset;
extern stack_Head offset_Stack;
extern char* last_func_name;
extern SymTable_T* symbol_table;
extern int global_counter;


struct SymTable_T* table;

static char* sym_type(enum SymbolType type){ 
    switch(type){
    case GLOBAL:
        return "global variable";
    case LOCAL:
        return "local variable";
    case FORMAL:
        return "formal argument";
    case USERFUNC:
        return "user function";
    case LIBFUNC:
        return "library function";
    default:
        return "NULL";
    }
return "NULL";
}


static unsigned int SymTable_hash(const char *pcKey)
{
  size_t ui;
  unsigned int uiHash = 0U;
  for (ui = 0U; pcKey[ui] != '\0'; ui++)
    uiHash = uiHash * HASH_MULTIPLIER + pcKey[ui];
  return uiHash % HASH_SIZE;
}

SymTable_T* SymTable_new(void){
	
	int i;
	table = malloc(sizeof(struct SymTable_T)*HASH_SIZE);
	assert(table);
    for(i = 0; i < HASH_SIZE-1; i++){
		table[i].head=NULL;
    }
	
	return &table;
}

int SymTable_ListContains(Binding* node, const char *pcKey){
	Binding *t;
	for(t =node; t != NULL; t = t->next){
		if(t->isFunction){
			if(strcmp(t->value.funcVal->name ,pcKey) == 0) return 1;
		}
		else{
			if(strcmp(t->value.varVal->name ,pcKey) == 0) return 1;
		}
	}
	return 0;
}

int SymTable_ListPut(ScopeList_Node** scopelist_head, struct SymTable_T* oSymTable, const char *pcKey, int isFunction, int scope, enum SymbolType type, int line, int isArg){
	Binding* t;
	Binding *f;
    //if(SymTable_ListContains((*oSymTable)->head,pcKey)) return 0;
    t = malloc(sizeof(Binding));
	t->scope=scope;
    t->type=type;
	if(type==USERFUNC||t->type==LIBFUNC)t->isFunction=1;
	else t->isFunction=0;
	t->isActive=1;
	t->nextInScope=NULL;
	t->isFunction=isFunction;
	if(isFunction){
		t->value.funcVal=malloc(sizeof(Function));
		t->value.funcVal->name = malloc(sizeof(char) * (strlen(pcKey)+1));
    	strcpy(t->value.funcVal->name, pcKey);
		t->value.funcVal->scope=scope;
		t->value.funcVal->line=line;
		t->value.funcVal->local_vars=0;
	}
	else{
		t->value.funcVal=malloc(sizeof(Variable));
		t->value.varVal->name = malloc(sizeof(char) * (strlen(pcKey)+1));
    	strcpy(t->value.varVal->name, pcKey);
		t->value.varVal->scope=scope;
		t->value.varVal->line=line;
		t->value.varVal->offset=global_offset++;
		if(inFunction){
			t->space=functionlocal;
			t->type=LOCAL;
			f=lookup(*symbol_table, last_func_name, scope, "variable");
			assert(f);
		 	f->value.funcVal->local_vars++;		
		}
		else{
			if(isArg==1){
				t->space=formalarg;
			}
			else{
				t->space=programvar;
			}
		}
	}
	
	Insert_New_Node(scopelist_head,t,scope);

    if(oSymTable-> head!=NULL){
    	 t -> next = oSymTable -> head;
    }else{
    	t->next=NULL;
    }
    oSymTable-> head  = t;
    return 1;
}


int SymTable_put(ScopeList_Node** scopelist_head, struct SymTable_T* oSymTable, const char *pcKey, int scope, enum SymbolType type, int line, int isFuntion, int isArg){
	unsigned int hash;
	assert(oSymTable && pcKey);
	//if(SymTable_contains(oSymTable, pcKey)) return 0;
	hash = SymTable_hash(pcKey);
	return SymTable_ListPut(scopelist_head, oSymTable+hash, pcKey, isFuntion, scope, type, line, isArg);
}

int SymTable_ListRemove(SymTable_T* oSymTable, const char *pcKey){
	Binding *t;
	Binding *p;
	t = (*oSymTable)->head;
	if(t->isFunction){
		if(strcmp(t->value.funcVal->name,pcKey) == 0){
			(*oSymTable) -> head = t -> next;
			free(t);
			return 1;
		}
	}
	else{
		if(strcmp(t->value.varVal->name,pcKey) == 0){
			(*oSymTable) -> head = t -> next;
			free(t);
			return 1;
		}
	}
	p = (*oSymTable)->head;
    for(; t!=NULL; t=t->next){
    	if(p->isFunction){
			if(strcmp(t->value.funcVal->name,pcKey) == 0){
				p->next = t->next;
				free(t);
				return 1;
			}
		}
		else{
			if(strcmp(t->value.varVal->name,pcKey) == 0){
				p->next = t->next;
				free(t);
				return 1;
			}
		}
    	p = t;
    }
    return 0;
}


int SymTable_remove(SymTable_T* oSymTable, const char *pcKey){
	unsigned int hash;
	assert(oSymTable && pcKey);
	hash = SymTable_hash(pcKey);
	return SymTable_ListRemove(oSymTable+hash,pcKey);
}


int SymTable_contains(SymTable_T* oSymTable, const char *pcKey){
	unsigned int hash;
	assert(oSymTable && pcKey);
	hash = SymTable_hash(pcKey);
	return SymTable_ListContains(oSymTable[hash]->head,pcKey);
}



char* name_get(Binding *t){
	
	if(t->isFunction){
		return t->value.funcVal->name;
	}
	else{
		return t->value.varVal->name;
	}
}

int scope_get(Binding *t){
	if(t->isFunction){
		return t->value.funcVal->scope;
	}
	else{
		return t->value.varVal->scope;
	}
}

Binding *SymTable_ListFind(SymTable_T oSymTable, const char *pcKey, int scope, int function){
	Binding *t;
	for(t = oSymTable->head; t!=NULL;t=t->next){
		if((strcmp(name_get(t),pcKey) == 0 )&& (scope_get(t) == scope )&& (t->isActive == 1)) return t;
	}
	for(t = oSymTable->head; t!=NULL;t=t->next){
		if((strcmp(name_get(t),pcKey) == 0 )&& ((scope_get(t) < scope )&& (scope_get(t) != 0) )&& (t->isActive == 1)) return t;
	}
	for(t = oSymTable->head; t!=NULL;t=t->next){
		if((strcmp(name_get(t),pcKey) == 0 )&& ((t->isActive == 1) && (scope_get(t) == 0))) return t;
	}
	return NULL;
}

Binding *SymTable_AllListFind(SymTable_T oSymTable, const char *pcKey, int scope, int function){
	Binding *t;
	for(t = oSymTable->head; t!=NULL;t=t->next){
		if((strcmp(name_get(t),pcKey) == 0 )) return t;
	}
	return NULL;
}

Binding *SymTable_find(SymTable_T oSymTable, const char *pcKey, int scope, char* type){
	unsigned int hash;
	assert(oSymTable && pcKey);
	hash = SymTable_hash(pcKey);
	if(scope < 0){
		return SymTable_AllListFind(oSymTable+hash, pcKey, scope, 1);
	}
	return SymTable_ListFind(oSymTable+hash, pcKey, scope, 1);
}


void hide(ScopeList_Node* head,int scope){
	if(scope>head->scope)return;
	ScopeList_Node* temp=head;
	while (temp->scope!=scope)
	{
		temp=temp->next;
	}
	Binding* temp_binding=temp->head;

	while(temp_binding!=NULL){
		temp_binding->isActive=0;
		temp_binding=temp_binding->nextInScope;
	}
	
}

Binding *lookup(SymTable_T oSymTable, const char *pcKey, int scope, char* type){
	return SymTable_find(oSymTable, pcKey, scope, type);
}

void insert(ScopeList_Node** scopelist_head, SymTable_T oSymTable ,const char *name, int scope, int isFunction, int line, enum SymbolType type, int isArg, char* funcname)
{
	if(isArg==0){
		if(scope==0&&type!=USERFUNC&&type!=LIBFUNC){
			SymTable_put(scopelist_head, oSymTable, name, scope, GLOBAL, line, isFunction, isArg);}
		else{
			SymTable_put(scopelist_head, oSymTable, name, scope, type, line, isFunction, isArg);
		}
		if(scope==0&&(type!=USERFUNC)&&(type!=LIBFUNC))global_counter++;
	}
	else{
		SymTable_put(scopelist_head, oSymTable, name, scope, type, line, isFunction, isArg); 
		unsigned int hash=SymTable_hash(funcname);
		Binding* temp_bind=oSymTable[hash].head;
		while(temp_bind!=NULL){
			if(temp_bind->isFunction){
				if(!strcmp(temp_bind->value.funcVal->name, name)){
					args_list_node* new_arg=malloc(sizeof(args_list_node));
					new_arg->bind=temp_bind;
					if(temp_bind->value.funcVal->head==NULL){
						new_arg->next=NULL;
						temp_bind->value.funcVal->head=new_arg;
					}
					else{
						new_arg->next=temp_bind->value.funcVal->head;
						temp_bind->value.funcVal->head=new_arg;
					}
				}
			}
			temp_bind=temp_bind->next;
		}
	}
}


 void print_table(ScopeList_Node* head){
     ScopeList_Node* temp=head;
     Binding* temp_bind;
     if(temp!=NULL)print_table(temp->next);
     else return;
     temp_bind=temp->head;
     printf("==============   SCOPE %d   ==============\n", temp->scope);
     while(temp_bind!=NULL){
         if(temp_bind->isFunction){
             printf("\"%s\", [%s], (line %d) (scope %d) (Local vars: %d)\n", temp_bind->value.funcVal->name, sym_type(temp_bind->type), temp_bind->value.funcVal->line, temp->scope, temp_bind->value.funcVal->local_vars); 
         }
         else{
             printf("\"%s\", [%s], (line %d) (scope %d) (Offset %d)\n", temp_bind->value.varVal->name, sym_type(temp_bind->type), temp_bind->value.varVal->line, temp->scope, temp_bind->value.varVal->offset); 
         }
         temp_bind=temp_bind->nextInScope;
     } 
}

int isFunction(SymTable_T head, const char* name){
	unsigned int hash = SymTable_hash(name);
	Binding* temp=head[hash].head;
	while (temp!=NULL)
	{
		if(temp->isFunction&&strcmp(temp->value.funcVal->name, name)==0&&temp->isActive==1){
			return 1;
		}
		temp=temp->next;
	}

	return 0;
}