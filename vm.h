#ifndef VM_H_
#define VM_H_
#include "target.h"
#include <math.h>

#define AVM_STACKSIZE 4096
#define AVM_WIPEOUT(m) memset(&(m), 0,sizeof(m))
#define AVM_TABLE_HASHSIZE 211

enum avm_memcell_t{
    number_m,
    string_m,
    bool_m,
    table_m,
    userfunc_m,
    libfunc_m,
    nil_m,
    undef_m
};

typedef struct avm_memcell{
  enum avm_memcell_t type;
  union{
      double numVal;
      char* strVal;
      unsigned int boolVal;
      struct avm_table* tableVal;
      unsigned funcVal;
      char* libfuncval;
      unsigned stackVal;
  }data;
}avm_memcell;

typedef struct avm_table_bucket {
	avm_memcell key;
	avm_memcell value;
	struct avm_table_bucket* next;
}avm_table_bucket;
	
typedef struct avm_table {
	unsigned refCounter;
	avm_table_bucket* strIndexed[AVM_TABLE_HASHSIZE];
	avm_table_bucket* numIndexed[AVM_TABLE_HASHSIZE];
	avm_table_bucket* boolIndexed[AVM_TABLE_HASHSIZE];
	avm_table_bucket* userfuncIndexed[AVM_TABLE_HASHSIZE];
	avm_table_bucket* libfuncIndexed[AVM_TABLE_HASHSIZE];
	unsigned total;
}avm_table;

static void avm_initstack(void);

avm_table* avm_tablenew (void);

void avm_memcellclear(avm_memcell *m);

void avm_tabledestroy(avm_table* t);

void avm_tablebucketsdestroy(avm_table_bucket **p);
/*
avm_memcell* avm_tablegetelem(avm_memcell* key);

void avm_tablesetelem(avm_memcell* key, avm_memcell* value);
*/
void avm_tableincrefcounter(avm_table *t);

void avm_tabledecrefcounter(avm_table *t);

void avm_tablebucketsinit(avm_table_bucket **p);

avm_memcell* avm_tablegetelem(avm_table* table,avm_memcell* index);

void avm_tablesetelem(avm_table* table,avm_memcell* index,avm_memcell* content);

avm_memcell* avm_translate_operand (vmarg* arg, avm_memcell* reg);

typedef unsigned int (*tobool_func_t)(avm_memcell*);

typedef void (*memclear_func_t)(avm_memcell*);

typedef void execute_func_t(instruction*);

void execute_code();

void execute_assign(instruction* i);
void execute_arithmetic(instruction* i);
void execute_uminus(instruction* i);
void execute_nop(instruction* i);
void execute_nop(instruction* i);
void execute_not(instruction* i);
void execute_jeq(instruction* i);
void execute_jne(instruction* i);
void execute_jle(instruction *i);
void execute_jge(instruction*i);
void execute_jlt(instruction *i);
void execute_jgt(instruction* i);
void execute_call(instruction* i);
void execute_pusharg(instruction* i);
void execute_funcenter(instruction *i);
void execute_funcexit(instruction* i);
void execute_newtable(instruction *i);
void execute_tablegetelem(instruction *i);
void execute_tablesetelem(instruction *i);
void execute_nop(instruction *i);
void memclear_string (avm_memcell* m);
void memclear_table(avm_memcell* m);

#endif