/*
    Test file for the final phase of HY-340: Languages & Compilers
    Computer science dpt, University of Crete, Greece

    Expected Output:
    2.000
    zero
    1.000
    1.000
    true
    0.000
    hello
    [ false, true, [ ], 5.000, [ hello, hi ], [ ] ]

*/

nl = "\n";

vec0=[];

vec1=[1, 2, 3];

vec2= [false, true, 1, 5, 3, 2];

vectors = [vec0, vec1, vec2];

vec3=[
    {2: vectors[0]},
    {3: vectors[1]}, 
    {0 : "zero"}, 
    {1 : "one"}
];


print(vec1[1], nl, vec3[0], nl); //2, zero

print(vectors[1][0], nl); //1

result=vec3[3][0];

print(result, nl); // 1

if(vectors[0] == vec0)
    print("true");      //TRUE
else
    print("false");

print(nl);    
    
vectors[0]=0;
result=vectors[0];
print(result, nl); //0

vec3.y[2]=vec2[4][0];
result=vec3[3][2];
print(result, nl); //"hello"   

print(vec2, nl);

