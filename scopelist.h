#ifndef SCOPELIST_H_
#define SCOPELIST_H_
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<string.h>

enum scope_space{
    programvar, 
    functionlocal,
    formalarg
};

enum SymbolType {
GLOBAL, LOCAL, FORMAL,
USERFUNC, LIBFUNC
};

typedef struct args_list_node{
	struct Binding* bind;
	struct args_list_node* next;
}args_list_node;
typedef struct Variable {
char *name;
unsigned int scope;
unsigned int line;
int offset;
} Variable;

typedef struct Function {
char *name;
unsigned int scope;
unsigned int line;
unsigned int local_vars;
args_list_node* head;
} Function;

typedef struct Binding {
	int isActive;
	int isFunction;
	int scope;
	int isArg;
	enum scope_space space;
	union {
		Variable *varVal;
		Function *funcVal;
	} value;
	enum SymbolType type;
	struct Binding * next;
	struct Binding * nextInScope;
} 	Binding;

typedef struct ScopeList_Node{
unsigned int scope;
struct ScopeList_Node *next;
struct Binding *head; 
}ScopeList_Node;


/* Creates and returns a new ScopeList object.
 */
ScopeList_Node* ScopeList_new(void);

Binding* ScopeListLookUp(ScopeList_Node* hScopeList,char* key,int scope);

void Insert_New_Node(ScopeList_Node** hScopeList,Binding* toInsert,int scope);

#endif