#ifndef SYMTABLE_H_
#define SYMTABLE_H_
#include "scopelist.h"
#include "stack.h"
#include <math.h>



/* The abstract data type in which is contained a pointer to a list
 */
struct SymTable_T{
	Binding* head;
}typedef* SymTable_T;

/* Creates and returns a new SymTable_T object.
 */
SymTable_T* SymTable_new(void);

/* Deallocates the memory allocated by the list.
 * It has a checked runtime error in case oSymTable is NULL.
 */
void SymTable_free(SymTable_T* oSymTable);

/* Returns the number of bindings that the list has.
 * It has a checked runtime error in case oSymTable is NULL.
 */
unsigned int SymTable_getLength(SymTable_T oSymTable);

/* Creates a new binding with the key and value that is given, puts it on the list and returns 1.
 * If a binding with the same key already exist on the list then it doesn't change list and
 * it returns 0.
 * It has a checked runtime error in case oSymTable, pcKey or pvValue is NULL.
 */
int SymTable_put(ScopeList_Node** scopelist_head, struct SymTable_T* oSymTable, const char *pcKey, int scope, enum SymbolType type, int line, int isFuntion, int isArg);
/* Deallocates the binding with the given key and returns 1.
 * If a binding with the key isn't found on the list it changes nothing and returns 0.
 * It has a checked runtime error in case oSymTable, pcKey is NULL.
 */
int SymTable_remove(SymTable_T* oSymTable, const char *pcKey);

/* Returns 1 if a binding with the given key is in the list and 0 if it doesn't find a binding
 * with the key.
 * It has a checked runtime error in case oSymTable, pcKey is NULL.
 */
int SymTable_contains(SymTable_T* oSymTable, const char *pcKey);

/* Returns the value pointer stored on the binding with the given key.
 * If a binding with the given key isn't found then it returns NULL.
 * It has a checked runtime error in case oSymTable, pcKey is NULL.
 */
void* SymTable_get(SymTable_T oSymTable, const char *pcKey, int scope, enum SymbolType type);

void hide(ScopeList_Node* head, int scope);

Binding * SymTable_find(SymTable_T oSymTable, const char *pcKey, int scope, char* type);

/*
*
*/
Binding * lookup(SymTable_T oSymTable, const char *pcKey, int scope, char* type);

void insert(ScopeList_Node** scopelist_head, SymTable_T oSymTable ,const char *name, int scope, int isFunction, int line, enum SymbolType type, int isArg, char* funcname);

void assign(SymTable_T oSymTable, const char *pcKey, void* value);

int SymTable_ListPut(ScopeList_Node** scopelist_head, struct SymTable_T* oSymTable, const char *pcKey, int isFunction, int scope, enum SymbolType type, int line, int isArg);

void print_scope(Binding *temp_bind, int scope);

void print_table(ScopeList_Node* head);

int isFunction(SymTable_T head, const char* name);
#endif /* SYMTABLE_H_ */
