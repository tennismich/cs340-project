all: binary vm.out

binary: parser.c scanner.c binary.out


scanner.c: al.l
	flex --outfile=scanner.c al.l
parser.c: al.y
	bison --yacc --defines --output=parser.c al.y 
binary.out: scanner.c 
	gcc scanner.c parser.c symtable.c scopelist.c intermidiate.c numlist.c target.c stack.c -o binary.out -g
vm.out:
	gcc vm.c symtable.c scopelist.c intermidiate.c numlist.c target.c stack.c -o vm.out -ldl -lm -g
	
clean: 
	rm *.out scanner.c parser.c parser.h bin.abc