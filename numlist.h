#ifndef NUMLIST_H_
#define NUMLIST_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

/* The abstract data type in which the key and value of each Num_Binding is store along with
 * a pointer to the next Num_Binding
 */
struct Num_Binding {
	int value;
	int scope;
	struct Num_Binding * next;
} typedef Num_Binding;

/* The abstract data type in which is contained a pointer to a list
 */
struct SymTable_NumT{
	Num_Binding* head;
	Num_Binding* end;
}typedef* SymTable_NumT;

/* Creates and returns a new SymTable_NumT object.
 */
SymTable_NumT SymTable_Num_new(void);

/* Deallocates the memory allocated by the list.
 * It has a checked runtime error in case oSymTable is NULL.
 */
void SymTable_Num_free(SymTable_NumT oSymTable);

/* Returns the number of Num_Bindings that the list has.
 * It has a checked runtime error in case oSymTable is NULL.
 */
unsigned int SymTable_Num_getLength(SymTable_NumT oSymTable);

/* Creates a new Num_Binding with the key and value that is given, puts it on the list and returns 1.
 * If a Num_Binding with the same key already exist on the list then it doesn't change list and
 * it returns 0.
 * It has a checked runtime error in case oSymTable, pcKey or pvValue is NULL.
 */
int SymTable_Num_put(SymTable_NumT oSymTable, const int pvValue, int scope);

/* Merges two SymTable_NumT objects and returns a new combined SymTable_NumT object.
 */
SymTable_NumT merge(SymTable_NumT table1, SymTable_NumT table2);

/* Applies the given function "pfApply" to all of the Num_Binding of the list.
 * It has a checked runtime error in case oSymTable, pcExtra is NULL.
 */
void SymTable_Num_map(SymTable_NumT oSymTable, void (*pfApply)(void *p, unsigned int pvExtra),void *array ,const int pvExtra);



#endif /* SYMTABLE_H_ */
