#include "intermidiate.h"
#define EXPAND_SIZE 1024
#define CURR_SIZE (total * sizeof(quad))
#define NEW_SIZE (EXPAND_SIZE * sizeof(quad) + CURR_SIZE)

extern unsigned int currQuad;
extern unsigned total, t_id;

struct alpha_token_t {
    unsigned int numline;
    unsigned int numToken;
    unsigned int quadline;
    char *content;
    const char *type;
    expr* expression;
    expr* prev_expr;
};

static void insertQuad(quad *array, enum iocode op, expr *result, expr *arg1, expr *arg2, unsigned label, unsigned line)
{
    array->op = op;
    array->result = result;
    array->arg1 = arg1;
    array->arg2 = arg2;
    array->label = label;
    array->line = line;
    array->ij = 0;
}

static quad* expandQuads(quad *array){
    quad *check_array;
    check_array = realloc(array,NEW_SIZE);
    if(check_array){
        array = check_array;
    }
    else{
       exit(-1);
    }
    total += EXPAND_SIZE;
    return array;
}

quad* emit(quad *array, enum iocode op, expr *result, expr *arg1, expr *arg2, unsigned label, unsigned line)
{
    if (currQuad == total)
    {
        array = expandQuads(array);
    }
    insertQuad(array + currQuad, op, result, arg1, arg2, label, line);
    currQuad++;
    return array;
}

void patchlabel(quad* array,unsigned label){
    array->label = label;
}

void printQuads(quad *array, unsigned int size)
{
    int i;
    printf("\nquad# opcode \tresult\t arg1 \targ2 \tlabel\n");
    printf("=============================================\n");
    for (i = 0; i < currQuad; i++)
    {
        if (array[i].arg1 != NULL)
        {
            if (array[i].arg2 == NULL)
            {
                if(array[i].arg1->type==constnum_e||array[i].arg1->type==conststring_e){
                    if (array[i].arg1->strConst != NULL)
                    {
                        printf("%d:   %s\t%s \t \"%s\"\n", i, optostring(array[i].op), array[i].result->sym->value.varVal->name, array[i].arg1->strConst);
                    }
                    else 
                    {
                        printf("%d:   %s\t%s\t%0.3f\n", i, optostring(array[i].op), array[i].result->sym->value.varVal->name, array[i].arg1->numConst);
                    }
                }
                else if(array[i].arg1->type==constbool_e||array[i].arg1->type==nil_e){
                    printf("%d:   %s\t%s \t %s\n", i, optostring(array[i].op), array[i].result->sym->value.varVal->name, array[i].arg1->strConst);
                }
                else{
                        printf("%d:   %s\t%s \t %s\n", i, optostring(array[i].op), array[i].result->sym->value.varVal->name, array[i].arg1->sym->value.varVal->name);
                }
            }
            else
            {
                if(array[i].result != NULL)
                {
                    if(array[i].arg1->type==constnum_e||array[i].arg1->type==conststring_e||array[i].arg1->type==tableitem_e){
                        if (array[i].arg1->strConst != NULL)
                        {
                            printf("%d:   %s\t%s \t \"%s\"\t", i, optostring(array[i].op), array[i].result->sym->value.varVal->name, array[i].arg1->strConst);
                        }
                        else 
                        {
                            printf("%d:   %s\t%s\t%0.3f\t", i, optostring(array[i].op), array[i].result->sym->value.varVal->name, array[i].arg1->numConst);
                        }
                    }
                    else if(array[i].arg1->type==constbool_e||array[i].arg1->type==nil_e){
                        if(array[i].result->sym!=NULL){
                            printf("%d:   %s\t%s \t %s\t", i, optostring(array[i].op), array[i].result->sym->value.varVal->name, array[i].arg1->strConst);
                        }
                        else{
                            printf("%d:   %s\t%s \t %s\t", i, optostring(array[i].op), array[i].result->strConst, array[i].arg1->strConst);
                    
                        }
                    }
                    else{
                            printf("%d:   %s\t%s \t %s\t", i, optostring(array[i].op), array[i].result->sym->value.varVal->name, array[i].arg1->sym->value.varVal->name);
                    }
                    if(array[i].arg2->type==constnum_e||array[i].arg2->type==conststring_e||array[i].arg2->type==tableitem_e){
                        if (array[i].arg2->strConst != NULL)
                        {
                            printf("\"%s\"\n", array[i].arg2->strConst);
                        }
                        else 
                        {
                        printf("%0.3f\n", array[i].arg2->numConst);
                        }
                    }
                    else if(array[i].arg2->type==constbool_e||array[i].arg2->type==nil_e){
                        if(array[i].result->sym!=NULL){
                            printf("%d:   %s\t%s \t %s", i, optostring(array[i].op), array[i].result->sym->value.varVal->name, array[i].arg2->strConst);
                        }
                        else{
                            printf("%d:   %s\t%s \t %s", i, optostring(array[i].op), array[i].result->strConst, array[i].arg2->strConst);

                        }
                    }
                    else{
                            printf("%s\n", array[i].arg2->sym->value.varVal->name);
                            
                    }
                }
                else
                {
                    if((array[i].arg1->type==var_e )&& (array[i].arg2->type==var_e)){
                        printf("%d:   %s  \t\t%s\t%s\t%d\n",i, optostring(array[i].op), array[i].arg1->sym->value.varVal->name, array[i].arg2->sym->value.varVal->name, array[i].label);
                    }
                    else if(((array[i].arg1->type==var_e)&&((array[i].arg2->type==constnum_e)||(array[i].arg2->type==conststring_e)||(array[i].arg2->type==constbool_e)||(array[i].arg2->type==nil_e)))){
                        if(array[i].arg2->type==conststring_e)
                            printf("%d:   %s  \t\t%s\t\"%s\"\t%d\n",i, optostring(array[i].op), array[i].arg1->sym->value.varVal->name, array[i].arg2->strConst, array[i].label);
                        else if(array[i].arg2->type==constnum_e)
                            printf("%d:   %s  \t\t%s\t%0.3f\t%d\n",i, optostring(array[i].op), array[i].arg1->sym->value.varVal->name, array[i].arg2->numConst, array[i].label);

                        else
                            printf("%d:   %s  \t\t%s\t%s\t%d\n",i, optostring(array[i].op), array[i].arg1->sym->value.varVal->name, array[i].arg2->strConst, array[i].label);
                    }
                    else if((array[i].arg2->type==var_e)&&((array[i].arg1->type==constbool_e)||(array[i].arg1->type==constnum_e)||(array[i].arg1->type==conststring_e)||(array[i].arg1->type==nil_e))){
                        if(array[i].arg1->type==conststring_e)
                            printf("%d:   %s  \t\t%s\t\"%s\"\t%d\n",i, optostring(array[i].op), array[i].arg1->strConst, array[i].arg2->sym->value.varVal->name, array[i].label);
                        else if(array[i].arg1->type==constnum_e)
                            printf("%d:   %s  \t\t%0.3f\t%s\t%d\n",i, optostring(array[i].op), array[i].arg1->numConst, array[i].arg2->sym->value.varVal->name, array[i].label);

                        else
                            printf("%d:   %s  \t\t%s\t%s\t%d\n",i, optostring(array[i].op), array[i].arg1->sym->value.varVal->name, array[i].arg2->strConst, array[i].label);                    }
                    else{
                        
                        printf("%d:   %s  \t\t%s\t%s\t%d\n",i, optostring(array[i].op), array[i].arg1->strConst, array[i].arg2->strConst, array[i].label);

                    }
                }
            }
        }
        else
        {
            if(array[i].result==NULL){
                printf("%d:   %s\t\t\t\t%d\n", i, optostring(array[i].op), array[i].label);
            }
            else{
                if(array[i].result->type==var_e){
                    printf("%d:   %s\t%s\n", i, optostring(array[i].op), array[i].result->sym->value.varVal->name);
                }
                else if(array[i].result->type==programfunc_e){
                    printf("%d:   %s\t%s\n", i, optostring(array[i].op), array[i].result->sym->value.funcVal->name);
                }
                else if((array[i].result->type==conststring_e)||(array[i].result->type==constbool_e)||array[i].result->type==nil_e){
                    printf("%d:   %s\t%s\n", i, optostring(array[i].op), array[i].result->strConst);
                }
                else if(array[i].result->type==constnum_e){
                    printf("%d:   %s\t%0.2f\n", i, optostring(array[i].op), array[i].result->numConst);
                }
                else{
                    printf("%d:   %s\t%s\n", i, optostring(array[i].op), array[i].result->sym->value.varVal->name);
                }
            }
        }
        //printf("%d:   %s\t%s\t%0.3f\n", i, optostring(array[i].op), array[i].result->sym->value.varVal->name, array[i].arg1->numConst);
    }
    
}

char* newtempname(){
    char b[20];
    b[0]='^';
    sprintf(b+1, "%d", t_id);
    char* newname=malloc(20*sizeof(char));
    strcpy(newname, b);
    t_id++;
    return newname;
}

void hidetemp(ScopeList_Node* head){
    ScopeList_Node* temp=head;
	Binding* temp_binding;
    while (temp!=NULL)
	{
        temp_binding=temp->head;
        while(temp_binding!=NULL)
        {
            if(temp_binding->isFunction==0){
                if(temp_binding->value.varVal->name[0]=='^')
                {
                    temp_binding->isActive=0;
                }
            }
            temp_binding=temp_binding->nextInScope;
	    }
        temp=temp->next;
	}
    t_id=0;
    return;
	
}

Binding* lookuptemp(ScopeList_Node* head, char* name){
ScopeList_Node* temp=head;
	Binding* temp_binding;
    while (temp!=NULL)
	{
        temp_binding=temp->head;
        while(temp_binding!=NULL){
            if(temp_binding->isFunction==0){
                if((strcmp(temp_binding->value.varVal->name, name)==0)){
                    temp_binding->isActive=1;
                    return temp_binding;
                }
            }
            temp_binding=temp_binding->nextInScope;
	    }
        temp=temp->next;
	}
    return NULL;
}


char *exprtostring(enum expr_t expr)
{
    switch (expr)
    {
    case var_e:
        return "variable";
        break;
    case tableitem_e:
        return "table item";
        break;
    case programfunc_e:
        return "program function";
        break;
    case libraryfunc_e:
        return "library function";
        break;
    case arithexpr_e:
        return "arithmetic expression";
        break;
    case boolexpr_e:
        return "boolean expression";
        break;
    case assignexpr_e:
        return "assign expression";
        break;
    case newtable_e:
        return "new table";
        break;
    case constnum_e:
        return "table item";
        break;
    case constbool_e:
        return "boolean";
        break;
    case conststring_e:
        return "string";
        break;
    default:
        return "nil";
        break;
    }
}

char *optostring(enum iocode op)
{
    switch (op)
    {
    case assign_:
        return "assign";
        break;
    case add:
        return "add";
        break;
    case sub:
        return "sub";
        break;
    case mul:
        return "mul";
        break;
    case div_:
        return "div";
        break;
    case mod:
        return "mod";
        break;
    case uminus:
        return "uminus";
        break;
    case and_:
        return "and";
        break;
    case or_:
        return "or";
        break;
    case not_:
        return "not";
        break;
    case if_eq:
        return "if_eq";
        break;
    case if_noteq:
        return "if_noteq";
        break;
    case if_lesseq:
        return "if_lesseq";
        break;
    case if_greatereq:
        return "if_greatereq";
        break;
    case if_less:
        return "if_less";
        break;
    case if_greater:
        return "if_greater";
        break;
    case jump:
        return "jump";
        break;
    case call:
        return "call";
        break;
    case param:
        return "param";
        break;
    case return_:
        return "return";
        break;
    case getretval:
        return "getretval";
        break;
    case funcstart:
        return "funcstart";
        break;
    case funcend:
        return "funcend";
        break;
    case tablecreate:
        return "tablecreate";
        break;
    case tablegetelem:
        return "tablegetelem";
        break;
    case tablesetelem:
        return "tablesetelem";
        break;
    default:
        return "noop";
        break;
    }
}

expr* token_to_expr(ScopeList_Node** scopelist_head, SymTable_T oSymTable, struct alpha_token_t* token, int scope){
    expr* new_expr=malloc(sizeof(expr));
                if(strcmp(token->type, "INTEGER")==0){
                    new_expr->type=constnum_e;
                    new_expr->numConst=atoi(token->content);
                }
                
                else if(strcmp(token->type, "FLOAT")==0){
                    new_expr->type=constnum_e;
                    new_expr->numConst=atof(token->content);
                }
                
                else if(strcmp(token->type, "STRING")==0){
                    new_expr->type=conststring_e;
                    new_expr->strConst=token->content;
                }
                
                else if(strcmp(token->type, "KEYWORD_TRUE")==0){
                    new_expr->type=constbool_e;
                    new_expr->strConst=token->content;
                }
                else if(strcmp(token->type, "KEYWORD_FALSE")==0){
                    new_expr->type=constbool_e;
                    new_expr->strConst=token->content;
                }
                else if(strcmp(token->type, "KEYWORD_NILL")==0){
           		    new_expr->type = nil_e;
                    new_expr->strConst="nil";
                }
                else{
                    new_expr->type=var_e;
                    new_expr->sym=lookup(oSymTable, token->content, scope, "variable");
                    if(new_expr->sym==NULL){
                        insert(scopelist_head, oSymTable, token->content, scope, 0, token->numline, FORMAL, 0, NULL);
                        new_expr->sym=lookup(oSymTable, token->content, scope, "variable");
                    }
                }

                return new_expr;
}

expr* term_token_to_expr(ScopeList_Node** scopelist_head, SymTable_T oSymTable, struct alpha_token_t* token, int scope){
    expr* new_expr=malloc(sizeof(expr));
                if(strcmp(token->type, "INTEGER")==0){
                    new_expr->type=constnum_e;
                    new_expr->numConst=atoi(token->content);
                }
                
                else if(strcmp(token->type, "FLOAT")==0){
                    new_expr->type=constnum_e;
                    new_expr->numConst=atof(token->content);
                }
                
                else if(strcmp(token->type, "STRING")==0){
                    printf("Error in line %d: Cannot operate with string\n", token->numline);
                    exit(-1);
                }
                
                else if(strcmp(token->type, "KEYWORD_TRUE")==0){
                    printf("Error in line %d: Cannot operate with boolean\n", token->numline);
                    exit(-1);
                }
                else if(strcmp(token->type, "KEYWORD_FALSE")==0){
                  printf("Error in line %d: Cannot operate with boolean\n", token->numline);
                  exit(-1);
                }
                else if(strcmp(token->type, "KEYWORD_NILL")==0){
           		   printf("Error in line %d: Cannot operate with nil\n", token->numline);
                      exit(-1);
                }
                else{
                    new_expr->type=var_e;
                    new_expr->sym=lookup(oSymTable, token->content, scope, "variable");
                    if(new_expr->sym==NULL){
                        insert(scopelist_head, oSymTable, token->content, scope, 0, token->numline, FORMAL, 0, NULL);
                        new_expr->sym=lookup(oSymTable, token->content, scope, "variable");
                    }
                }

                return new_expr;
}

