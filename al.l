%{
    #include<stdio.h>
    #include<ctype.h>
    #include<string.h>
    #include<stdlib.h>
    #include "parser.h"
    #include"intermidiate.h"

    int token_count=0;
    int line=1;

    /*token type*/
 
    struct alpha_token_t {
        unsigned int numline;
        unsigned int numToken;
        unsigned int quadline;
        char *content;
        const char *type;
        expr* expression;
        expr* prev_expr;
    };

    struct alpha_token_node {
        struct alpha_token_t* token;
        struct alpha_token_node* next;
    };


    struct alpha_token_node* head=NULL, *curr_node=NULL;

    void register_token(unsigned int numline, unsigned int numToken, char *content, const char *type){
        struct alpha_token_node* new_node=malloc(sizeof(struct alpha_token_node));
        new_node->token=malloc(sizeof(struct alpha_token_t));
        new_node->token->numline=numline;
        new_node->token->numToken=numToken;
        new_node->token->content=malloc(strlen(content)+1);
        strcpy(new_node->token->content, content);
        new_node->token->type=type;
        new_node->next=NULL;
        yylval.token_str=new_node->token;


        if(head==NULL){
            head=new_node;
            curr_node=head;
            return;
        }
        
        curr_node->next=new_node;
        curr_node=new_node;
        return;
    } 



    
%}

KEYWORDS if|else|while|for|function|return|break|continue|and|not|or|local|true|false|nil
OPERANDS \+|\=|\-|\*|\/|\%|==|!=|\+\+|\-\-|>|<|>=|<=
INTEGER [0-9]+
FLOAT [0-9]+"."[0-9]+
STRING \"([^\n"]|\\(.)|\n)*\"
PUNCTUATION \.|\,|\:\:|\:|\;|\.\.|\)|\(|\{|\}|\]|\[
IDENT [A-Za-z][a-zA-Z_0-9]*
LINECOMMENT "//"[^\n"/*"]*
BLOCKCOMMENT "/*"
SPACE [\r\n \t\v]

%option noyywrap

%%

"\n" {line++;}

{SPACE} {}

{INTEGER} {
    token_count++;
    register_token(line, token_count, yytext, "INTEGER");
    return INTEGER;
}

{FLOAT} {
    token_count++;
    register_token(line, token_count, yytext, "FLOAT");
    return FLOAT;
}

{KEYWORDS} {
    token_count++;
    if(!strcmp(yytext, "if")){
        register_token(line, token_count, yytext, "KEYWORD_IF");
        return KEYWORD_IF;
    }
    else if(!strcmp(yytext, "else")){
        register_token(line, token_count, yytext, "KEYWORD_ELSE");
        return KEYWORD_ELSE;
    }
    else if(!strcmp(yytext, "while")){
        register_token(line, token_count, yytext, "KEYWORD_WHILE");
        return KEYWORD_WHILE;
    }
    else if(!strcmp(yytext, "for")){
        register_token(line, token_count, yytext, "KEYWORD_FOR");
        return KEYWORD_FOR;
    }
    else if(!strcmp(yytext, "function")){
        register_token(line, token_count, yytext, "KEYWORD_FUNCTION");
        return KEYWORD_FUNCTION;
    }
    else if(!strcmp(yytext, "return")){
        register_token(line, token_count, yytext, "KEYWORD_RETURN");
        return KEYWORD_RETURN;
    }
    else if(!strcmp(yytext, "break")){
        register_token(line, token_count, yytext, "KEYWORD_BREAK");
        return KEYWORD_BREAK;
    }
    else if(!strcmp(yytext, "continue")){
        register_token(line, token_count, yytext, "KEYWORD_CONTINUE");
        return KEYWORD_CONTINUE;
    }
    else if(!strcmp(yytext, "and")){
        register_token(line, token_count, yytext, "KEYWORD_AND");
        return KEYWORD_AND;
    }
    else if(!strcmp(yytext, "not")){
        register_token(line, token_count, yytext, "KEYWORD_NOT");
        return KEYWORD_NOT;
    }
    else if(!strcmp(yytext, "local")){
        register_token(line, token_count, yytext, "KEYWORD_LOCAL");
        return KEYWORD_LOCAL;
    }
    else if(!strcmp(yytext, "or")){
        register_token(line, token_count, yytext, "KEYWORD_OR");
        return KEYWORD_OR;
    }
    else if(!strcmp(yytext, "true")){
        register_token(line, token_count, yytext, "KEYWORD_TRUE");
        return KEYWORD_TRUE;
    }
    else if(!strcmp(yytext, "false")){
        register_token(line, token_count, yytext, "KEYWORD_FALSE");
        return KEYWORD_FALSE;
    }
    else if(!strcmp(yytext, "nil")){
        register_token(line, token_count, yytext, "KEYWORD_FALSE");
        return KEYWORD_FALSE;
    }
}

{OPERANDS} {
    token_count++;
    if(!strcmp(yytext, "+")){
        register_token(line, token_count, yytext, "OPERAND_PLUS");
        return OPERAND_PLUS;
    }
    else if(!strcmp(yytext, "=")){
        register_token(line, token_count, yytext, "OPERAND_ASSIGN");
        return OPERAND_ASSIGN;
    }
    else if(!strcmp(yytext, "-")){
        register_token(line, token_count, yytext, "OPERAND_MINUS");
        return OPERAND_MINUS;
    }
    else if(!strcmp(yytext, "*")){
        register_token(line, token_count, yytext, "OPERAND_MULTIPLICATION");
        return OPERAND_MULTIPLICATION;
    }
    else if(!strcmp(yytext, "/")){
        register_token(line, token_count, yytext, "OPERAND_DIVIDE");
        return OPERAND_DIVIDE;
    }
    else if(!strcmp(yytext, "%")){
        register_token(line, token_count, yytext, "OPERAND_PERCENTAGE");
        return OPERAND_PERCENTAGE;
    }
    else if(!strcmp(yytext, "==")){
        register_token(line, token_count, yytext, "OPERAND_EQUALS");
        return OPERAND_EQUALS;
    }
    else if(!strcmp(yytext, "!=")){
        register_token(line, token_count, yytext, "OPERAND_NOT_EQUAL");
        return OPERAND_NOT_EQUAL;
    }
    else if(!strcmp(yytext, "++")){
        register_token(line, token_count, yytext, "OPERAND_PLUS_PLUS");
        return OPERAND_PLUS_PLUS;
    }
    else if(!strcmp(yytext, "--")){
        register_token(line, token_count, yytext, "OPERAND_MINUS_MINUS");
        return OPERAND_MINUS_MINUS;
    }
    else if(!strcmp(yytext, ">")){
        register_token(line, token_count, yytext, "OPERAND_GREATER_THAN");
        return OPERAND_GREATER_THAN;
    }
    else if(!strcmp(yytext, "<")){
        register_token(line, token_count, yytext, "OPERAND_SMALLER_THAN");
        return OPERAND_SMALLER_THAN;
    }
    else if(!strcmp(yytext, ">=")){
        register_token(line, token_count, yytext, "OPERAND_GREATER_OR_EQUAL");
        return OPERAND_GREATER_OR_EQUAL;
    }
    else if(!strcmp(yytext, "<=")){
        register_token(line, token_count, yytext, "OPERAND_SMALLER_OR_EQUAL");
        return OPERAND_SMALLER_OR_EQUAL;
    }
        
}

{LINECOMMENT} {
    token_count++;
    char* line_comm=malloc(strlen(yytext)-2);
    strncpy(line_comm, yytext+2, strlen(yytext)-2);
    register_token(token_count,line,line_comm,"LINE_COMMENT");

}

{BLOCKCOMMENT} {
    auto void comment();
    comment(line,0);
    void comment(int l,int emb){
        char c, next;
        int endofcomment;  
        c=input();
        endofcomment = 0;
        while((next=input()) && endofcomment != 1){
            if(c == '\n') line++;
            else if(c == '/'){
                if(next == '*'){
                    comment(line,1);
                }
                else{
                    c = next;
                    continue;
                }
            }
            else if(c == '*'){
                if(next == '/'){
                    endofcomment = 1;
                    token_count++;
                }
                else{
                    c = next;
                    continue;
                }
            }
            c = next;
        }
        if(endofcomment == 1 && emb == 1){
            register_token(token_count,l,"block_comment","EMBEDDED_BLOCK_COMMENT");
        }
        else if(endofcomment == 1 && emb == 0){     
            register_token(token_count,l,"block_comment","BLOCK_COMMENT");
        }
        else if(endofcomment == 0){
            printf("Wrong Comment Format on line: %d \n",line);
            exit(EXIT_FAILURE);
        }
    }

}

{IDENT} {
    token_count++;
    register_token(line, token_count, yytext, "IDENTIFIER");
    
    return IDENTIFIER;
}

{PUNCTUATION} {
    token_count++;
    if(!strcmp(yytext, ".")){
        register_token(line, token_count, yytext, "PUNC_DOT");
        return PUNC_DOT;
    }
    else if(!strcmp(yytext, ",")){
        return PUNC_COM;
    }
    else if(!strcmp(yytext, "::")){
        return PUNC_DOUBLE_COLON;
    }else if(!strcmp(yytext, ":")){
        return PUNC_COLON;
    }else if(!strcmp(yytext, ";")){
        return SEMICOLON;
    }else if(!strcmp(yytext, "..")){
        return PUNC_DOUBLE_DOT;
    }else if(!strcmp(yytext, ")")){
        return PUNC_RIGHT_PAR;
    }else if(!strcmp(yytext, "(")){
        return PUNC_LEFT_PAR;
    }else if(!strcmp(yytext, "}")){
        return PUNC_CURL_RIGHT;
    }else if(!strcmp(yytext, "{")){
        return PUNC_CURL_LEFT;
    }else if(!strcmp(yytext, "]")){
        return PUNC_RIGHT_BRACK;
    }else if(!strcmp(yytext, "[")){
        register_token(line, token_count, yytext, "PUNC_LEFT_BRACK");
        return PUNC_LEFT_BRACK;
    }
}

{STRING} {
    int i,counter,error_flag;
    char *tmp_save=(char*)malloc(strlen(yytext)+1);
    char *string=(char*)malloc(strlen(yytext)+1);
    
    token_count++;
    i=1;
    error_flag=counter=0;

    strncpy(tmp_save,yytext,strlen(yytext)+1);
    tmp_save[strlen(yytext)-1]='\0';

    while(i<(strlen(tmp_save)+1) && error_flag==0){
        if(tmp_save[i]!='\\'){
            string[counter]=tmp_save[i];
            counter++;
        }
        else{
            i++;
            switch(tmp_save[i]){
                case 't': 
                    string[counter]='\t';
                    counter++;
                    break;
                case 'n':
                    string[counter]='\n';
                    counter++;
                    break;
                case 'v':
                    string[counter]='\v';
                    counter++;
                    break;
                case 'f':
                    string[counter]='\f';
                    counter++;
                    break;    
                case 'r':
                    string[counter]='\r';
                    counter++;
                    break;
                case '\\':
                    string[counter]='\\';
                    counter++;
                    break;
                case '\'':
                    string[counter]='\'';
                    counter++;
                    break;
                case '\"':
                    string[counter]='\"';
                    counter++;
                    break;
                default:
                    token_count--;
                    printf("ERROR:Couldn't recognize the token: %s, in line: %d ,as a string.\n",yytext,line);
                    error_flag=1;
                    break;
            }
        }
        i++;    
    }

    if(error_flag!=1){
        register_token(line, token_count, string, "STRING");
        return STRING;
    }

    free(string);
    free(tmp_save);
}

%%
