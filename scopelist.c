#include "scopelist.h"

static void Insert_New_Head(ScopeList_Node **lista, int scope, ScopeList_Node* new_node, Binding* newbind)
{
	ScopeList_Node *p;
	
	new_node->next = (*lista);
	new_node->head = newbind;
	(*lista) = new_node;
	return;
}

Binding *ScopeListLookUp(ScopeList_Node *hScopeList, char *key, int scope)
{
	if(hScopeList==NULL)return NULL;
	ScopeList_Node *tmp_node = hScopeList;
	Binding *tmp_bind;
	//all scopes = -1
	
	if (scope == -1)
	{
		while (tmp_node != NULL)
		{
			tmp_bind = tmp_node->head;
			while (tmp_bind != NULL)
			{
				if (tmp_bind->isFunction)
				{
					if (!strcmp(tmp_bind->value.funcVal->name, key))
						return tmp_bind;
					else
						tmp_bind = tmp_bind->nextInScope;
				}
				else
				{
					if (!strcmp(tmp_bind->value.varVal->name, key))
						return tmp_bind;
					else
						tmp_bind = tmp_bind->nextInScope;
				}
			}
			tmp_node = tmp_node->next;
		}
	}
	else
	{	if(tmp_node==NULL||tmp_node->scope<scope)return NULL;
		while (tmp_node->scope != scope)
			tmp_node = tmp_node->next;
		tmp_bind = tmp_node->head;
		while (tmp_bind != NULL)
		{
			if (tmp_bind->isFunction)
			{
				if (!strcmp(tmp_bind->value.funcVal->name, key))
					return tmp_bind;
				else
					tmp_bind = tmp_bind->nextInScope;
			}
			else
			{
				if (!strcmp(tmp_bind->value.varVal->name, key))
					return tmp_bind;
				else
					tmp_bind = tmp_bind->nextInScope;
			}
		}
	}
	return NULL;
}

void Insert_New_Node(ScopeList_Node **hScopeList, Binding *toInsert, int scope)
{
	ScopeList_Node *temp = *hScopeList;
	ScopeList_Node *new_node;
	Binding *bind_temp;
	new_node = malloc(sizeof(struct ScopeList_Node));
	// new_node->head = toInsert;
	new_node->scope = scope;
	new_node->head = toInsert;
	
	if (temp == NULL)
	{

		new_node->next = NULL;
		(*hScopeList) = new_node;
		return;
	}

	while (temp != NULL)
	{
		if (temp->scope == scope)
		{
			break;
		}
		if (temp->next == NULL)
		{
			Insert_New_Head(hScopeList, scope, new_node, toInsert);
			return;

		}
		temp = temp->next;
	}

	toInsert->nextInScope = temp->head;
	temp->head = toInsert;

	return;
}
