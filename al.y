%{
#include <stdio.h>
#include <string.h>
#include "intermidiate.h"
#include "numlist.h"
#include <stdlib.h>
#include "stack.h"
#include "target.h"
#include "vm.h"


#define EXPAND_SIZE 1024
#define CURR_SIZE (total*sizeof(quad))
#define NEW_SIZE (EXPAND_SIZE*sizeof(quad)+CURR_SIZE)

ScopeList_Node* head_scope;
int yylex(void);
int yyerror (char* yaccProvidedMessage);
extern FILE* yyin;
int nameless_id, t_id, assign_counter, inop, op_flag, not_counter;
char* id_temp;
expr* last_temp;
char* last_name;
expr* last_arg;
expr* lvalue_head, *rvalue_head;
expr_head* list_head;
int list_create;
int global_offset;
int inFunction;
stack_Head *offset_Stack;
int call_arg_func;
int* backpatch_labels;

struct alpha_token_t {
    unsigned int numline;
    unsigned int numToken;
    unsigned int quadline;
    char *content;
    const char *type;
    expr* expression;
    expr* prev_expr;
};

SymTable_T* symbol_table;
char* funtionName;
char* last_func_name;
extern int line;
int scope, varInFunc=0;

quad *quads;
unsigned total;
unsigned int currQuad;
int global_counter;
int cont_break_scope;
SymTable_NumT breaklist;
SymTable_NumT contlist;

int func_call;


unsigned int currInst;



stack_Head *offset_Stack;
stack_Head* funcname_Stack;
//program_mem memory;
int s_counter, n_counter, f_counter, l_counter;

void patchall(SymTable_NumT oSymTable, int nval){
	Num_Binding *t;
	for(t = oSymTable->head; t!=NULL;t=t->next){
        if(t->scope == cont_break_scope){
		    patchlabel(quads+(t->value),nval);
        }
	}
	return;
}

void patch_table(quad* array, expr* arg2){
    t_id--;
    /*SymTable_remove(&symbol_table,array->result->sym->value.varVal->name);*/
    array->op = tablesetelem;
    array->result = array->arg1;
    array->arg1 = array->arg2;
    array->arg2 = arg2;
}

struct alpha_token_t* complete_emit(struct alpha_token_t* token){
    char* temp_name;
    expr* t_expr;
    struct alpha_token_t* ret;
    ret=malloc(sizeof(struct alpha_token_t));
    //ret->expression=lvalue_head;
    ret->numline=token->numline;
    ret->type="table";
    temp_name=malloc(20*sizeof(char));
    temp_name=newtempname();
    ret->content = temp_name;
    t_expr=malloc(sizeof(expr));
    t_expr->type=var_e;
    if(lookup(*symbol_table, temp_name, scope, "variable")==NULL){
        insert(&head_scope, *symbol_table, temp_name, scope, 0, token->numline, GLOBAL, 0, NULL);
    }
    t_expr->sym=lookup(*symbol_table, temp_name, scope, "variable");
    quads = emit(quads, tablegetelem, t_expr, token->expression, token->expression->index,0, token->numline);
    ret->expression = t_expr;
    ret->prev_expr = token->expression;
}

%}  

%start program

%union {
    char* stringValue;
    int intValue;
    double realValue;
    struct alpha_token_t* token_str;
}

%token<token_str> KEYWORD_IF
%token<token_str> KEYWORD_ELSE
%token<token_str> KEYWORD_WHILE
%token<token_str> KEYWORD_FOR
%token<token_str> KEYWORD_FUNCTION
%token<token_str> KEYWORD_RETURN 
%token<token_str> KEYWORD_BREAK
%token<token_str> KEYWORD_CONTINUE
%token<token_str> IDENTIFIER
%token<token_str> SEMICOLON
%token<token_str> PUNC_CURL_LEFT PUNC_CURL_RIGHT
%token<token_str> PUNC_DOUBLE_COLON PUNC_COLON
%token<token_str> KEYWORD_LOCAL
%token<token_str> PUNC_COM
%token<token_str> FLOAT
%token<token_str> STRING
%token<token_str> INTEGER
%token<token_str> KEYWORD_FALSE
%token<token_str> KEYWORD_TRUE
%token<token_str> KEYWORD_NILL

%right<token_str> OPERAND_ASSIGN
%left<token_str> KEYWORD_OR
%left<token_str> KEYWORD_AND
%nonassoc<token_str> OPERAND_EQUALS OPERAND_NOT_EQUAL
%nonassoc<token_str> OPERAND_SMALLER_OR_EQUAL OPERAND_SMALLER_THAN OPERAND_GREATER_THAN OPERAND_GREATER_OR_EQUAL
%left<token_str> OPERAND_PLUS OPERAND_MINUS
%left<token_str> OPERAND_MULTIPLICATION OPERAND_DIVIDE OPERAND_PERCENTAGE
%right<token_str> KEYWORD_NOT OPERAND_PLUS_PLUS OPERAND_MINUS_MINUS 
%left<token_str> PUNC_DOT PUNC_DOUBLE_DOT
%left<token_str> PUNC_LEFT_BRACK PUNC_RIGHT_BRACK
%left<token_str> PUNC_LEFT_PAR PUNC_RIGHT_PAR


%type <token_str> program
%type <token_str> stmts
%type <token_str> stmt
%type <token_str> term
%type <token_str> assginexpr
%type <token_str> expr
%type <token_str> primary
%type <token_str> lvalue
%type <token_str> member
%type <token_str> call
%type <token_str> callsuffix
%type <token_str> normcall
%type <token_str> methodcall
%type <token_str> elist
%type <token_str> objectdef
%type <token_str> indexed
%type <token_str> indexedelem
%type <token_str> block
%type <token_str> funcdef
%type <token_str> const
%type <token_str> idlist
%type <token_str> ifstmt
%type <token_str> whilestmt
%type <token_str> forstmt
%type <token_str> returnstmt


%%
program: {}
        |stmts
        ;

stmts: stmt{$$ = $1; }
      |stmt stmts{
          /*struct alpha_token_t *ret_token=malloc(sizeof(struct alpha_token_t));
          $$=ret_token;
          $$->breaklist = merge($1->breaklist,$2->breaklist);
          $$->contlist = merge($1->contlist,$2->contlist);*/
      }
      ;

stmt: expr SEMICOLON {
    if(assign_counter == 0 && strcmp($1->type,"table") == 0){
        complete_emit($1);
    }
    hidetemp(head_scope); 
    assign_counter=0;
    inop=0;
}
    | ifstmt 
    | whilestmt
    | forstmt 
    | returnstmt SEMICOLON
    | KEYWORD_BREAK SEMICOLON{
        /*struct alpha_token_t *ret_token=malloc(sizeof(struct alpha_token_t));
        $$=ret_token;
        $$->breaklist = SymTable_Num_new();
        SymTable_Num_put($$->breaklist,currQuad);*/
        SymTable_Num_put(breaklist,currQuad,cont_break_scope);
        quads=emit(quads, jump,NULL, NULL, NULL, 0, 0);

    }
    | KEYWORD_CONTINUE SEMICOLON{
        /*struct alpha_token_t *ret_token=malloc(sizeof(struct alpha_token_t));
        $$=ret_token;
        $$->contlist = SymTable_Num_new();
        SymTable_Num_put($$->contlist,currQuad);*/
        SymTable_Num_put(contlist,currQuad,cont_break_scope);
        quads=emit(quads, jump,NULL, NULL, NULL, 0, 0);
    } 
    | block
    | funcdef
    ;
expr: assginexpr{$$=$1;}
    | expr OPERAND_DIVIDE expr {
        if(strcmp($1->type, "table")==0){
            $1 = complete_emit($1);
        }
        if(strcmp($3->type, "table")==0){
            $3 = complete_emit($3);
        }
        Binding *b;
        if((strcmp($1->type, "IDENTIFIER")==0)&&((b=lookup(*symbol_table, $1->content, scope, "variable"))==NULL)){
            insert(&head_scope, *symbol_table, $1->content, scope, 0, $1->numline, FORMAL, 0, NULL);
        }
        if((strcmp($3->type, "IDENTIFIER")==0)&&(b=lookup(*symbol_table, $3->content, scope, "variable"))==NULL){
            insert(&head_scope, *symbol_table, $3->content, scope, 0, $3->numline, FORMAL, 0, NULL);
        }


        char* temp_name=malloc(20*sizeof(char*));
        
        if(inop==0||op_flag>=1){
            temp_name=newtempname();
            last_name=temp_name;
        }
        else{
            temp_name=last_name;
        }
        
        Binding *tmp=lookuptemp(head_scope, temp_name);

        if(tmp==NULL){
            insert(&head_scope, *symbol_table, temp_name, scope, 0, 0, LOCAL, 0, NULL);
            tmp=lookup(*symbol_table, temp_name, scope, "variable");
        }
        expr* t_expr= malloc(sizeof(expr));
        t_expr->sym=tmp;
        t_expr->isConst=0;
        last_temp=t_expr;

        expr* arg1=malloc(sizeof(expr));
      
        arg1= term_token_to_expr(&head_scope, *symbol_table, $1, scope);
        
        expr* arg2=malloc(sizeof(expr));

        arg2=term_token_to_expr(&head_scope, *symbol_table, $3, scope);

        quads=emit(quads, div_, t_expr, arg1, arg2, 0, $1->numline);
        struct alpha_token_t *ret_token=malloc(sizeof(struct alpha_token_t));
        ret_token->type="IDENTIFIER";
        ret_token->numline=0;
        ret_token->content=temp_name;
        inop=0;
        op_flag=0;
        $$=ret_token;
    }
    | expr OPERAND_PERCENTAGE expr {
        if(strcmp($1->type, "table")==0){
            $1 = complete_emit($1);
        }
        if(strcmp($3->type, "table")==0){
            $3 = complete_emit($3);
        }
        $$=$1;
        Binding *b;
        if((strcmp($1->type, "IDENTIFIER")==0)&&((b=lookup(*symbol_table, $1->content, scope, "variable"))==NULL)){
            insert(&head_scope, *symbol_table, $1->content, scope, 0, $1->numline, FORMAL, 0, NULL);
        }
        if((strcmp($3->type, "IDENTIFIER")==0)&&(b=lookup(*symbol_table, $3->content, scope, "variable"))==NULL){
            insert(&head_scope, *symbol_table, $3->content, scope, 0, $3->numline, FORMAL, 0, NULL);
        }

        char* temp_name=malloc(20*sizeof(char*));
        if(inop==0){
            temp_name=newtempname();
            last_name=temp_name;
        }
        else{
            temp_name=last_name;
        }
        
        Binding *tmp=lookuptemp(head_scope, temp_name);

        if(tmp==NULL){
            insert(&head_scope, *symbol_table, temp_name, scope, 0, 0, LOCAL, 0, NULL);
            tmp=lookup(*symbol_table, temp_name, scope, "variable");
        }
        expr* t_expr= malloc(sizeof(expr));
        t_expr->sym=tmp;
        t_expr->isConst=0;
        last_temp=t_expr;

        expr* arg1=malloc(sizeof(expr));
        arg1=term_token_to_expr(&head_scope, *symbol_table, $1, scope);

        expr* arg2=malloc(sizeof(expr));
        arg2=term_token_to_expr(&head_scope, *symbol_table, $3, scope);

        quads=emit(quads, mod, t_expr, arg1, arg2, 0, $1->numline);
        struct alpha_token_t *ret_token=malloc(sizeof(struct alpha_token_t));
        ret_token->type="IDENTIFIER";
        ret_token->numline=0;
        ret_token->content=temp_name;
        inop=1;
        $$=ret_token;
    }
    | expr OPERAND_GREATER_THAN expr {
        if(strcmp($1->type, "table")==0){
            $1 = complete_emit($1);
        }
        if(strcmp($3->type, "table")==0){
            $3 = complete_emit($3);
        }
        $$=$1;
   Binding *b;
        if((strcmp($1->type, "IDENTIFIER")==0)&&((b=lookup(*symbol_table, $1->content, scope, "variable"))==NULL)){
            insert(&head_scope, *symbol_table, $1->content, scope, 0, $1->numline, FORMAL, 0, NULL);
        }
        if((strcmp($3->type, "IDENTIFIER")==0)&&((b=lookup(*symbol_table, $3->content, scope, "variable"))==NULL)){
            insert(&head_scope, *symbol_table, $3->content, scope, 0, $3->numline, FORMAL, 0, NULL);
        }

        expr* arg1=malloc(sizeof(struct expr));
        expr* arg2=malloc(sizeof(struct expr));
        expr* true_expr=malloc(sizeof(struct expr));
        expr* false_expr=malloc(sizeof(struct expr));
        expr* t_expr=malloc(sizeof(struct expr));


        char* temp_name=malloc(20*sizeof(char*));
        if(inop==0){
            temp_name=newtempname();
            last_name=temp_name;
        }
        else{
            temp_name=last_name;
        }

        arg1=term_token_to_expr(&head_scope, *symbol_table, $1, scope);

        arg2=term_token_to_expr(&head_scope, *symbol_table, $3, scope);

        true_expr->type=constbool_e;
        true_expr->strConst="true";

        false_expr->type=constbool_e;
        false_expr->strConst="false";

        t_expr->type=var_e;
        t_expr->sym=lookup(*symbol_table, temp_name, scope, "variable");

        if(t_expr->sym==NULL){
            insert(&head_scope, *symbol_table, temp_name, scope, 0, $3->numline, FORMAL, 0, NULL);
            t_expr->sym=lookup(*symbol_table, temp_name, scope, "variable");
        }


        int temp_val=currQuad;
        int l1, l2, l3, l4;
        l4=temp_val+4;
        l1=(temp_val += 2);
        l2=(temp_val += 2);
        l3=temp_val + 1;

        quads=emit(quads, if_greater, NULL ,arg1 ,arg2, l1, $2->numline);
        quads=emit(quads, jump, NULL ,NULL ,NULL, l2, $2->numline);
        quads=emit(quads, assign_, t_expr ,true_expr ,NULL , 0, $2->numline);
        quads=emit(quads, jump, NULL ,NULL ,NULL, l3, $2->numline);
        quads=emit(quads, assign_, t_expr ,false_expr ,NULL , 0, $2->numline);

        struct alpha_token_t *ret_token=malloc(sizeof(struct alpha_token_t));
        ret_token->type="IDENTIFIER";
        ret_token->numline=0;
        ret_token->content=temp_name;
        inop=0;
        $$=ret_token;
    }
    | expr OPERAND_MULTIPLICATION expr {
        if(strcmp($1->type, "table")==0){
            $1 = complete_emit($1);
        }
        if(strcmp($3->type, "table")==0){
            $3 = complete_emit($3);
        }
    Binding *b;
        if((strcmp($1->type, "IDENTIFIER")==0)&&((b=lookup(*symbol_table, $1->content, scope, "variable"))==NULL)){
            insert(&head_scope, *symbol_table, $1->content, scope, 0, $1->numline, FORMAL, 0, NULL);
        }
        if((strcmp($3->type, "IDENTIFIER")==0)&&(b=lookup(*symbol_table, $3->content, scope, "variable"))==NULL){
            insert(&head_scope, *symbol_table, $3->content, scope, 0, $3->numline, FORMAL, 0, NULL);
        }

        char* temp_name=malloc(20*sizeof(char*));
        if(inop==0){
            temp_name=newtempname();
            last_name=temp_name;
        }
        else{
            temp_name=last_name;
        }
        
        Binding *tmp=lookuptemp(head_scope, temp_name);

        if(tmp==NULL){
            insert(&head_scope, *symbol_table, temp_name, scope, 0, 0, LOCAL, 0, NULL);
            tmp=lookup(*symbol_table, temp_name, scope, "variable");
        }
        expr* t_expr= malloc(sizeof(expr));
        t_expr->sym=tmp;
        t_expr->isConst=0;
        last_temp=t_expr;

        expr* arg1=malloc(sizeof(expr));
        arg1=term_token_to_expr(&head_scope, *symbol_table, $1, scope);

        expr* arg2=malloc(sizeof(expr));
        arg2=term_token_to_expr(&head_scope, *symbol_table, $3, scope);

        quads=emit(quads, mul, t_expr, arg1, arg2, 0, $1->numline);
        struct alpha_token_t *ret_token=malloc(sizeof(struct alpha_token_t));
        ret_token->type="IDENTIFIER";
        ret_token->numline=0;
        ret_token->content=temp_name;
        inop=1;
        $$=ret_token;
    }
    | expr OPERAND_MINUS expr {
        if(strcmp($1->type, "table")==0){
            $1 = complete_emit($1);
        }
        if(strcmp($3->type, "table")==0){
            $3 = complete_emit($3);
        }
    Binding *b;
        op_flag++;
        if((strcmp($1->type, "IDENTIFIER")==0)&&((b=lookup(*symbol_table, $1->content, scope, "variable"))==NULL)){
            insert(&head_scope, *symbol_table, $1->content, scope, 0, $1->numline, FORMAL, 0, NULL);
        }
        if((strcmp($3->type, "IDENTIFIER")==0)&&(b=lookup(*symbol_table, $3->content, scope, "variable"))==NULL){
            insert(&head_scope, *symbol_table, $3->content, scope, 0, $3->numline, FORMAL, 0, NULL);
        }

        char* temp_name=malloc(20*sizeof(char*));
        if(inop==0){
            temp_name=newtempname();
            last_name=temp_name;
        }
        else{
            temp_name=last_name;
        }
        
        Binding *tmp=lookuptemp(head_scope, temp_name);

        if(tmp==NULL){
            insert(&head_scope, *symbol_table, temp_name, scope, 0, 0, LOCAL, 0, NULL);
            tmp=lookup(*symbol_table, temp_name, scope, "variable");
        }
        expr* t_expr= malloc(sizeof(expr));
        t_expr->sym=tmp;
        t_expr->isConst=0;
        last_temp=t_expr;

        expr* arg1=malloc(sizeof(expr));
        arg1=term_token_to_expr(&head_scope, *symbol_table, $1, scope);

        expr* arg2=malloc(sizeof(expr));
        
        arg2=term_token_to_expr(&head_scope, *symbol_table, $3, scope);

        quads=emit(quads, sub, t_expr, arg1, arg2, 0, $1->numline);
        struct alpha_token_t *ret_token=malloc(sizeof(struct alpha_token_t));
        ret_token->type="IDENTIFIER";
        ret_token->numline=0;
        ret_token->content=temp_name;
        inop=1;
        $$=ret_token;
    }
    | expr OPERAND_PLUS expr {
        if(strcmp($1->type, "table")==0){
            $1 = complete_emit($1);
        }
        if(strcmp($3->type, "table")==0){
            $3 = complete_emit($3);
        }
    Binding *b;
    op_flag++;
        if((strcmp($1->type, "IDENTIFIER")==0)&&((b=lookup(*symbol_table, $1->content, scope, "variable"))==NULL)){
            insert(&head_scope, *symbol_table, $1->content, scope, 0, $1->numline, FORMAL, 0, NULL);
        }
        if((strcmp($3->type, "IDENTIFIER")==0)&&(b=lookup(*symbol_table, $3->content, scope, "variable"))==NULL){
            insert(&head_scope, *symbol_table, $3->content, scope, 0, $3->numline, FORMAL, 0, NULL);
        }

        char* temp_name=malloc(20*sizeof(char*));
        if(inop==0||op_flag==1){
            temp_name=newtempname();
            last_name=temp_name;
        }
        else{
            temp_name=last_name;
        }
        
        Binding *tmp=lookuptemp(head_scope, temp_name);

        if(tmp==NULL){
            insert(&head_scope, *symbol_table, temp_name, scope, 0, 0, LOCAL, 0, NULL);
            tmp=lookup(*symbol_table, temp_name, scope, "variable");
        }
        expr* t_expr= malloc(sizeof(expr));
        t_expr->sym=tmp;
        t_expr->isConst=0;
        last_temp=t_expr;

        expr* arg1=malloc(sizeof(expr));
        
        arg1=term_token_to_expr(&head_scope, *symbol_table, $1, scope);

        expr* arg2=malloc(sizeof(expr));
        
        arg2=term_token_to_expr(&head_scope, *symbol_table, $3, scope);

        quads=emit(quads, add, t_expr, arg1, arg2, 0, $1->numline);
        struct alpha_token_t *ret_token=malloc(sizeof(struct alpha_token_t));
        ret_token->type="IDENTIFIER";
        ret_token->numline=0;
        ret_token->content=temp_name;
        inop=0;
        $$=ret_token;
    }
    | expr OPERAND_SMALLER_OR_EQUAL expr {
        if(strcmp($1->type, "table")==0){
            $1 = complete_emit($1);
        }
        if(strcmp($3->type, "table")==0){
            $3 = complete_emit($3);
        }
    Binding *b;
        if((strcmp($1->type, "IDENTIFIER")==0)&&((b=lookup(*symbol_table, $1->content, scope, "variable"))==NULL)){
            insert(&head_scope, *symbol_table, $1->content, scope, 0, $1->numline, FORMAL, 0, NULL);
        }
        if((strcmp($3->type, "IDENTIFIER")==0)&&((b=lookup(*symbol_table, $3->content, scope, "variable"))==NULL)){
            insert(&head_scope, *symbol_table, $3->content, scope, 0, $3->numline, FORMAL, 0, NULL);
        }

        expr* arg1=malloc(sizeof(struct expr));
        expr* arg2=malloc(sizeof(struct expr));
        expr* true_expr=malloc(sizeof(struct expr));
        expr* false_expr=malloc(sizeof(struct expr));
        expr* t_expr=malloc(sizeof(struct expr));


        char* temp_name=malloc(20*sizeof(char*));
        if(inop==0){
            temp_name=newtempname();
            last_name=temp_name;
        }
        else{
            temp_name=last_name;
        }

        arg1=term_token_to_expr(&head_scope, *symbol_table, $1, scope);

        arg2=term_token_to_expr(&head_scope, *symbol_table, $3, scope);

        true_expr->type=constbool_e;
        true_expr->strConst="true";

        false_expr->type=constbool_e;
        false_expr->strConst="false";

        t_expr->type=var_e;
        t_expr->sym=lookup(*symbol_table, temp_name, scope, "variable");

        if(t_expr->sym==NULL){
            insert(&head_scope, *symbol_table, temp_name, scope, 0, $3->numline, FORMAL, 0, NULL);
            t_expr->sym=lookup(*symbol_table, temp_name, scope, "variable");
        }


        int temp_val=currQuad;
        int l1, l2, l3, l4;
        l4=temp_val+4;
        l1=(temp_val += 2);
        l2=(temp_val += 2);
        l3=temp_val + 1;

        quads=emit(quads, if_lesseq, NULL ,arg1 ,arg2, l1, $2->numline);
        quads=emit(quads, jump, NULL ,NULL ,NULL, l2, $2->numline);
        quads=emit(quads, assign_, t_expr ,true_expr ,NULL , 0, $2->numline);
        quads=emit(quads, jump, NULL ,NULL ,NULL, l3, $2->numline);
        quads=emit(quads, assign_, t_expr ,false_expr ,NULL , 0, $2->numline);

        struct alpha_token_t *ret_token=malloc(sizeof(struct alpha_token_t));
        ret_token->type="IDENTIFIER";
        ret_token->numline=0;
        ret_token->content=temp_name;
        inop=0;
        $$=ret_token;


    }
    | expr OPERAND_SMALLER_THAN expr {
        if(strcmp($1->type, "table")==0){
            $1 = complete_emit($1);
        }
        if(strcmp($3->type, "table")==0){
            $3 = complete_emit($3);
        }
        Binding *b;
        if((strcmp($1->type, "IDENTIFIER")==0)&&((b=lookup(*symbol_table, $1->content, scope, "variable"))==NULL)){
            insert(&head_scope, *symbol_table, $1->content, scope, 0, $1->numline, FORMAL, 0, NULL);
        }
        if((strcmp($3->type, "IDENTIFIER")==0)&&((b=lookup(*symbol_table, $3->content, scope, "variable"))==NULL)){
            insert(&head_scope, *symbol_table, $3->content, scope, 0, $3->numline, FORMAL, 0, NULL);
        }

        expr* arg1=malloc(sizeof(struct expr));
        expr* arg2=malloc(sizeof(struct expr));
        expr* true_expr=malloc(sizeof(struct expr));
        expr* false_expr=malloc(sizeof(struct expr));
        expr* t_expr=malloc(sizeof(struct expr));


        char* temp_name=malloc(20*sizeof(char*));
        if(inop==0){
            temp_name=newtempname();
            last_name=temp_name;
        }
        else{
            temp_name=last_name;
        }

        arg1=term_token_to_expr(&head_scope, *symbol_table, $1, scope);

        arg2=term_token_to_expr(&head_scope, *symbol_table, $3, scope);

        true_expr->type=constbool_e;
        true_expr->strConst="true";

        false_expr->type=constbool_e;
        false_expr->strConst="false";

        t_expr->type=var_e;
        t_expr->sym=lookup(*symbol_table, temp_name, scope, "variable");

        if(t_expr->sym==NULL){
            insert(&head_scope, *symbol_table, temp_name, scope, 0, $3->numline, FORMAL, 0, NULL);
            t_expr->sym=lookup(*symbol_table, temp_name, scope, "variable");
        }


        int temp_val=currQuad;
        int l1, l2, l3, l4;
        l4=temp_val+4;
        l1=(temp_val += 2);
        l2=(temp_val += 2);
        l3=temp_val + 1;

        quads=emit(quads, if_less, NULL ,arg1 ,arg2, l1, $2->numline);
        quads=emit(quads, jump, NULL ,NULL ,NULL, l2, $2->numline);
        quads=emit(quads, assign_, t_expr ,true_expr ,NULL , 0, $2->numline);
        quads=emit(quads, jump, NULL ,NULL ,NULL, l3, $2->numline);
        quads=emit(quads, assign_, t_expr ,false_expr ,NULL , 0, $2->numline);

        struct alpha_token_t *ret_token=malloc(sizeof(struct alpha_token_t));
        ret_token->type="IDENTIFIER";
        ret_token->numline=0;
        ret_token->content=temp_name;
        inop=0;
        $$=ret_token;
    }
    | expr OPERAND_GREATER_OR_EQUAL expr {
        if(strcmp($1->type, "table")==0){
            $1 = complete_emit($1);
        }
        if(strcmp($3->type, "table")==0){
            $3 = complete_emit($3);
        }
    Binding *b;
        if((strcmp($1->type, "IDENTIFIER")==0)&&((b=lookup(*symbol_table, $1->content, scope, "variable"))==NULL)){
            insert(&head_scope, *symbol_table, $1->content, scope, 0, $1->numline, FORMAL, 0, NULL);
        }
        if((strcmp($3->type, "IDENTIFIER")==0)&&((b=lookup(*symbol_table, $3->content, scope, "variable"))==NULL)){
            insert(&head_scope, *symbol_table, $3->content, scope, 0, $3->numline, FORMAL, 0, NULL);
        }

        expr* arg1=malloc(sizeof(struct expr));
        expr* arg2=malloc(sizeof(struct expr));
        expr* true_expr=malloc(sizeof(struct expr));
        expr* false_expr=malloc(sizeof(struct expr));
        expr* t_expr=malloc(sizeof(struct expr));


        char* temp_name=malloc(20*sizeof(char*));
        if(inop==0){
            temp_name=newtempname();
            last_name=temp_name;
        }
        else{
            temp_name=last_name;
        }

        arg1=term_token_to_expr(&head_scope, *symbol_table, $1, scope);

        arg2=term_token_to_expr(&head_scope, *symbol_table, $3, scope);

        true_expr->type=constbool_e;
        true_expr->strConst="true";

        false_expr->type=constbool_e;
        false_expr->strConst="false";

        t_expr->type=var_e;
        t_expr->sym=lookup(*symbol_table, temp_name, scope, "variable");

        if(t_expr->sym==NULL){
            insert(&head_scope, *symbol_table, temp_name, scope, 0, $3->numline, FORMAL, 0, NULL);
            t_expr->sym=lookup(*symbol_table, temp_name, scope, "variable");
        }


        int temp_val=currQuad;
        int l1, l2, l3, l4;
        l4=temp_val+4;
        l1=(temp_val += 2);
        l2=(temp_val += 2);
        l3=temp_val + 1;

        quads=emit(quads, if_greatereq, NULL ,arg1 ,arg2, l1, $2->numline);
        quads=emit(quads, jump, NULL ,NULL ,NULL, l2, $2->numline);
        quads=emit(quads, assign_, t_expr ,true_expr ,NULL , 0, $2->numline);
        quads=emit(quads, jump, NULL ,NULL ,NULL, l3, $2->numline);
        quads=emit(quads, assign_, t_expr ,false_expr ,NULL , 0, $2->numline);

        struct alpha_token_t *ret_token=malloc(sizeof(struct alpha_token_t));
        ret_token->type="IDENTIFIER";
        ret_token->numline=0;
        ret_token->content=temp_name;
        inop=0;
        $$=ret_token;
    }
    | expr OPERAND_NOT_EQUAL expr {
        if(strcmp($1->type, "table")==0){
            $1 = complete_emit($1);
        }
        if(strcmp($3->type, "table")==0){
            $3 = complete_emit($3);
        }
     Binding *b;
        if((strcmp($1->type, "IDENTIFIER")==0)&&((b=lookup(*symbol_table, $1->content, scope, "variable"))==NULL)){
            insert(&head_scope, *symbol_table, $1->content, scope, 0, $1->numline, FORMAL, 0, NULL);
        }
        if((strcmp($3->type, "IDENTIFIER")==0)&&((b=lookup(*symbol_table, $3->content, scope, "variable"))==NULL)){
            insert(&head_scope, *symbol_table, $3->content, scope, 0, $3->numline, FORMAL, 0, NULL);
        }

        expr* arg1=malloc(sizeof(struct expr));
        expr* arg2=malloc(sizeof(struct expr));
        expr* true_expr=malloc(sizeof(struct expr));
        expr* false_expr=malloc(sizeof(struct expr));
        expr* t_expr=malloc(sizeof(struct expr));


        char* temp_name=malloc(20*sizeof(char*));
        if(inop==0){
            temp_name=newtempname();
            last_name=temp_name;
        }
        else{
            temp_name=last_name;
        }

        arg1=token_to_expr(&head_scope, *symbol_table, $1, scope);

        arg2=token_to_expr(&head_scope, *symbol_table, $3, scope);



        true_expr->type=constbool_e;
        true_expr->strConst="true";

        false_expr->type=constbool_e;
        false_expr->strConst="false";

        t_expr->type=var_e;
        t_expr->sym=lookup(*symbol_table, temp_name, scope, "variable");

        if(t_expr->sym==NULL){
            insert(&head_scope, *symbol_table, temp_name, scope, 0, $3->numline, FORMAL, 0, NULL);
            t_expr->sym=lookup(*symbol_table, temp_name, scope, "variable");
        }


        int temp_val=currQuad;
        int l1, l2, l3, l4;
        l4=temp_val+4;
        l1=(temp_val += 2);
        l2=(temp_val += 2);
        l3=temp_val + 1;

        quads=emit(quads, if_noteq, NULL ,arg1 ,arg2, l1, $2->numline);
        quads=emit(quads, jump, NULL ,NULL ,NULL, l2, $2->numline);
        quads=emit(quads, assign_, t_expr ,true_expr ,NULL , 0, $2->numline);
        quads=emit(quads, jump, NULL ,NULL ,NULL, l3, $2->numline);
        quads=emit(quads, assign_, t_expr ,false_expr ,NULL , 0, $2->numline);

        struct alpha_token_t *ret_token=malloc(sizeof(struct alpha_token_t));
        ret_token->type="IDENTIFIER";
        ret_token->numline=0;
        ret_token->content=temp_name;
        inop=0;
        $$=ret_token;
    }
    | expr KEYWORD_OR expr {
        if(strcmp($1->type, "table")==0){
            $1 = complete_emit($1);
        }
        if(strcmp($3->type, "table")==0){
            $3 = complete_emit($3);
        }
     Binding *b;
        if(strcmp($1->type, $3->type)){printf("Error cannot compare 2 different types\n");}
        if((strcmp($1->type, "IDENTIFIER")==0)&&((b=lookup(*symbol_table, $1->content, scope, "variable"))==NULL)){
            insert(&head_scope, *symbol_table, $1->content, scope, 0, $1->numline, FORMAL, 0, NULL);
        }
        if((strcmp($3->type, "IDENTIFIER")==0)&&(b=lookup(*symbol_table, $3->content, scope, "variable"))==NULL){
            insert(&head_scope, *symbol_table, $3->content, scope, 0, $3->numline, FORMAL, 0, NULL);
        }
        
        expr* arg1=malloc(sizeof(struct expr));
        expr* arg2=malloc(sizeof(struct expr));
        expr* true_expr=malloc(sizeof(struct expr));
        expr* false_expr=malloc(sizeof(struct expr));
        expr* t_expr=malloc(sizeof(struct expr));


        char* temp_name=malloc(20*sizeof(char*));
        if(inop==0){
            temp_name=newtempname();
            last_name=temp_name;
        }
        else{
            temp_name=last_name;
        }

       arg1=token_to_expr(&head_scope, *symbol_table, $1, scope);

        arg2=token_to_expr(&head_scope, *symbol_table, $3, scope);

        int temp_val=currQuad;

        true_expr->type=constbool_e;
        true_expr->strConst="true";

        false_expr->type=constbool_e;
        false_expr->strConst="false";

        t_expr->type=var_e;
        t_expr->sym=lookup(*symbol_table, temp_name, scope, "variable");

        int l1, l2, l3, l4;
        l4=temp_val+4;
        l1=(temp_val += 2);
        l2=(temp_val += 4);
        l3=temp_val + 1;
        

        if(t_expr->sym==NULL){
            insert(&head_scope, *symbol_table, temp_name, scope, 0, $3->numline, FORMAL, 0, NULL);
            t_expr->sym=lookup(*symbol_table, temp_name, scope, "variable");
        }

    

        quads=emit(quads, if_noteq,NULL ,arg1, true_expr, l1, $1->numline);
        quads=emit(quads, jump, NULL, NULL ,NULL, l4, $1->numline);
        quads=emit(quads, if_noteq,NULL ,arg2  , true_expr, l2, $1->numline);
        quads=emit(quads, jump, NULL, NULL , NULL, l4, $1->numline);
        quads=emit(quads, assign_, t_expr, true_expr, 0, 0, $1->numline);
        quads=emit(quads, jump, NULL, NULL , NULL, l3, $1->numline);
        quads=emit(quads, assign_, t_expr, false_expr, NULL, 0, $1->numline);

        struct alpha_token_t *ret_token=malloc(sizeof(struct alpha_token_t));
        ret_token->type="IDENTIFIER";
        ret_token->numline=0;
        ret_token->content=temp_name;
        inop=0;
        $$=ret_token;
    }
    | expr KEYWORD_AND expr {
        if(strcmp($1->type, "table")==0){
            $1 = complete_emit($1);
        }
        if(strcmp($3->type, "table")==0){
            $3 = complete_emit($3);
        }
        Binding *b;
        if(strcmp($1->type, $3->type)){printf("Error cannot compare 2 different types\n");}
        if((strcmp($1->type, "IDENTIFIER")==0)&&((b=lookup(*symbol_table, $1->content, scope, "variable"))==NULL)){
            insert(&head_scope, *symbol_table, $1->content, scope, 0, $1->numline, FORMAL, 0, NULL);
        }
        if((strcmp($3->type, "IDENTIFIER")==0)&&(b=lookup(*symbol_table, $3->content, scope, "variable"))==NULL){
            insert(&head_scope, *symbol_table, $3->content, scope, 0, $3->numline, FORMAL, 0, NULL);
        }
        
        expr* arg1=malloc(sizeof(struct expr));
        expr* arg2=malloc(sizeof(struct expr));
        expr* true_expr=malloc(sizeof(struct expr));
        expr* false_expr=malloc(sizeof(struct expr));
        expr* t_expr=malloc(sizeof(struct expr));

        char* temp_name=malloc(20*sizeof(char*));
        if(inop==0){
            temp_name=newtempname();
            last_name=temp_name;
        }
        else{
            temp_name=last_name;
        }

       arg1=token_to_expr(&head_scope, *symbol_table, $1, scope);

       arg2=token_to_expr(&head_scope, *symbol_table, $3, scope);

        int temp_val=currQuad;

        true_expr->type=constbool_e;
        true_expr->strConst="true";

        false_expr->type=constbool_e;
        false_expr->strConst="false";

        t_expr->type=var_e;
        t_expr->sym=lookup(*symbol_table, temp_name, scope, "variable");

        int l1, l2, l3, l4;
        l4=temp_val+4;
        l1=(temp_val += 2);
        l2=(temp_val += 4);
        l3=temp_val + 1;

        if(t_expr->sym==NULL){
            insert(&head_scope, *symbol_table, temp_name, scope, 0, $3->numline, FORMAL, 0, NULL);
            t_expr->sym=lookup(*symbol_table, temp_name, scope, "variable");
        }

    

        quads=emit(quads, if_eq, NULL ,arg1, true_expr, l1, $1->numline);
        quads=emit(quads, jump, NULL, NULL ,NULL, l4, $1->numline);
        quads=emit(quads, if_eq, NULL,  arg2 ,true_expr, l2, $1->numline);
        quads=emit(quads, jump, NULL, NULL , NULL, l4, $1->numline);
        quads=emit(quads, assign_, t_expr, false_expr, 0, 0, $1->numline);
        quads=emit(quads, jump, NULL, NULL , NULL, l3, $1->numline);
        quads=emit(quads, assign_, t_expr, true_expr, NULL, 0, $1->numline);

        struct alpha_token_t *ret_token=malloc(sizeof(struct alpha_token_t));
        ret_token->type="IDENTIFIER";
        ret_token->numline=0;
        ret_token->content=temp_name;
        inop=0;
        $$=ret_token;

    }
    | expr OPERAND_EQUALS expr {
        if(strcmp($1->type, "table")==0){
            $1 = complete_emit($1);
        }
        if(strcmp($3->type, "table")==0){
            $3 = complete_emit($3);
        }
        Binding *b;
        if((strcmp($1->type, "IDENTIFIER")==0)&&((b=lookup(*symbol_table, $1->content, scope, "variable"))==NULL)){
            insert(&head_scope, *symbol_table, $1->content, scope, 0, $1->numline, FORMAL, 0, NULL);
        }
        if((strcmp($3->type, "IDENTIFIER")==0)&&((b=lookup(*symbol_table, $3->content, scope, "variable"))==NULL)){
            insert(&head_scope, *symbol_table, $3->content, scope, 0, $3->numline, FORMAL, 0, NULL);
        }

        expr* arg1=malloc(sizeof(struct expr));
        expr* arg2=malloc(sizeof(struct expr));
        expr* true_expr=malloc(sizeof(struct expr));
        expr* false_expr=malloc(sizeof(struct expr));
        expr* t_expr=malloc(sizeof(struct expr));


        char* temp_name=malloc(20*sizeof(char*));
        if(inop==0){
            temp_name=newtempname();
            last_name=temp_name;
        }
        else{
            temp_name=last_name;
        }

        arg1=token_to_expr(&head_scope, *symbol_table, $1, scope);

        arg2=token_to_expr(&head_scope, *symbol_table, $3, scope);


        true_expr->type=constbool_e;
        true_expr->strConst="true";

        false_expr->type=constbool_e;
        false_expr->strConst="false";

        t_expr->type=var_e;
        t_expr->sym=lookup(*symbol_table, temp_name, scope, "variable");

        if(t_expr->sym==NULL){
            insert(&head_scope, *symbol_table, temp_name, scope, 0, $3->numline, FORMAL, 0, NULL);
            t_expr->sym=lookup(*symbol_table, temp_name, scope, "variable");
        }


        int temp_val=currQuad;
        int l1, l2, l3, l4;
        l4=temp_val+4;
        l1=(temp_val += 2);
        l2=(temp_val += 2);
        l3=temp_val + 1;

        quads=emit(quads, if_eq, NULL ,arg1 ,arg2, l1, $2->numline);
        quads=emit(quads, jump, NULL ,NULL ,NULL, l2, $2->numline);
        quads=emit(quads, assign_, t_expr ,true_expr ,NULL , 0, $2->numline);
        quads=emit(quads, jump, NULL ,NULL ,NULL, l3, $2->numline);
        quads=emit(quads, assign_, t_expr ,false_expr ,NULL , 0, $2->numline);

        struct alpha_token_t *ret_token=malloc(sizeof(struct alpha_token_t));
        ret_token->type="IDENTIFIER";
        ret_token->numline=0;
        ret_token->content=temp_name;
        inop=0;
        $$=ret_token;

    }
    | term {$$=$1;}
    ;
term: PUNC_LEFT_PAR expr PUNC_RIGHT_PAR {$$=$2;}
    |OPERAND_MINUS expr         { 
            if(strcmp($2->type, "table")==0){
                $2 = complete_emit($2);
            }
            if(isFunction(*symbol_table, id_temp)==1){printf("Error in line %d/Cant perform operation on a function\n", $1->numline);}
            char* temp_name=malloc(20*sizeof(char));
            if(last_name==NULL){
                temp_name=newtempname();
                last_name=temp_name;
            }
            else{
                temp_name=last_name;
            }

            Binding *tmp=lookuptemp(head_scope, temp_name);

            if(tmp==NULL){
                insert(&head_scope, *symbol_table, temp_name, scope, 0, 0, LOCAL, 0, NULL);
                tmp=lookup(*symbol_table, temp_name, scope, "variable");
            }

            expr* t_expr= malloc(sizeof(expr));
            t_expr->sym=tmp;
            t_expr->isConst=0;
            t_expr->type=var_e;
            last_temp=t_expr;

            expr* arg1=malloc(sizeof(expr));
            
            if(strcmp($2->type, "INTEGER")==0){
                arg1->type=constnum_e;
                arg1->numConst=atoi($2->content);
            }
            
            else if(strcmp($2->type, "FLOAT")==0){
                arg1->type=constnum_e;
                arg1->numConst=atof($2->content);
            }
            
            else if(strcmp($2->type, "STRING")==0){
                arg1->type=conststring_e;
                arg1->strConst=$2->content;
            }
            
            else if(strcmp($2->type, "BOOLEAN")==0){
                arg1->type=constbool_e;
                arg1->strConst=$2->content;
            }
            else{
                arg1->type=var_e;
                arg1->sym=lookup(*symbol_table, $2->content, scope, "variable");
                if(arg1->sym==NULL){
                    insert(&head_scope, *symbol_table, $2->content, scope, 0, line, FORMAL, 0, NULL);
                    arg1->sym=lookup(*symbol_table, $2->content, scope, "variable");
                }
            }


            quads=emit(quads, uminus,t_expr,arg1 ,NULL, 0, $2->numline);

            struct alpha_token_t *ret_token=malloc(sizeof(struct alpha_token_t));
            ret_token->type="IDENTIFIER";
            ret_token->numline=0;
            ret_token->content=temp_name;
            inop=0;
            $$=ret_token;
        }
    |KEYWORD_NOT {not_counter++;}expr{
            
            if(strcmp($3->type, "table")==0){
                $3 = ($3);
            }
            if(isFunction(*symbol_table, id_temp)==1){printf("Error in line %d/Cant perform operation on a function\n", $1->numline);}
            char* temp_name=malloc(20*sizeof(char));
            if(last_name==NULL){
                temp_name=newtempname();
                last_name=temp_name;
            }
            else{
                temp_name=last_name;
            }
             Binding *tmp=lookuptemp(head_scope, temp_name);

            if(tmp==NULL){
                insert(&head_scope, *symbol_table, temp_name, scope, 0, 0, LOCAL, 0, NULL);
                tmp=lookup(*symbol_table, temp_name, scope, "variable");
            }
            expr* t_expr= malloc(sizeof(expr));
            t_expr->sym=tmp;
            t_expr->isConst=0;
            t_expr->type=var_e;
            last_temp=t_expr;

            expr* true_expr= malloc(sizeof(expr));
            true_expr->sym=NULL;
            true_expr->isConst=1;
            true_expr->type=constbool_e;
            true_expr->strConst="true";

            expr* false_expr= malloc(sizeof(expr));
            false_expr->sym=NULL;
            false_expr->isConst=1;
            false_expr->type=constbool_e;
            false_expr->strConst="false";

            expr* arg1=malloc(sizeof(expr));
            
            arg1=token_to_expr(&head_scope, *symbol_table, $3, scope);

            //printf("Not counter: %d\n", not_counter);
            int l1, l2, l3, l4;
            l1=l2=l3=l4=currQuad;
            l1+=3;
            l2+=4;
            quads=emit(quads, if_eq, NULL, arg1, true_expr, l1, $3->numline);
            quads=emit(quads, assign_, t_expr, true_expr, NULL, 0, $3->numline);
            quads=emit(quads, jump, NULL, NULL, NULL, l1+1, $3->numline);
            quads=emit(quads, assign_, t_expr, false_expr, NULL, 0, $3->numline);
           
            
        
            struct alpha_token_t *ret_token=malloc(sizeof(struct alpha_token_t));
            ret_token->type="IDENTIFIER";
            ret_token->numline=0;
            ret_token->content=temp_name;
            inop=1;
            $$=ret_token;

            }
    |OPERAND_PLUS_PLUS lvalue   {
        char* temp_name;
        expr* t_expr;
        if(strcmp($2->type, "table")==0){
            $2 = complete_emit($2);
        }
        if(isFunction(*symbol_table, id_temp)==1){printf("Error in line %d/Cant perform operation on a funtion\n", $1->numline);}
        if(strcmp($2->type, "table")!=0){
            temp_name=malloc(20*sizeof(char));
                if(last_name==NULL){
                    temp_name=newtempname();
                    last_name=temp_name;
                }
                else{
                    temp_name=last_name;
                }
                Binding *tmp=lookuptemp(head_scope, temp_name);

                if(tmp==NULL){
                    insert(&head_scope, *symbol_table, temp_name, scope, 0, 0, LOCAL, 0, NULL);
                    tmp=lookup(*symbol_table, temp_name, scope, "variable");
                }

                t_expr= malloc(sizeof(expr));
                t_expr->sym=tmp;
                t_expr->isConst=0;
                t_expr->type=var_e;
                last_temp=t_expr;
        }
        expr* arg1=malloc(sizeof(expr));
            
            arg1=term_token_to_expr(&head_scope, *symbol_table, $2, scope);
            
            expr* incr_expr= malloc(sizeof(expr));
            incr_expr->sym=NULL;
            incr_expr->isConst=1;
            incr_expr->type=constnum_e;
            incr_expr->numConst=1;

            quads=emit(quads, add, arg1, arg1, incr_expr, 0, $2->numline);
            if(strcmp($2->type, "table")==0){
                quads=emit(quads, tablesetelem, $2->prev_expr, $2->prev_expr->index, $2->expression, 0, $2->numline);
            }else{   
                quads=emit(quads, assign_, t_expr, arg1, NULL, 0, $2->numline);
            }

            struct alpha_token_t *ret_token=malloc(sizeof(struct alpha_token_t));
            ret_token->type="IDENTIFIER";
            ret_token->numline=0;
            if(strcmp($2->type, "table")==0){
                ret_token->content=$2->content;
            }else{   
                ret_token->content=temp_name;
            }
            inop=0;
            $$=ret_token;

    }
    |lvalue OPERAND_PLUS_PLUS   {
        char* temp_name;
        expr* t_expr;
        if(strcmp($1->type, "table")==0){
            $1 = complete_emit($1);
        }
        if(isFunction(*symbol_table, id_temp)==1){printf("Error in line %d/Cant perform operation on a funtion\n", $2->numline);}
        
            temp_name=malloc(20*sizeof(char));
                if(last_name==NULL){
                    temp_name=newtempname();
                    last_name=temp_name;
                }
                else{
                    temp_name=last_name;
                }
                Binding *tmp=lookuptemp(head_scope, temp_name);

                if(tmp==NULL){
                    insert(&head_scope, *symbol_table, temp_name, scope, 0, 0, LOCAL, 0, NULL);
                    tmp=lookup(*symbol_table, temp_name, scope, "variable");
                }

                t_expr= malloc(sizeof(expr));
                t_expr->sym=tmp;
                t_expr->isConst=0;
                t_expr->type=var_e;
                last_temp=t_expr;
        
            expr* arg1=malloc(sizeof(expr));
            
            arg1=term_token_to_expr(&head_scope, *symbol_table, $1, scope);
            
            expr* incr_expr= malloc(sizeof(expr));
            incr_expr->sym=NULL;
            incr_expr->isConst=1;
            incr_expr->type=constnum_e;
            incr_expr->numConst=1;
            if(strcmp($1->type, "table")==0){
                quads=emit(quads, assign_, t_expr, $1->expression, NULL, 0, $1->numline);
                quads=emit(quads, add, $1->expression, $1->expression, incr_expr, 0, $1->numline);
                quads=emit(quads, tablesetelem, $1->prev_expr, $1->prev_expr->index, $1->expression, 0, $1->numline);
            }else{   
                quads=emit(quads, assign_, t_expr, arg1, NULL, 0, $1->numline);
                quads=emit(quads, add, arg1, arg1, incr_expr, 0, $1->numline);
            }
            
            struct alpha_token_t *ret_token=malloc(sizeof(struct alpha_token_t));
            ret_token->type="IDENTIFIER";
            ret_token->numline=0;
            ret_token->content=temp_name;
            inop=0;
            $$=ret_token;

        }
    |OPERAND_MINUS_MINUS lvalue {
        char* temp_name;
        Binding *tmp;
        expr* t_expr;
        if(strcmp($2->type, "table")==0){
            $2 = complete_emit($2);
        }
        if(isFunction(*symbol_table, id_temp)==1){
            printf("Error in line %d/Cant perform operation on a funtion\n", $1->numline);
        }
        if(isFunction(*symbol_table, id_temp)==1){printf("Error in line %d/Cant perform operation on a funtion\n", $1->numline);}
        if(strcmp($2->type, "table")!=0){
            temp_name=malloc(20*sizeof(char));
                if(last_name==NULL){
                    temp_name=newtempname();
                    last_name=temp_name;
                }
                else{
                    temp_name=last_name;
                }
                tmp=lookuptemp(head_scope, temp_name);

                if(tmp==NULL){
                    insert(&head_scope, *symbol_table, temp_name, scope, 0, 0, LOCAL, 0, NULL);
                    tmp=lookup(*symbol_table, temp_name, scope, "variable");
                }

                t_expr= malloc(sizeof(expr));
                t_expr->sym=tmp;
                t_expr->isConst=0;
                t_expr->type=var_e;
                last_temp=t_expr;
        }
        expr* arg1=malloc(sizeof(expr));
           
            arg1=term_token_to_expr(&head_scope, *symbol_table, $2, scope);
            
            expr* incr_expr= malloc(sizeof(expr));
            incr_expr->sym=NULL;
            incr_expr->isConst=1;
            incr_expr->type=constnum_e;
            incr_expr->numConst=1;

            quads=emit(quads, sub, arg1, arg1, incr_expr, 0, $2->numline);
            if(strcmp($2->type, "table")==0){
                quads=emit(quads, tablesetelem, $2->prev_expr, $2->prev_expr->index, $2->expression, 0, $2->numline);
            }else{   
                quads=emit(quads, assign_, t_expr, arg1, NULL, 0, $2->numline);
            }
            struct alpha_token_t *ret_token=malloc(sizeof(struct alpha_token_t));
            ret_token->type="IDENTIFIER";
            ret_token->numline=0;
            if(strcmp($2->type, "table")==0){
                ret_token->content=$2->content;
            }else{   
                ret_token->content=temp_name;
            }
            inop=0;
            $$=ret_token;
    }
    |lvalue OPERAND_MINUS_MINUS {
        char* temp_name;
        Binding *tmp;
        expr* t_expr;
        if(strcmp($1->type, "table")==0){
            $1 = complete_emit($1);
        }
        if(isFunction(*symbol_table, id_temp)==1){printf("Error in line %d/Cant perform operation on a funtion\n", $2->numline);}

            temp_name=malloc(20*sizeof(char));
                if(last_name==NULL){
                    temp_name=newtempname();
                    last_name=temp_name;
                }
                else{
                    temp_name=last_name;
                }
                tmp=lookuptemp(head_scope, temp_name);

                if(tmp==NULL){
                    insert(&head_scope, *symbol_table, temp_name, scope, 0, 0, LOCAL, 0, NULL);
                    tmp=lookup(*symbol_table, temp_name, scope, "variable");
                }

                t_expr= malloc(sizeof(expr));
                t_expr->sym=tmp;
                t_expr->isConst=0;
                t_expr->type=var_e;
                last_temp=t_expr;
        
        expr* arg1=malloc(sizeof(expr));
           
           arg1=term_token_to_expr(&head_scope, *symbol_table, $1, scope);
            
            expr* incr_expr= malloc(sizeof(expr));
            incr_expr->sym=NULL;
            incr_expr->isConst=1;
            incr_expr->type=constnum_e;
            incr_expr->numConst=1;

            if(strcmp($1->type, "table")==0){
                quads=emit(quads, assign_, t_expr, $1->expression, NULL, 0, $1->numline);
                quads=emit(quads, sub, $1->expression, $1->expression, incr_expr, 0, $1->numline);
                quads=emit(quads, tablesetelem, $1->prev_expr, $1->prev_expr->index, $1->expression, 0, $1->numline);
            }else{   
                quads=emit(quads, assign_, t_expr, arg1, NULL, 0, $1->numline);
                quads=emit(quads, sub, arg1, arg1, incr_expr, 0, $1->numline);
            }
            

            struct alpha_token_t *ret_token=malloc(sizeof(struct alpha_token_t));
            ret_token->type="IDENTIFIER";
            ret_token->numline=0;
            ret_token->content=temp_name;
            inop=0;
            $$=ret_token;
    
    }
    |primary{$$=$1;}
    ;
assginexpr: lvalue OPERAND_ASSIGN expr{

        expr* result;
  if(strcmp($1->type, "table")==0 || strcmp($3->type, "table")==0){
        if(strcmp($3->type, "table")==0){
            $3 = complete_emit($3);
        }
        int i;
        Binding* b;
        $$=$3;
        
        
        if(strcmp($1->type, "table")!=0){
            printf("$1: %s\n",$1->type);
            b=lookup(*symbol_table, $1->content, scope, "variable");
            if(b!=NULL){

                if(b->isFunction==0){
                    /**/
                }
                else{
                    printf("Error\n");
                }
            }    
            else{
                insert(&head_scope, *symbol_table, $1->content, scope, 0, $1->numline, FORMAL, 0, NULL);
            }
        }
        
        if(strcmp($3->type, "OBJECTDEF") == 0){
            /**/
        }
        else if(strcmp($3->type, "INTEGER")&&strcmp($3->type, "FLOAT")&&strcmp($3->type, "STRING")&&strcmp($3->type, "KEYWORD_NILL")&&strcmp($3->type, "KEYWORD_FALSE")&&strcmp($3->type, "KEYWORD_TRUE")){ 
            b=lookup(*symbol_table, $3->content, scope, "variable");
            if(b==NULL){
                insert(&head_scope, *symbol_table, $3->content, scope, 0, $3->numline, FORMAL, 0, NULL);
            }
            else{
                if(b->isFunction==0){
                    /**/
                }
                else{
                    printf("ERROR in line %d/in variable %s Cannot assign variable to funciton \n",$1->numline, $1->content);
                }
            }
        }

        
        if( $1->content!=NULL){
            result=malloc(sizeof(expr));
            result->sym=lookup(*symbol_table, $1->content, scope, "variable");
            //assert(result->sym);
            result->index=NULL;
            result->strConst=NULL;
            result->next=NULL;
        }

        expr* arg1=malloc(sizeof(expr));
        if(assign_counter==0){
            
            if(!strcmp($3->type,"STRING")){
                arg1->strConst=$3->content;
                arg1->type=conststring_e;
            }
            else if(!strcmp($3->type, "INTEGER")||!strcmp($3->type, "FLOAT")){
                arg1->numConst=atof($3->content);
                arg1->type=constnum_e;
            }
            else if((strcmp($3->type, "KEYWORD_FALSE")==0)||(strcmp($3->type, "KEYWORD_TRUE")==0)){
                arg1->strConst=$3->content;
                arg1->type=constbool_e;
            }
            
            else{
                arg1->strConst=$3->content;
                arg1->sym=lookup(*symbol_table, $3->content, scope, "variable");
                arg1->type=var_e;
            }
            if(strcmp($1->type, "table")!=0){
                quads= emit(quads, assign_, result, arg1, NULL,3,0);
            }
            else{
                quads = emit(quads, tablesetelem, $1->expression, $1->expression->index, arg1,3,0);
            }
        }
        else{
            if(strcmp($1->type, "table")!=0){
                quads= emit(quads, assign_, result, last_temp, NULL,3,0);
            }
            else{
                assert($1->expression);
                quads = emit(quads, tablesetelem, $1->expression, $1->expression->index, arg1,3,0);
            }
        }
        char* temp_name=malloc(20*sizeof(char*));
        temp_name=newtempname();
        Binding *tmp=lookuptemp(head_scope, temp_name);

        if(tmp==NULL){
            insert(&head_scope, *symbol_table, temp_name, scope, 0, 0, LOCAL, 0, NULL);
            tmp=lookup(*symbol_table, temp_name, scope, "variable");
        }
        expr* t_expr= malloc(sizeof(expr));
        t_expr->sym=tmp;
        t_expr->type=var_e;
        last_temp=t_expr;
        if(strcmp($1->type, "table")!=0){
            quads = emit(quads, assign_, t_expr, result, NULL,3,0); 
        }
        else{
            quads = emit(quads, tablegetelem, t_expr, (quads+(currQuad-1))->result, (quads+(currQuad-1))->arg1,3,0); 
        }
        
        assign_counter++;
    }
    else{
        int i;
    $$=$3;
    Binding* b=lookup(*symbol_table, $1->content, scope, "variable");
    if(b!=NULL){
        if(b->isFunction==0){
            /**/
        }
        else{
            printf("Error\n");
        }
    }    
    else{
        insert(&head_scope, *symbol_table, $1->content, scope, 0, $1->numline, FORMAL, 0, NULL);
    }
        if(strcmp($3->type, "OBJECTDEF") == 0){
            /**/
        }
        else if(strcmp($3->type, "INTEGER")&&strcmp($3->type, "FLOAT")&&strcmp($3->type, "STRING")&&strcmp($3->type, "KEYWORD_NILL")&&strcmp($3->type, "KEYWORD_FALSE")&&strcmp($3->type, "KEYWORD_TRUE")){ 
            b=lookup(*symbol_table, $3->content, scope, "variable");
            if(b==NULL){
                insert(&head_scope, *symbol_table, $3->content, scope, 0, $3->numline, FORMAL, 0, NULL);
            }
            else{
                if(b->isFunction==0){
                    /**/
                }
                else{
                    printf("ERROR in line %d/in variable %s Cannot assign variable to funciton \n",$1->numline, $1->content);
                }
            }
        }
    expr* result=malloc(sizeof(expr));
    result->sym=lookup(*symbol_table, $1->content, scope, "variable");
    assert(result->sym);
    result->index=NULL;
    result->strConst=NULL;
    result->next=NULL;
    
    
    if(assign_counter==0){
        expr* arg1=malloc(sizeof(expr));
        if(!strcmp($3->type,"STRING")){
            arg1->strConst=$3->content;
            arg1->type=conststring_e;
        }
        else if(!strcmp($3->type, "INTEGER")||!strcmp($3->type, "FLOAT")){
            arg1->numConst=atof($3->content);
            arg1->type=constnum_e;
        }
        else if((strcmp($3->type, "KEYWORD_FALSE")==0)||(strcmp($3->type, "KEYWORD_TRUE")==0)){
            arg1->strConst=$3->content;
            arg1->type=constbool_e;
        }
        else{
            arg1->strConst=$3->content;
            arg1->sym=lookup(*symbol_table, $3->content, scope, "variable");
            arg1->type=var_e;
        }
        emit(quads, assign_, result, arg1, NULL,3,0);
    }
    else{
         emit(quads, assign_, result, last_temp, NULL,3,0);
    }

    char* temp_name=malloc(20*sizeof(char*));
    temp_name=newtempname();
    Binding *tmp=lookuptemp(head_scope, temp_name);

    if(tmp==NULL){
        insert(&head_scope, *symbol_table, temp_name, scope, 0, 0, LOCAL, 0, NULL);
        tmp=lookup(*symbol_table, temp_name, scope, "variable");
    }
    expr* t_expr= malloc(sizeof(expr));
    t_expr->sym=tmp;
    t_expr->type=var_e;
    last_temp=t_expr;
    emit(quads, assign_, t_expr, result, NULL,3,0); 
    assign_counter++;

    }


};

primary: lvalue {$$=$1;}
    | call{func_call=0;}
    | objectdef{$$=$1;}
    | PUNC_LEFT_PAR funcdef PUNC_RIGHT_PAR{$$ = $2;}
    | const {$$=$1;}
    ;
lvalue: IDENTIFIER {
                    
                    id_temp=malloc(strlen($1->content)*sizeof(char)); strcpy(id_temp, $1->content);
                    }
    | KEYWORD_LOCAL IDENTIFIER {
                    $$=$2;
                    Binding* b=lookup(*symbol_table, $1->content, scope, "variable");
                    if(b == NULL){ 
                        insert(&head_scope, *symbol_table, $2->content, scope, 0, $2->numline, LOCAL, 0, NULL);
                    }
                    else if(b->isFunction == 0 && b->value.varVal->scope == scope){
                         /**/
                    }
                    else if(b != NULL && b->type != LIBFUNC){ 
                        insert(&head_scope, *symbol_table, $2->content, scope, 0, $2->numline, LOCAL, 0, NULL);
                    }
                    else{
                        printf("Error in line %d/Cannot declare library function name as variable\n", $2->numline);
                    }
                    }
    | PUNC_DOUBLE_COLON IDENTIFIER{
        $$=$2;
        Binding* b=lookup(*symbol_table, $2->content, 0, "variable");
        if(b==NULL){
            printf("Error in line %d Variable not global\n", $2->numline);
        }
        else{
            /**/
        }
    } 
    | member {$$=$1;}
    ;
member: lvalue PUNC_DOT IDENTIFIER {
        expr* l_expr, *id_expr, *t_expr;
        struct alpha_token_t* ret;
        //init_expr
        id_expr=malloc(sizeof(expr));
        id_expr->type=tableitem_e;
        id_expr->strConst=$3->content;
        ret=malloc(sizeof(struct alpha_token_t));
        //ret->expression=lvalue_head;
        ret->expression=id_expr;
        ret->numline=$1->numline;
        ret->type="table";
        if($1->expression == NULL){
            l_expr=malloc(sizeof(expr));
            l_expr->type=var_e;
            l_expr->sym=lookup(*symbol_table, $1->content, scope, "variable");
            if(l_expr->sym==NULL){
                insert(&head_scope, *symbol_table, $1->content, scope, 0, $1->numline, FORMAL, 0, NULL);
                l_expr->sym=lookup(*symbol_table, $1->content, scope, "variable");
                assert( l_expr->sym);
            }
            l_expr->index = id_expr;
            ret->expression = l_expr;
        }
        else{
            char* temp_name=malloc(20*sizeof(char));
            temp_name=newtempname();
            t_expr=malloc(sizeof(expr));
            t_expr->type=var_e;
            if(lookup(*symbol_table, temp_name, 0, "variable")==NULL){
                insert(&head_scope, *symbol_table, temp_name, 0, 0, $1->numline, GLOBAL, 0, NULL);
            }
            t_expr->sym=lookup(*symbol_table, temp_name, 0, "variable");
            assert( t_expr->sym);
            ret->content=temp_name;
            quads= emit(quads, tablegetelem, t_expr, $1->expression, $1->expression->index,0,$1->numline);
            t_expr->index = id_expr;
            ret->expression = t_expr;
        }
        $$=ret;
    }
    | lvalue PUNC_LEFT_BRACK expr PUNC_RIGHT_BRACK{
        expr* l_expr, *id_expr, *t_expr;
        struct alpha_token_t* ret;
        //init_expr
        ret=malloc(sizeof(struct alpha_token_t));
        //ret->expression=lvalue_head;
        id_expr=malloc(sizeof(expr));
        //id_expr->type=tableitem_e;
        if(!strcmp($3->type,"STRING")){
            id_expr->strConst=$3->content;
            id_expr->type=conststring_e;
        }
        else if(!strcmp($3->type, "INTEGER")||!strcmp($3->type, "FLOAT")){
          
            id_expr->numConst=atof($3->content);
            id_expr->type=constnum_e;
        }
        else if((strcmp($3->type, "KEYWORD_FALSE")==0)||(strcmp($3->type, "KEYWORD_TRUE")==0)){
            id_expr->strConst=$3->content;
            id_expr->type=constbool_e;
        }
        else{
            id_expr->strConst=$3->content;
            id_expr->sym=lookup(*symbol_table, $3->content, scope, "variable");
            if(id_expr->sym==NULL){
                insert(&head_scope, *symbol_table, $3->content, scope, 0, $3->numline, FORMAL, 0, NULL);
                id_expr->sym=lookup(*symbol_table, $3->content, scope, "variable");
                assert(id_expr->sym);
            }
            id_expr->type=var_e;
        }
        //id_expr->strConst=$3->content;
        ret->expression=id_expr;
        ret->numline=$1->numline;
        ret->type="table";
        if($1->expression == NULL){
            l_expr=malloc(sizeof(expr));
            l_expr->type=var_e;
            l_expr->sym=lookup(*symbol_table, $1->content, scope, "variable");
            if(l_expr->sym==NULL){
                insert(&head_scope, *symbol_table, $1->content, scope, 0, $1->numline, FORMAL, 0, NULL);
                l_expr->sym=lookup(*symbol_table, $1->content, scope, "variable");
                assert(l_expr->sym);
            }
            l_expr->index = id_expr;
            ret->expression = l_expr;
        }
        else{
            char* temp_name=malloc(20*sizeof(char));
            temp_name=newtempname();
            t_expr=malloc(sizeof(expr));
            t_expr->type=var_e;
            if(lookup(*symbol_table, temp_name, scope, "variable")==NULL){
                insert(&head_scope, *symbol_table, temp_name, scope, 0, $1->numline, GLOBAL, 0, NULL);
            }
            t_expr->sym=lookup(*symbol_table, temp_name, scope, "variable");
            assert( t_expr->sym);
            ret->content=temp_name;
            quads= emit(quads, tablegetelem, t_expr, $1->expression, $1->expression->index,0,$1->numline);
            t_expr->index = id_expr;
            ret->expression = t_expr;
        }
        $$=ret; 
    }
    | call PUNC_DOT IDENTIFIER{
        //foo().ident
        //expr* l_expr, *t_expr;
        expr *id_expr ;
        struct alpha_token_t* ret;
        //init_expr
        id_expr=malloc(sizeof(expr));
        id_expr->type=tableitem_e;
        id_expr->strConst=$3->content;
        ret=malloc(sizeof(struct alpha_token_t));
        //ret->expression=lvalue_head;
        //ret->expression=id_expr;
        ret->numline=$1->numline;
        ret->type="table";
        $1->expression->index = id_expr;
        ret->expression = $1->expression;
        ret->content=$1->content;
        $$=ret;
    }
    | call PUNC_LEFT_BRACK expr PUNC_RIGHT_BRACK{ 
        expr *id_expr ;
        struct alpha_token_t* ret;
        id_expr=malloc(sizeof(expr));
        if(!strcmp($3->type,"STRING")){
            id_expr->strConst=$3->content;
            id_expr->type=conststring_e;
        }
        else if(!strcmp($3->type, "INTEGER")||!strcmp($3->type, "FLOAT")){
         
            id_expr->numConst=atof($3->content);
            id_expr->type=constnum_e;
        }
        else if((strcmp($3->type, "KEYWORD_FALSE")==0)||(strcmp($3->type, "KEYWORD_TRUE")==0)){
            id_expr->strConst=$3->content;
            id_expr->type=constbool_e;
        }
        else{
            id_expr->strConst=$3->content;
            id_expr->sym=lookup(*symbol_table, $3->content, scope, "variable");
            if(id_expr->sym==NULL){
                insert(&head_scope, *symbol_table, $3->content, scope, 0, $3->numline, FORMAL, 0, NULL);
                id_expr->sym=lookup(*symbol_table, $3->content, scope, "variable");
            }
            id_expr->type=var_e;
        }
        ret=malloc(sizeof(struct alpha_token_t));
        ret->numline=$1->numline;
        ret->type="table";
        $1->expression->index = id_expr;
        ret->expression = $1->expression;
        ret->content=$1->content;
        $$=ret;
    }
    ;

call: call PUNC_LEFT_PAR elist PUNC_RIGHT_PAR{
 
    expr* e_expr=malloc(sizeof(expr));
    e_expr->type=programfunc_e;
    e_expr->sym=lookup(*symbol_table, $1->content, scope, "variable");
    if( e_expr->sym==NULL){
        insert(&head_scope, *symbol_table, $1->content, 0, 0, $1->numline, GLOBAL, 0, NULL);
         e_expr->sym=lookup(*symbol_table, $1->content, scope, "variable");
    }
    quads= emit(quads, call, e_expr, NULL, NULL,0,$1->numline);

    char* temp_name=malloc(20*sizeof(char*));
    temp_name=newtempname();

    Binding *tmp=lookuptemp(head_scope, temp_name);

    if(tmp==NULL){
        insert(&head_scope, *symbol_table, temp_name, scope, 0, 0, LOCAL, 0, NULL);
        tmp=lookup(*symbol_table, temp_name, scope, "variable");
    }

    expr* t_expr= malloc(sizeof(expr));
    t_expr->sym=tmp;
    t_expr->type=var_e;
    quads= emit(quads, getretval, t_expr, NULL, NULL,0,$1->numline);

    struct alpha_token_t *ret_token=malloc(sizeof(struct alpha_token_t));
    ret_token->type="IDENTIFIER";
    ret_token->numline=0;
    ret_token->expression=t_expr;
    ret_token->content=temp_name;
    $$=ret_token;   

} 
    | lvalue{func_call=1;} callsuffix {
            Binding * lib_func=lookup(*symbol_table, $1->content, 0, "variable");
            int lib=0;
            if(lib_func!=NULL&&lib_func->type==LIBFUNC)
            {
                l_counter++;
                lib=1;
            }
            char* temp_name;
            expr* t_expr, *l_expr, *id_expr;
            struct alpha_token_t* ret;
        if(strcmp($1->type, "table")==0){
            $1 = complete_emit($1);
        }
        if(strcmp($3->type, "METHODCALL")==0){
            id_expr=malloc(sizeof(expr));
            id_expr->type=tableitem_e;
            id_expr->strConst=$3->content;
            ret=malloc(sizeof(struct alpha_token_t));
            ret->expression=id_expr;
            ret->numline=$1->numline;
            ret->type="table";
            if($1->expression == NULL){
                l_expr=malloc(sizeof(expr));
                l_expr->type=var_e;
                l_expr->sym=lookup(*symbol_table, $1->content, scope, "variable");
                if(l_expr->sym==NULL){
                    insert(&head_scope, *symbol_table, $1->content, scope, 0, $1->numline, FORMAL, 0, NULL);
                    l_expr->sym=lookup(*symbol_table, $1->content, scope, "variable");
                    assert( l_expr->sym);
                }
                l_expr->index = id_expr;
                ret->expression = l_expr;
            }
            else{
                char* temp_name=malloc(20*sizeof(char));
                temp_name=newtempname();
                t_expr=malloc(sizeof(expr));
                t_expr->type=var_e;
                if(lookup(*symbol_table, temp_name, 0, "variable")==NULL){
                    insert(&head_scope, *symbol_table, temp_name, 0, 0, $1->numline, GLOBAL, 0, NULL);
                }
                t_expr->sym=lookup(*symbol_table, temp_name, 0, "variable");
                assert( t_expr->sym);
                ret->content=temp_name;
                quads= emit(quads, tablegetelem, t_expr, $1->expression, $1->expression->index,0,$1->numline);
                t_expr->index = id_expr;
                ret->expression = t_expr;
            }
            $1=ret;
            $1 = complete_emit($1);
            quads = emit(quads, param, $1->prev_expr, NULL, NULL,0,$1->numline);
        }
        
        /*expr* t_expr;*/
        Binding* b=lookup(*symbol_table, $1->content, scope, "variable");
        temp_name=malloc(20*sizeof(char*));
        temp_name=newtempname();
        if(b!=NULL){
            expr* c_expr=malloc(sizeof(expr));
            if(lib==0){
                c_expr->type=programfunc_e;
            }
            else{
                c_expr->type=libraryfunc_e;
            }
            c_expr->sym=b;
            
            if((strcmp($3->type, "METHODCALL")==0)){
                quads= emit(quads, call, $1->expression, NULL, NULL,0,$1->numline);
            }else{
                // printf("%s\n", b->value.funcVal->name);
                quads= emit(quads, call, c_expr, NULL, NULL,0,$1->numline);
            }
            Binding *tmp=lookup(*symbol_table, temp_name, scope, "variable");

            if(tmp==NULL){
                insert(&head_scope, *symbol_table, temp_name, scope, 0, 0, LOCAL, 0, NULL);
                tmp=lookup(*symbol_table, temp_name, scope, "variable");
            }
            t_expr= malloc(sizeof(expr));
            t_expr->sym=tmp;
            t_expr->type=var_e;
            quads= emit(quads, getretval, t_expr, NULL, NULL,0,$1->numline);
          
            // if((call_arg_func==1)){
            //     quads= emit(quads, param, t_expr, NULL, NULL,0,$1->numline);
            // }
            struct alpha_token_t *ret_token=malloc(sizeof(struct alpha_token_t));
            ret_token->type="IDENTIFIER";
            ret_token->numline=0;
            ret_token->content=temp_name;
            $$=ret_token;
        }
        else if(b==NULL){
            printf("ERROR in line %d/Function %s doesn't exist \n",$1->numline, $1->content);
        }
        
        struct alpha_token_t *ret_token=malloc(sizeof(struct alpha_token_t));
        ret_token->type="IDENTIFIER";
        ret_token->numline=0;
        ret_token->content=temp_name;
        ret_token->expression=t_expr;
        $$=ret_token;
        
    }
    | PUNC_LEFT_PAR funcdef PUNC_RIGHT_PAR PUNC_LEFT_PAR elist PUNC_RIGHT_PAR
    ;
callsuffix: normcall{call_arg_func=1;}
    | methodcall{$$=$1;}
    ;
normcall: PUNC_LEFT_PAR elist PUNC_RIGHT_PAR{
   
};
methodcall: PUNC_DOUBLE_DOT IDENTIFIER PUNC_LEFT_PAR elist PUNC_RIGHT_PAR{
    expr *id_expr;
    struct alpha_token_t* ret;
    id_expr=malloc(sizeof(expr));
    id_expr->type=tableitem_e;
    id_expr->strConst=$2->content;
    ret=malloc(sizeof(struct alpha_token_t));
    ret->numline=$2->numline;
    ret->type="METHODCALL";
    ret->expression = id_expr;
    ret->content=$2->content;
    $$=ret;
}; 

elist: {}
    |expr {
        if(strcmp($1->type, "table")==0){
            $1 = complete_emit($1);
        }
        expr_head* new_list;
        expr* arg_expr=malloc(sizeof(expr));
        Binding* b=lookup(*symbol_table, $1->content, scope, "variable");
        if(b!=NULL){
            if(b->isFunction){
                if(b->type!=LIBFUNC){
                    arg_expr->type=programfunc_e;
                }
                else{
                    arg_expr->type=libraryfunc_e;
                }
            }
            else if(!strcmp($1->type, "IDENTIFIER")){
                arg_expr->type=var_e;
            }
            arg_expr->sym=b;
        }
        else{
            if(!strcmp($1->type, "IDENTIFIER")){
                arg_expr->type=var_e;
                if(lookup(*symbol_table, $1->content, scope, "variable")==NULL){
                    insert(&head_scope, *symbol_table, $1->content, scope, 0, 0, FORMAL, 0, NULL);
                }
                arg_expr->sym=lookup(*symbol_table, $1->content, scope, "variable");
            }
            else if(!strcmp($1->type, "STRING")){
                arg_expr->type=conststring_e;
                arg_expr->strConst=$1->content;
            }
            else if((!strcmp($1->type, "KEYWORD_TRUE"))||(!strcmp($1->type, "KEYWORD_FALSE"))){
                arg_expr->type=constbool_e;
                arg_expr->strConst=$1->content;
            }
            else if(!strcmp($1->type, "INTEGER")){
                arg_expr->type=constnum_e;
                arg_expr->numConst=atoi($1->content);
            }
            else{
                arg_expr->type=constnum_e;
                arg_expr->numConst=atof($1->content);
            }

        }
        if(func_call){
            quads=emit(quads, param, arg_expr, NULL, NULL,0,$1->numline);
            
        }
        if(list_create){
            struct alpha_token_t* token= malloc(sizeof( struct alpha_token_t));
            token->expression=arg_expr;
            token->type="variable";
            token->numline=$1->numline;
            token->content=$1->content;
            $$=token;
            new_list = malloc(sizeof(expr_head));
            new_list->head = arg_expr;
            if(list_head != NULL){
                new_list->last = list_head;
            }
            list_head = new_list;
        }
        $$=$1;
    }
    |expr PUNC_COM elist {
        if(strcmp($1->type, "table")==0){
            $1 = complete_emit($1);
        }
        expr* arg_expr=malloc(sizeof(expr));
        Binding* b=lookup(*symbol_table, $1->content, scope, "variable");
        if(b!=NULL){
            if(b->isFunction){
                  if(b->type!=LIBFUNC){
                    arg_expr->type=programfunc_e;
                }
                else{
                    arg_expr->type=libraryfunc_e;
                }
            }
            else if(!strcmp($1->type, "IDENTIFIER")){
                arg_expr->type=var_e;
            }
            arg_expr->sym=b;
        }
        else{
            if(!strcmp($1->type, "IDENTIFIER")){
                arg_expr->type=var_e;
                if(lookup(*symbol_table, $1->content, scope, "variable")==NULL){
                    insert(&head_scope, *symbol_table, $1->content, scope, 0, 0, FORMAL, 0, NULL);
                }
                arg_expr->sym=lookup(*symbol_table, $1->content, scope, "variable");
            }
            else if(!strcmp($1->type, "STRING")){
                arg_expr->type=conststring_e;
                arg_expr->strConst=$1->content;
            }
            else if((!strcmp($1->type, "KEYWORD_TRUE"))||(!strcmp($1->type, "KEYWORD_FALSE"))){
                arg_expr->type=constbool_e;
                arg_expr->strConst=$1->content;
            }
            else if(!strcmp($1->type, "INTEGER")){
                arg_expr->type=constnum_e;
                arg_expr->numConst=atoi($1->content);
            }
            else{
                arg_expr->type=constnum_e;
                arg_expr->numConst=atof($1->content);
            }

        }
        if(func_call){
            quads=emit(quads, param, arg_expr, NULL, NULL,0,$1->numline);
        }
        if(list_create){
            struct alpha_token_t* token= malloc(sizeof( struct alpha_token_t));
            token->expression=arg_expr;
            token->type="variable";
            token->numline=$1->numline;
            token->content=$1->content;
            $$=token;
            arg_expr->next = list_head->head;
            list_head->head = arg_expr;
        }
        $$=$1;
    
    }
    ;

objectdef:PUNC_LEFT_BRACK{list_create = 1;} elist PUNC_RIGHT_BRACK {
                int list_counter;
                expr* tmp_expr;
                expr* num_expr=malloc(sizeof(expr));
                list_create=0;
                expr* t=malloc(sizeof(expr));
                t->type=var_e;
                char* temp_name= malloc(20* sizeof(char));
               
                temp_name=newtempname();

                Binding *tmp=lookuptemp(head_scope, temp_name);

                if(tmp==NULL){
                    insert(&head_scope, *symbol_table, temp_name, 0, 0, 0, FORMAL, 0, NULL);
                    tmp=lookup(*symbol_table, temp_name, 0, "variable");
                    assert(tmp);
                }

                t->sym=tmp;

                quads=emit(quads, tablecreate, t, NULL, NULL, 0, 0);

                struct alpha_token_t* token = malloc(sizeof(struct alpha_token_t));
                token->numline=0;
                token->content=temp_name;
                token->type="IDENTIFIER";
                list_counter = 0;
                if(list_head != NULL){
                    for(tmp_expr = list_head->head; tmp_expr != NULL ;tmp_expr = tmp_expr->next){
                        num_expr->type=constnum_e;
                        num_expr->numConst=list_counter;
                        quads=emit(quads, tablesetelem, t, num_expr, tmp_expr, 0, 0);
                        list_counter++;
                        num_expr=malloc(sizeof(expr));
                    }
                    list_head = list_head->last;
                }
                
                $$ = token;
            }
        |PUNC_LEFT_BRACK indexed PUNC_RIGHT_BRACK{
                expr* tmp_expr,*t;
                struct alpha_token_t* token = malloc(sizeof(struct alpha_token_t));
                t=malloc(sizeof(expr));
                t->type=var_e;
                char* temp_name= malloc(20* sizeof(char));
               
                temp_name=newtempname();

                Binding *tmp=lookuptemp(head_scope, temp_name);

                if(tmp==NULL){
                    insert(&head_scope, *symbol_table, temp_name, scope, 0, 0, FORMAL, 0, NULL);
                    tmp=lookup(*symbol_table, temp_name, scope, "variable");
                }

                t->sym=tmp;

                quads=emit(quads, tablecreate, t, NULL, NULL, 0, 0);
                token->numline=0;
                token->numToken=0;
                token->content=temp_name;
                token->type="IDENTIFIER";
                if(list_head != NULL){
                    for(tmp_expr = list_head->head; tmp_expr != NULL ;tmp_expr = tmp_expr->next){
                        //here
                        /*if(tmp_expr->sym !=NULL && tmp_expr->sym->value.funcVal != NULL){
                            printf("\nTTTT: %s\n",tmp_expr->sym->value.funcVal->name);
                        }
                        else if (tmp_expr->sym !=NULL){
                            printf("\n TTT: %s\n",tmp_expr->sym->value.varVal->name);
                        }*/
                        quads=emit(quads, tablesetelem, t, tmp_expr->index, tmp_expr, 0, 0);
                    }
                    list_head = list_head->last;
                }
                
                $$ = token;
            }
        ;
indexed: indexedelem PUNC_COM indexed{
        $1->expression->next = list_head->head;
        list_head->head = $1->expression;
    }
    |indexedelem{
        expr_head* new_list;
        new_list = malloc(sizeof(expr_head));
        new_list->head = $1->expression;
        if(list_head != NULL){
            new_list->last = list_head;
        }
        list_head = new_list;
    }
    ;
indexedelem: PUNC_CURL_LEFT expr PUNC_COLON expr PUNC_CURL_RIGHT{
        if(strcmp($2->type, "table")==0){
                $2 = complete_emit($2);
        }
        if(strcmp($4->type, "table")==0){
                $4 = complete_emit($4);
        }
        expr* index,*indexed;
        Binding *b;
        struct alpha_token_t* token;
        index=malloc(sizeof(expr));
         if(!strcmp($2->type, "IDENTIFIER")){
                index->type=var_e;
                if((b=lookup(*symbol_table, $2->content, scope, "variable"))==NULL){
                    insert(&head_scope, *symbol_table, $2->content, scope, 0, 0, FORMAL, 0, NULL);
                }
                index->sym=lookup(*symbol_table, $2->content, scope, "variable");
                assert(index->sym);
            }
            else if(!strcmp($2->type, "STRING")){
                index->type=conststring_e;
                index->strConst=$2->content;
            }
            else if((!strcmp($2->type, "KEYWORD_TRUE"))||(!strcmp($2->type, "KEYWORD_FALSE"))){
                index->type=constbool_e;
                index->strConst=$2->content;
            }
            else if(!strcmp($2->type, "INTEGER")){
                index->type=constnum_e;
                index->numConst=atoi($2->content);
            }
            else{
                index->type=constnum_e;
                index->numConst=atof($2->content);
            }
            
            indexed=malloc(sizeof(expr));
        
            if(!strcmp($4->type, "IDENTIFIER")){
                indexed->type=var_e;
                if((b=lookup(*symbol_table, $4->content, scope, "variable"))==NULL){
                    insert(&head_scope, *symbol_table, $4->content, scope, 0, 0, FORMAL, 0, NULL);
                }
                indexed->sym=lookup(*symbol_table, $4->content, scope, "variable");
                assert(indexed->sym);
            }
            else if(!strcmp($4->type, "STRING")){
                indexed->type=conststring_e;
                indexed->strConst=$4->content;
            }
            else if((!strcmp($4->type, "KEYWORD_TRUE"))||(!strcmp($4->type, "KEYWORD_FALSE"))){
                indexed->type=constbool_e;
                indexed->strConst=$4->content;
            }
            else if(!strcmp($4->type, "INTEGER")){
                indexed->type=constnum_e;
                indexed->numConst=atoi($4->content);
            }
            else{
                indexed->type=constnum_e;
                indexed->numConst=atof($4->content);
            }
            indexed->index = index;
            token = malloc(sizeof( struct alpha_token_t));
            token->expression=indexed;
            token->type="IDENTIFIER";
            token->numline=$2->numline;
            token->content=$2->content;
            $$ = token;
    };

block: PUNC_CURL_LEFT{scope++;} stmts PUNC_CURL_RIGHT{
        scope--; 
        //hide(head_scope,scope+1);
    }
    | PUNC_CURL_LEFT PUNC_CURL_RIGHT;

funcdef: KEYWORD_FUNCTION IDENTIFIER PUNC_LEFT_PAR{
                 
                f_counter++;
                funtionName=malloc(strlen($2->content)); strcpy(funtionName, $2->content);
                push_l(funcname_Stack, $2->content);
                last_func_name=malloc(sizeof(char)*strlen($2->content));
                strcpy(last_func_name, $2->content);
                
                //n_expr->strConst=$2->content;
               
                
                
            } 
            
            idlist PUNC_RIGHT_PAR {
            Binding* b=lookup(*symbol_table, $2->content, scope, "variable");
            if(b == NULL){
                insert(&head_scope, *symbol_table, $2->content, scope, 1, $2->numline, USERFUNC, 0, NULL);
            }
            else{
                if(scope>b->scope){
                    insert(&head_scope, *symbol_table, $2->content, scope, 1, $2->numline, USERFUNC, 0, NULL);
                }
                else{
                    printf("ERROR in line %d/Function %s is already defined \n",$2->numline, $2->content);
                }
            }
            expr* n_expr=malloc(sizeof(expr));
            n_expr->sym=lookup(*symbol_table, $2->content, scope, "variable");
             n_expr->type=programfunc_e; 
            quads=emit(quads, funcstart, n_expr, NULL, NULL, 0, $2->numline);
            //emit funcstart
            inFunction=1;
            push(offset_Stack,global_offset);
            global_offset = 0;
        }block {
            pop_l(funcname_Stack, &last_func_name);
            expr* n_expr=malloc(sizeof(expr));
            //n_expr->strConst=$2->content;
            n_expr->type=programfunc_e;
            n_expr->sym=lookup(*symbol_table, $2->content, scope, "variable");
            quads=emit(quads, funcend, n_expr, NULL, NULL, 0, $8->numline);
            hide(head_scope,scope+1);
            struct alpha_token_t* ret=malloc(sizeof(struct alpha_token_t));
            ret->type="IDENTIFIER";
            ret->content=$2->content;
            inFunction=0;
            $$=ret;
            global_offset = pop(offset_Stack);
        }
        |KEYWORD_FUNCTION PUNC_LEFT_PAR {
            f_counter++;
            char b[20];
            b[0]='$';
            sprintf(b+1, "%d", nameless_id++);
            funtionName=malloc(20*sizeof(char));
            strcpy(funtionName, b);
            last_func_name=funtionName;
            insert(&head_scope, *symbol_table, b, scope, 1, $1->numline, USERFUNC, 0, NULL);

            expr* n_expr=malloc(sizeof(expr));
            n_expr->strConst=funtionName;
            n_expr->type=conststring_e;
            push(offset_Stack,global_offset);
            global_offset = 0;
            quads=emit(quads, funcstart, n_expr, NULL, NULL, 0, $2->numline);

        } idlist PUNC_RIGHT_PAR block{
            
            expr* n_expr=malloc(sizeof(expr));
            n_expr->strConst=funtionName;
            n_expr->type=conststring_e;
            quads=emit(quads, funcend, n_expr, NULL, NULL, 0, $1->numline);
            global_offset = pop(offset_Stack);
            hide(head_scope,scope+1);
            struct alpha_token_t* ret=malloc(sizeof(struct alpha_token_t));
            ret->type="IDENTIFIER";
            ret->content=funtionName;
            $$=ret;
        }
        ;

const: FLOAT{n_counter++; $$=$1;} | INTEGER {n_counter++; $$=$1;}| STRING {s_counter++; $$=$1;}| KEYWORD_NILL{$$=$1;} | KEYWORD_FALSE{$$=$1;} | KEYWORD_TRUE{$$=$1;};
idlist: {}
    |IDENTIFIER {
        push(offset_Stack,global_offset);
        global_offset = 0;
        insert(&head_scope, *symbol_table, $1->content, scope+1, 0, $1->numline, FORMAL, 1, funtionName);

    }
    |IDENTIFIER PUNC_COM idlist{
        insert(&head_scope, *symbol_table, $1->content, scope+1, 0, $1->numline, FORMAL, 1, funtionName);
    };
ifstmt: KEYWORD_IF PUNC_LEFT_PAR expr PUNC_RIGHT_PAR{
            if(strcmp($3->type, "table")==0){
                $3 = complete_emit($3);
            }
            int label;
            $3->quadline = currQuad;
            expr* arg1;
            expr* true_expr;
            Binding *tmp;
            arg1 = malloc(sizeof(expr));
            true_expr= malloc(sizeof(expr));
            tmp = lookuptemp(head_scope, $3->content);
            if(tmp==NULL){
                arg1->strConst=$3->content;
                arg1->type=constbool_e;
            }
            else{
                arg1->sym = tmp;
                arg1->type = var_e;
            }
            true_expr->strConst = "true";
            true_expr->type = constbool_e;

            label = currQuad+2;
            quads = emit(quads,if_eq,NULL,arg1,true_expr,label,0);
            quads = emit(quads,jump,NULL,NULL,NULL,0,0);
        }  stmt {
            patchlabel(quads+($3->quadline + 1),currQuad);
            $$ = $3;
        }
        | ifstmt KEYWORD_ELSE{
            patchlabel(quads+($1->quadline + 1),currQuad+1);
            $1->quadline = currQuad;
            quads = emit(quads,jump,NULL,NULL,NULL,0,0);
        } stmt{
            patchlabel(quads+($1->quadline),currQuad);
        };
whilestmt: KEYWORD_WHILE{
            struct alpha_token_t* token;
            token = malloc(sizeof(struct alpha_token_t));
            $1 = token;
            $1->quadline = currQuad;
        } PUNC_LEFT_PAR expr PUNC_RIGHT_PAR{
            if(strcmp($4->type, "table")==0){
                $4 = complete_emit($4);
            }
            int label;
            $3->quadline = currQuad;
            expr* arg1;
            expr* true_expr;
            Binding *tmp;
            arg1 = malloc(sizeof(expr));
            true_expr= malloc(sizeof(expr));
            tmp = lookuptemp(head_scope, $4->content);
      
            if((strcmp($4->type, "KEYWORD_FALSE")==0)||(strcmp($4->type, "KEYWORD_TRUE")==0)){
                arg1->strConst=$4->content;
                arg1->type=constbool_e;
            }
            else{
                if(tmp==NULL){
                    insert(&head_scope, *symbol_table, $4->content, scope, 0, $4->numline, FORMAL, 0, NULL);
                    tmp=lookuptemp(head_scope, $4->content);
                }
                arg1->sym = tmp;
                arg1->type = var_e;
            }

            true_expr->strConst = "true";
            true_expr->type = constbool_e;

            label = currQuad+2;
            quads = emit(quads,if_eq,NULL,arg1,true_expr,label,0);
            quads = emit(quads,jump,NULL,NULL,NULL,0,0);
            cont_break_scope++;
        } stmt{
            patchall(contlist,$1->quadline);
            quads = emit(quads,jump,NULL,NULL,NULL,$1->quadline,0);
            patchall(breaklist,currQuad);
            patchlabel(quads+($3->quadline)+1,currQuad);
            cont_break_scope--;
        };
forstmt: KEYWORD_FOR PUNC_LEFT_PAR elist{
            struct alpha_token_t* token;
            token = malloc(sizeof(struct alpha_token_t));
            $1 = token;
            $1->quadline = currQuad;
        } SEMICOLON expr{
            if(strcmp($6->type, "table")==0){
                $6 = complete_emit($6);
            }
            $5->quadline = currQuad;
            expr* arg1;
            expr* true_expr;
            Binding *tmp;
            arg1 = malloc(sizeof(expr));
            true_expr= malloc(sizeof(expr));
            tmp = lookuptemp(head_scope, $6->content);

            arg1->sym = tmp;
            arg1->type = var_e;

            true_expr->strConst = "true";
            true_expr->type = constbool_e;

            quads = emit(quads,if_eq,NULL,arg1,true_expr,0,0);
            quads = emit(quads,jump,NULL,NULL,NULL,0,0);
        } SEMICOLON elist{
            quads = emit(quads,jump,NULL,NULL,NULL,$1->quadline,0);
            patchlabel(quads+($5->quadline),currQuad);
            cont_break_scope++;
        } PUNC_RIGHT_PAR stmt{
            patchall(contlist,$5->quadline+2);
            quads = emit(quads,jump,NULL,NULL,NULL,$5->quadline+2,0);
            patchall(breaklist,currQuad);
            patchlabel(quads+($5->quadline+1),currQuad);
            cont_break_scope--;
            free($1);
        };
returnstmt: KEYWORD_RETURN expr{
    if(strcmp($2->type, "table")==0){
            $2 = complete_emit($2);
            $2->type="IDENTIFIER";
    }
    Binding* b;
    expr* ret_expr = malloc(sizeof(expr));
    //ret_expr->sym=lookuptemp(head_scope, $2->content);
    //ret_expr->type=var_e;
    if(!strcmp($2->type, "IDENTIFIER")){
        ret_expr->type=var_e;
        if((b=lookup(*symbol_table, $2->content, scope, "variable"))==NULL){
            insert(&head_scope, *symbol_table, $2->content, scope, 0, 0, FORMAL, 0, NULL);
            b=lookup(*symbol_table, $2->content, scope, "variable");
            
        }
        ret_expr->sym=b;
        assert(b);

    }
    else if(!strcmp($2->type, "STRING")){
        ret_expr->type=conststring_e;
        ret_expr->strConst=$2->content;
    }
    else if((!strcmp($2->type, "KEYWORD_TRUE"))||(!strcmp($2->type, "KEYWORD_FALSE"))){
        ret_expr->type=constbool_e;
        ret_expr->strConst=$2->content;
    }
    else if(!strcmp($2->type, "INTEGER")){
        ret_expr->type=constnum_e;
        ret_expr->numConst=atoi($2->content);
    }
    else{
        ret_expr->type=constnum_e;
        ret_expr->numConst=atof($2->content);
}
    quads = emit(quads,return_ ,ret_expr,NULL,NULL,0,$1->numline);

};
%%

int yyerror(char* yaccProvidedMessage){
    printf("yyerror: %s\n", yaccProvidedMessage);
    return 0;
}

int main(int argc, char* argv[]){

    /*if(argc=2  !strcmp(argv[1],"-e")){
    }
    else if(argc>2){
        assert(NULL);
    }*/

    head_scope=NULL;
    scope=0;
    nameless_id=0;
    t_id=0;
    assign_counter=0;
    inop=0;
    total = 0;
    op_flag=0;
    last_name=malloc(20*sizeof(char));
    last_name=NULL;
    symbol_table=SymTable_new();
    quads = (quad *)malloc(NEW_SIZE);
    currQuad = 0;
    not_counter=0;
    total = EXPAND_SIZE;
    list_create=0;
    cont_break_scope = 0;
    breaklist = SymTable_Num_new();
    contlist = SymTable_Num_new();
    inFunction=0;
    last_func_name=NULL;
    call_arg_func=0;
    func_call = 0;
    global_counter=0;

    s_counter=0;
    n_counter=0;
    f_counter=0;
    l_counter=0;

    global_offset=0;
    offset_Stack = New_Stack();
    funcname_Stack=New_Stack();

    currInst = 0;
    //memory = malloc(sizeof(program_mem));

    //memory->code = (instruction*)0;
    

    backpatch_labels = (int*) malloc(sizeof((currQuad*4)*sizeof(int)));

    if(argc==2){
        yyin=fopen(argv[1], "r");
    }   
    
    insert(&head_scope, *symbol_table, "print", 0, 1, 0, LIBFUNC, 0, NULL);
    insert(&head_scope, *symbol_table, "input", 0, 1, 0, LIBFUNC, 0, NULL);
    insert(&head_scope, *symbol_table, "objectmemberkeys", 0, 1, 0, LIBFUNC, 0, NULL);
    insert(&head_scope, *symbol_table, "objecttotalmembers", 0, 1, 0, LIBFUNC, 0, NULL);
    insert(&head_scope, *symbol_table, "objectcopy", 0, 1, 0, LIBFUNC, 0, NULL);
    insert(&head_scope, *symbol_table, "totalarguments", 0, 1, 0, LIBFUNC, 0, NULL);
    insert(&head_scope, *symbol_table, "argument", 0, 1, 0, LIBFUNC, 0, NULL);
    insert(&head_scope, *symbol_table, "typeof", 0, 1, 0, LIBFUNC, 0, NULL);
    insert(&head_scope, *symbol_table, "strtonum", 0, 1, 0, LIBFUNC, 0, NULL);
    insert(&head_scope, *symbol_table, "sqrt", 0, 1, 0, LIBFUNC, 0, NULL);
    insert(&head_scope, *symbol_table, "cos", 0, 1, 0, LIBFUNC, 0, NULL);
    insert(&head_scope, *symbol_table, "sin", 0, 1, 0, LIBFUNC, 0, NULL);
    
    yyparse();
    print_table(head_scope);  
    printQuads(quads, 1);
    
    printf("y\n");
    generate(n_counter, s_counter, f_counter, l_counter);

    

    
    print_target();
   
    
   
    return 0;
}

