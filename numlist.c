#include "numlist.h"



/* Creates and returns a new SymTable_NumT object.
 */
SymTable_NumT SymTable_Num_new(void){
SymTable_NumT table = NULL;
table = malloc(sizeof(SymTable_NumT));
table -> head = NULL;
table -> end = NULL;
return table;
}

/*
 * Function used by SymTable_free
 */
void SymTable_Num_free2(Num_Binding* node){
	if(node -> next != NULL)
	SymTable_Num_free2(node -> next);
	free(node);
	return;
}

/* Deallocates the memory allocated by the list.
 * It has a checked runtime error in case oSymTable is NULL.
 */
void SymTable_Num_free(SymTable_NumT oSymTable){
	assert(oSymTable != NULL);
	SymTable_Num_free2(oSymTable -> head);
	free(oSymTable);
	return;
}

/* Returns the number of Num_Bindings that the list has.
 * It has a checked runtime error in case oSymTable is NULL.
 */
unsigned int SymTable_Num_getLength(SymTable_NumT oSymTable){
	unsigned int n = 0;
	Num_Binding* t;
	assert(oSymTable != NULL);
	for(t = oSymTable->head; t != NULL; t = t -> next)
		n++;
	return n;
}

/* Creates a new Num_Binding with the key and value that is given, puts it on the list and returns 1.
 * If a Num_Binding with the same key already exist on the list then it doesn't change list and
 * it returns 0.
 * It has a checked runtime error in case oSymTable, pcKey or pvValue is NULL.
 */
int SymTable_Num_put(SymTable_NumT oSymTable, const int pvValue, int scope){
	Num_Binding* t;
    assert(oSymTable!=NULL);
    t = malloc(sizeof(Num_Binding));
    t -> value = pvValue;
	t -> scope = scope;
    if(oSymTable -> head){
    	 t -> next = oSymTable -> head;
    }else{
    	t->next=NULL;
		oSymTable -> end = t;
    }
    oSymTable -> head  = t;
    return 1;
}

/* Applies the given function "pfApply" to all of the Num_Binding of the list.
 * It has a checked runtime error in case oSymTable, pcExtra is NULL.
 */
void SymTable_Num_map(SymTable_NumT oSymTable, void (*pfApply)(void* p, unsigned int pvExtra),void *array ,const int pvExtra){
	Num_Binding *t;
	if(oSymTable == NULL){
		return;
	}
	for(t = oSymTable->head; t!=NULL;t=t->next){
		(*pfApply)(t-> value + array, pvExtra);
	}
	return;
}

/* Merges two SymTable_NumT objects and returns a new combined SymTable_NumT object.
 */
SymTable_NumT merge(SymTable_NumT table1, SymTable_NumT table2){
	SymTable_NumT table = NULL;
	table = malloc(sizeof(SymTable_NumT));
	if(table1 == NULL && table2 != NULL){
		table -> head = table2 -> head;
		table -> end = table2 -> end;
		return table;
	}
	else if(table2 == NULL && table1 != NULL){
		table -> head = table1 -> head;
		table -> end = table1 -> end;
		return table;
	}
	else{
		return table;
	}
table1->end->next = table2->head;	
table -> head = table1 -> head;
table -> end = table2 -> end;
return table;
}

/*void patch_all(SymTable_NumT oSymTable,quad* array,int nval){
	Num_Binding *t;
	assert(nval!=NULL);
	for(t = oSymTable->head; t!=NULL;t=t->next){
		patchlabel(array+t->value,nval);
	}
	return;
}*/