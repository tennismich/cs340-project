#include "stack.h"


stack_Head* New_Stack(){
    stack_Head* stack;
    stack = malloc(sizeof(stack_Head));
    stack->head = NULL;
    return stack;
}

void add_to_l(stack_Head *s_head, int value){
    L_Binding *l;
    l = malloc(sizeof(L_Binding));
    l->value = value;
    if(s_head->head->list != NULL){
        l->next = s_head->head->list;
    }
    else{
        l->next = NULL;
    }
    s_head->head->list = l;
}

int push(stack_Head* s_head, int value){
	S_Binding* t;
    assert(s_head!=NULL);
    t = malloc(sizeof(S_Binding));
    t -> value = value;
    if(s_head -> head){
    	 t -> next = s_head -> head;
    }else{
    	t->next=NULL;
    }
    s_head -> head  = t;
    return 1;
}

int pop(stack_Head* s_head){
    int value;
    
    S_Binding* t;
    t = s_head->head;
    if(t==NULL){
        return -1;
    }
    value =t->value;
    s_head->head = s_head->head->next;
    free(t);
    return value;
}

int push_l(stack_Head* s_head,char* function){
	S_Binding* t;
    assert(s_head!=NULL);
    t = malloc(sizeof(S_Binding));
    t->s=malloc(sizeof(char)*strlen(function));
    strcpy(t->s,function);
    t->list = NULL;
    if(s_head -> head){
    	 t -> next = s_head -> head;
    }else{
    	t->next=NULL;
    }
    s_head -> head  = t;
    return 1;
}

L_Binding* pop_l(stack_Head* s_head,char** function){
    L_Binding* l;
    S_Binding* t;
    t = s_head->head;
    l = NULL;
    if(t==NULL){
        assert(NULL);
    }
    if(t->list) l = t->list;
    *function=malloc(sizeof(char)*strlen(t->s));
    strcpy(*function,t->s);
    s_head->head = s_head->head->next;
    free(t);
    return l;
}