#ifndef INTERMIDIATE_H_
#define INTERMIDIATE_H_

#include<stdio.h>
#include"symtable.h"

struct alpha_token_t;

enum iocode{
    assign_, add,    sub,
    mul,    div_,    mod,
    uminus, and_,    or_,
    not_,    if_eq,  if_noteq,
    if_lesseq,  if_greatereq,    if_less,
    if_greater, jump, call, param, return_, 
    getretval, funcstart, funcend, tablecreate,
    tablegetelem, tablesetelem  
};

enum expr_t{
    var_e,
    tableitem_e,

    programfunc_e,
    libraryfunc_e,

    arithexpr_e,
    boolexpr_e,
    assignexpr_e,
    newtable_e,

    constnum_e,
    constbool_e,
    conststring_e,

    nil_e
};



typedef struct expr{
    enum expr_t type;
    Binding* sym;
    struct expr* index;
    double numConst;
    char* strConst;
    unsigned char boolConst;
    int isConst;
    struct expr* next;
} expr;

typedef struct quad{
    enum iocode op;
    expr* result;
    expr* arg1;
    expr* arg2;
    unsigned label;
    unsigned line;
    unsigned taddress;
    unsigned ij;
}quad;

typedef struct expr_head{
    expr* head;
    struct expr_head* last;
} expr_head;

quad* emit(quad* array,enum iocode op, expr *result, expr *arg1, expr *arg2, unsigned label, unsigned line);
void patchlabel(quad* array,unsigned label);
char *exprtostring(enum expr_t expr);
char *optostring(enum iocode op);
void printQuads(quad *array, unsigned int size);
char* newtempname();
void hidetemp(ScopeList_Node* head);
Binding* lookuptemp(ScopeList_Node* head, char* name);
expr* token_to_expr(ScopeList_Node** scopelist_head, SymTable_T oSymTable, struct alpha_token_t* token, int scope);
expr* term_token_to_expr(ScopeList_Node** scopelist_head, SymTable_T oSymTable, struct alpha_token_t* token, int scope);

#endif