#include "target.h"

extern SymTable_T* symbol_table;
extern quad *quads;
extern unsigned int currInst;
extern unsigned total;
extern unsigned int currQuad;
program_mem* memory;
extern int s_counter;
extern int n_counter;
extern int f_counter;
extern int l_counter;
extern int global_counter;
extern int scope;
stack_Head* s_head;

unsigned gi;
unsigned cq;
struct incomplete_jump *ij_head;
unsigned ij_total;
unsigned fj;

instruction new_inst(){
    instruction n;
    n.result.type = nil_a;
    n.arg1.type = nil_a;
    n.arg2.type = nil_a;
    return n;
}

void emit_t(instruction i){     
    memory->code[currInst] = i;
    currInst++;
}

instruction new_instruction(enum vmopcode op,quad* q){
    vmarg result, arg1, arg2;
    instruction i;
    i = new_inst();
    i.op = op;
    if(q->result != NULL){
        make_operand(q->result, &result);
        i.result = result;
    }
    if(q->arg1 != NULL){
        make_operand(q->arg1, &arg1);
        i.arg1 = arg1;
    }
    if(q->arg2 != NULL){
        make_operand(q->arg2, &arg2);
        i.arg2 = arg2;
    }
	i.srcline=q->line;
    q->taddress = currInst;
	emit_t(i);
    return i;
}

void add_incomplete_jump(unsigned int inst_n, quad* q){
    /*struct incomplete_jump* new_ij;
    new_ij = malloc(sizeof(struct incomplete_jump));
    new_ij->instrNo = inst_n;
    new_ij->iadress = label;
    if(ij_head==NULL){
        ij_head=new_ij;
    }
    else{
        new_ij->next = ij_head;
        ij_head = new_ij;
    }*/
  
    q->ij = (q->label)+1;
    ij_total++;
}

void generate_relational(enum iocode op, quad *q){
	instruction t;
    t = new_inst();
	t.op = op;
	if(q->arg1 != NULL){
        make_operand(q->arg1, &t.arg1);
    }
    if(q->arg2 != NULL){
        make_operand(q->arg2, &t.arg2);
    }
	//t.result.type = label_a;
	if (q->label< cq)
		t.srcline = quads[q->label].taddress;
	else
		add_incomplete_jump(currInst, q);
	q->taddress = currInst;
	emit_t(t); 
}

void backpatch_code(){
    /*struct incomplete_jump *in;
    int i;
    in = ij_head;
    for(i = 0;i<ij_total;i++){
        if(in->iadress > currQuad-1){
            memory->code[in->instrNo].srcline = currInst;
        }
        else{
            printf("q: %d, t:%d\n",in->iadress,in->instrNo);
            memory->code[in->instrNo].srcline = quads[in->iadress].taddress;
        }
        in = in->next;
    }*/
    int i;
    unsigned a;
    for(i = 0; i < currQuad ; i++){
        if(quads[i].ij != 0){
           a = quads[i].ij -1; 
           if(a > currQuad-1){
            memory->code[quads[i].taddress].srcline = currInst;
           }
            else{
                memory->code[quads[i].taddress].srcline = quads[a].taddress;
            } 
        }
    }
}

void generate_ADD(quad *q, stack_Head *s_head){
    new_instruction(add_v,q);
}
void generate_SUB(quad *q, stack_Head* s_head){
    new_instruction(sub_v,q);
}
void generate_MUL(quad *q, stack_Head* s_head){
    new_instruction(mul_v,q);
}
void generate_DIV(quad *q, stack_Head *s_head){
    new_instruction(div_v,q);
}
void generate_MOD(quad *q, stack_Head *s_head){
    new_instruction(mod_v,q);
}
void generate_NEWTABLE(quad *q, stack_Head* s_head)
{
    new_instruction(newtable_v,q);
}
void generate_TABLEGETELEM(quad *q, stack_Head* s_head){
    new_instruction(tablegetelem_v,q);
}
void generate_TABLESETELEM(quad *q, stack_Head* s_head){
    new_instruction(tablesetelem_v,q);
}
void generate_ASSIGN(quad *q, stack_Head* s_head){
    new_instruction(assign_v,q);
}
void generate_NOOP(quad *q, stack_Head* s_head){
    instruction t; t.op=nop_v; emit_t(t);
}
void generate_JUMP(quad *q, stack_Head* s_head){
    generate_relational(jump_v,q);
}
void generate_IF_EQ(quad *q, stack_Head* s_head){
    generate_relational(jeq_v,q);
}
void generate_IF_NOTEQ(quad *q, stack_Head* s_head){
    generate_relational(jne_v,q);
}
void generate_IFGREATER(quad *q, stack_Head* s_head){
    generate_relational(jgt_v,q);
}
void generate_IFGREATEREQ(quad *q, stack_Head* s_head){
    generate_relational(jge_v,q);
}
void generate_IF_LESS(quad *q, stack_Head* s_head){
    generate_relational(jlt_v,q);
}
void generate_IF_LESSEQ(quad *q, stack_Head* s_head){
    generate_relational(jle_v,q);
}
void generate_PARAM(quad *q, stack_Head* s_head){
    q->taddress=currInst; instruction t; t=new_inst(); t.op=pusharg_v; make_operand(q->result, &t.result); emit_t(t);
}
void generate_CALL(quad *q, stack_Head* s_head){
    q->taddress=currInst;
     instruction t;
     t=new_inst(); 
     t.op=call_v; 
     //if(q->result->sym->type==LIBFUNC){q->result->type=libraryfunc_e;}
     make_operand(q->result, &t.result); 
     emit_t(t);
}
void generate_GETRETVAL(quad *q, stack_Head* s_head){
    assert(q);
    q->taddress=currInst; 
    instruction t; 
    t=new_inst();
    t.op=assign_v; 
    make_operand(q->result, &t.result); 
    make_retvaloperand(&t.arg1); 
    emit_t(t);
}
void generate_FUNCSTART(quad *q, stack_Head* s_head){
    memory->user_func_line[gi] = currInst+1;
    gi++;

    q->taddress=currInst;
    instruction t,t2; 
    t=new_inst();
    t.op=funcenter_v; 
    t.result.type=label_a;
    t2=new_inst();
    t2.op=jump_v;
    t2.srcline = 225;
   // q->result->type=label_a;
    make_operand(q->result, &t.result); 
    push_l(s_head," ");
    add_to_l(s_head, currInst);
    emit_t(t2);
    emit_t(t);
}
void generate_RETURN(quad *q, stack_Head* s_head){
    q->taddress=currInst;
    instruction t; 
    t=new_inst();
    t.op=assign_v;
    make_retvaloperand(&t.result); 
    make_operand(q->result, &t.arg1); 
    emit_t(t);

    add_to_l(s_head, currInst);

    instruction t2; 
    t2=new_inst();
    t2.op=jump_v; 
    emit_t(t2);
}
void generate_FUNCEND(quad *q, stack_Head* s_head){
    L_Binding* list;
    char *function;
    printf("prepop\n");
    
    printf("pop_l\n");
    for(list = pop_l(s_head,&function); list->next !=NULL ; list = list->next){
        printf("pop\n");
        memory->code[list->value].srcline = currInst;
    }
    memory->code[list->value].srcline = currInst+1;
    printf("postpop\n");
    q->taddress=currInst;
    instruction t; 
    t=new_inst();
    t.op=funcexit_v; 
    t.result.type=label_a;
    printf("pre\n");
    make_operand(q->result, &t.result); 
    emit_t(t);
}
void generate_UMINUS(quad *q, stack_Head* s_head){
    q->taddress=currInst;
    instruction t;
    t=new_inst();
    t.op=mul_v;
    make_operand(q->result, &t.result);
    make_operand(q->arg1, &t.arg1);
    make_numberoperand(&t.arg2,-1);
    emit_t(t);
}



unsigned consts_newstring(char* s){
    int i;
   
    for(i=0;i<s_counter;i++){
        if((*(memory->string+i) != NULL) && (strcmp(s,*(memory->string+i)) == 0)){
            return i;
        }
    }
   // *(memory->string+s_counter) = s;
    memory->string[s_counter]=malloc(strlen(s)*sizeof(char)+1);
    
    memory->str_len[s_counter]=strlen(s);
    strcpy(memory->string[s_counter], s);
    s_counter++;
    return (s_counter-1);
}

int consts_newnumber (double n){
    int i;
    for(i=0;i<n_counter;i++){
        if(((memory->num+i) != NULL) && (n == *(memory->num+i))){
             return i;
        }     
    }
    memory->num[n_counter] = n;
    n_counter++;
    return i;
}

unsigned user_funcs_newused (char* n){
    int i;
    for(i=0;i<f_counter;i++){
        if(((memory->user_function+i) != NULL) && (strcmp(n,*(memory->user_function+i)) == 0)){
             return i;
        }     
    }
    memory->user_function[f_counter]=malloc(strlen(n)*sizeof(char));
    memory->user_len[f_counter]=strlen(n);
    Binding* b=lookup(*symbol_table, n, -1, "variable");
    assert(b);
    memory->func_locals[f_counter]=b->value.funcVal->local_vars;
    strcpy(memory->user_function[f_counter], n);
    
    f_counter++;
    return i;
}

unsigned library_funcs_newused (char* n){
    int i;
    for(i=0;i<l_counter;i++){
        if(((memory->library_function+i) != NULL) && (strcmp(n,*(memory->library_function+i)) == 0)){
             return i;
        }
    }
    memory->library_function[l_counter]=malloc(strlen(n)*sizeof(char));
    strcpy(memory->library_function[l_counter], n);
    memory->lib_len[l_counter]=strlen(n);
    l_counter++;
    return i;
}

void make_numberoperand(vmarg* arg, double val){
    arg->val=consts_newnumber(val);
    arg->type=number_a;
}

void make_booloperand(vmarg * arg, unsigned val){
    arg->val=val;
    arg->type=bool_a;
}

void make_retvaloperand(vmarg* arg){
    arg->type=retval_a;
    arg->val = 0;
}

/*void generate_relational(enum iocode op, quad *q){
	instruction t;
	t.op = op;
	make_operand(q->arg1, &t.arg1);
	make_operand(q->arg2, &t.arg2);
	t.result.type = label_a;
	if (q->label< currprocessedquad())
		t.result.val = quads[q->label].taddress;
	else
		add_incomplete_jump(nextinstructionlabel(), q->label);
	q->taddress = nextinstructionlabel();
	emit_t(t);    
}*/

void make_operand(expr* e, vmarg* arg){
    switch (e->type){
        case var_e:{
            arg->val=e->sym->value.varVal->offset;
            switch (e->sym->space)
            {
                case programvar: arg->type=global_a; break;
                case functionlocal: arg->type = local_a; break;
                case formalarg: arg->type = formal_a; break;
                
                default: assert(NULL);
            }
            break;
        }
        case tableitem_e:{
            arg->val = consts_newstring(e->strConst);
            arg->type = string_a;
            break;

        }
        case arithexpr_e:{
            /**/
            break;

        }
        case assignexpr_e:{
            /**/
            break;
        }
        case boolexpr_e:{
            arg->type=bool_a;
            if(!strcmp(e->strConst, "TRUE")){
                arg->val=1;
            }
            else{
                arg->val=0;
            }
            arg->type=bool_a;
            // arg->val=consts_newstring(e->strConst);
            break;
        }
        case newtable_e: {
            arg->val=e->sym->value.varVal->offset;
            switch (e->sym->space)
            {
                case programvar: arg->type=global_a; break;
                case functionlocal: arg->type = local_a; break;
                case formalarg: arg->type = formal_a; break;
                
                default: assert(NULL);
            }
            break;
        }
        case constbool_e:{
            // arg->val = e->boolConst;
            // arg->type = bool_a; break;
            //printf("b %s\n",e->strConst);
            arg->type=bool_a;
            if(!strcmp(e->strConst, "true")){
                arg->val=1;
            }
            else{
                arg->val=0;
            }
            break;
        }
        case conststring_e:{
            //if(arg->type!=label_a){
                
                arg->val = consts_newstring(e->strConst);
                arg->type = string_a; 
            //}
            break;
        }
        case constnum_e:{
            arg->type=number_a;
            make_numberoperand(arg, e->numConst);
            break;
        }
        case nil_e:{
            arg->type=nil_a;
            break;
        }
        case programfunc_e:{
            arg->type=userfunc_a;
            arg->val= user_funcs_newused(e->sym->value.funcVal->name);
            break;
        }
        case libraryfunc_e:{
            arg->type=libfunc_a;
            arg->val= library_funcs_newused(e->sym->value.funcVal->name);
            break;
        }
        default: assert(0);
    }
}

generator_func_t generators[] =
	{generate_ASSIGN,
	 generate_ADD,
	 generate_SUB,
	 generate_MUL,
	 generate_DIV,
	 generate_MOD,
     generate_UMINUS,
     generate_NOOP,
     generate_NOOP,
     generate_NOOP,
	 generate_IF_EQ,
	 generate_IF_NOTEQ,
	 generate_IF_LESSEQ,
	 generate_IFGREATEREQ,
	 generate_IF_LESS,
	 generate_IFGREATER,
     generate_JUMP,
	 generate_CALL,
	 generate_PARAM,
     generate_RETURN,
     generate_GETRETVAL,
	 generate_FUNCSTART,
	 generate_FUNCEND,
	 generate_NEWTABLE,
	 generate_TABLEGETELEM,
	 generate_TABLESETELEM
	 };

void generate(int n, int s, int f, int l){
    int i;
    // in = malloc(sizeof(int));
    // in = {1};
    
    s_head = New_Stack();
    memory = malloc(sizeof(program_mem));
    memory->code = malloc(currQuad*4*sizeof(instruction));
    memory->num=malloc(sizeof(double)* n);
    memory->string=malloc(sizeof(char*)*s);
    memory->user_function = malloc(sizeof(char*)*f);
    memory->library_function = malloc(sizeof(char*)*l);
    memory->str_len=malloc(sizeof(unsigned)*s);
    memory->lib_len=malloc(sizeof(unsigned)*l);
    memory->user_func_line=malloc(sizeof(unsigned)*n);
    memory->user_len=malloc(sizeof(int)*f);
    memory->func_locals=malloc(sizeof(unsigned)*f);

    for(i = 0; i<currQuad*4 ; i++){
        memory->code[i].op=nop_v;
        memory->code[i].result.type = nil_a;
        memory->code[i].arg1.type = nil_a;
        memory->code[i].arg2.type = nil_a;
        memory->code[i].srcline = 0;
    }

    s_counter = 0;
    n_counter = 0;
    f_counter = 0;
    l_counter = 0;

    //ij_head = (struct incomplete_jump *)0;
    ij_total = 0;
    
    for(cq = 0; cq<currQuad; ++cq){
        //printf("\n op: %d\n",quads[cq].op);
        (*generators[quads[cq].op])(quads+cq,s_head);
    }
    
    backpatch_code();
    /*for(i= 0; i<f_counter ; i++){
        for(cq = 0; cq<currInst; ++cq){
            if((memory->code[cq].op == funcenter_v) && (strcmp(memory->string[memory->code[cq].result.val], memory->user_function[i]) == 0))
                memory->user_func_line[i] = cq; 
        }
    }*/

    FILE* avm = fopen("bin.abc", "wb");
    unsigned magic_number=340200501;
    

    fwrite(&magic_number, sizeof(unsigned), 1, avm);
    fwrite(&global_counter, sizeof(int), 1, avm);
    fwrite(&s_counter, sizeof(unsigned), 1, avm);
    fwrite(memory->str_len, sizeof(unsigned), s_counter, avm);
    for(int j=0; j<s_counter; j++){
        fwrite(memory->string[j], sizeof(char), strlen(memory->string[j]), avm);
    }
    fwrite(&n_counter, sizeof(unsigned), 1, avm);
    fwrite(memory->num, sizeof(double), n_counter, avm);
    
    fwrite(&f_counter, sizeof(unsigned), 1, avm);
    /*
    f_counter
    array_lengths
    array_name
    array_lines
    */
    fwrite(memory->user_len, sizeof(unsigned), f_counter, avm);
    for(int u=0; u<f_counter; u++){
        fwrite(memory->user_function[u], sizeof(char), strlen(memory->user_function[u]), avm);
    }
    fwrite(memory->user_func_line, sizeof(unsigned), f_counter, avm);
    
    fwrite(memory->func_locals, sizeof(unsigned), f_counter, avm);
    
    fwrite(&l_counter, sizeof(unsigned), 1, avm);
    
    fwrite(memory->lib_len, sizeof(unsigned), l_counter, avm);
    for(int u=0; u<l_counter; u++){
        fwrite(memory->library_function[u], sizeof(char), strlen(memory->library_function[u]), avm);
    }
    fwrite(&currInst, sizeof(unsigned), 1, avm);
    fwrite(memory->code, sizeof(instruction), currInst, avm);

    fclose(avm);
   
    unsigned mn;
}

char* typetostring(enum vmarg_t t){
  
    switch (t)
    {
    case 0:
        return "label";
        break;
     case 1:
        return "global";
        break;
         case 2:
         return "formal";
        break;
         case 3:
            return "local";
        break;
         case 4:
            return "number";
        break;
         case 5:
            return "string";
        break;
         case 6:
            return "bool";
        break;
         case 7:
            return "nil";
        break;
         case 8:
         return "userfunc";
        break;
         case 9:
         return "libfunc";
        break;
        case 10:
            return "retval";
    default:
        break;
    }
}

char* opvmtostring(enum vmopcode vmop){
    switch (vmop)
    {
    case 0:
        return "assign";  
        break;
    case 1:
        return "add";
        break;
    case 2:
        return "sub";
        break;
    case 3:
        return "mul";
        break;
    case 4:
        return "div";
        break;
    case 5:
        return "mod";
        break;
    case 6:
        return "uminus";
        break;
    case 7:
        return "and";
        break;
    case 8:
        return "or";
        break;
    case 9:
        return "not";
        break;
    case 10:
        return "jeq";
        break;
    case 11:
        return "jne";
        break;
    case 12:
        return "jle";
        break;
    case 13:
        return "jge";
        break;
    case 14:
        return "jlt";
        break;
    case 15:
        return "jgt";
        break;
    case 16:
        return "call";
        break;
    case 17:
        return "pusharg";
        break;
    case 18:
        return "funcenter";
        break;
    case 19:
        return "funcexit";
        break;
    case 20:
        return "newtable";
        break;
    case 21:
        return "tablegetelem";
        break;
    case 22:
        return "tablesetelem";
        break;
    case 23:
        return "nop";
        break;
    case 24:
        return "jump";
        break;
    default:
        break;
    }
}

void print_target(){
    instruction* t_i;
    int i=0;

    printf("\n=============================================\n\nConst num table\n");
    for(i=0; i<n_counter; i++){
        printf("%lf\n", memory->num[i]);
    }
    
    printf("\nConst string table\n");
    for(i=0; i<s_counter; i++){
        printf("%s\n", memory->string[i]);
    }

    printf("\nUser function table\n\n");
    for(i=0; i<f_counter; i++){
        printf("%s\n", memory->user_function[i]);
    }

    printf("\nLibrary function table\n\n");
    for(i=0; i<l_counter; i++){
        printf("%s\n", memory->library_function[i]);
    }


    printf("\n=============================================\nPrint target code:\n\n" );
    for(i=0; i<currInst; i++){
        printf("%d:\t", i);
        t_i = ((memory->code)+i);
        printf("%s\t", opvmtostring(t_i->op));
        if(t_i->result.type != nil_a){
            printf("result: %s, %d,",typetostring(t_i->result.type), t_i->result.val);
        }
        if(t_i->arg1.type != nil_a){
            printf("\targ1: %s, %d,",typetostring(t_i->arg1.type), t_i->arg1.val);
        }
        if(t_i->arg2.type != nil_a){
            printf("\targ2: %s, %d,",typetostring(t_i->arg2.type), t_i->arg2.val);
        }
        if((t_i->op == jump_v) || (t_i->op == jle_v) || (t_i->op == jlt_v) || (t_i->op == jeq_v) || (t_i->op == jne_v)|| (t_i->op == jgt_v)|| (t_i->op == jge_v)){
            printf("\tlabel: %d",t_i->srcline);
        }
        printf("\n");
    }
    printf("\n=============================================\n\n\n" );
}

