
#ifndef CODE_H_
#define CODE_H_

#include "intermidiate.h"

enum vmopcode
{
  assign_v, add_v, sub_v,
  mul_v, div_v, mod_v,
  uminus_v, and_v, or_v, 
  not_v, jeq_v, jne_v, 
  jle_v, jge_v, jlt_v,
  jgt_v, call_v, pusharg_v,
  funcenter_v, funcexit_v, newtable_v,
  tablegetelem_v, tablesetelem_v, nop_v,
  jump_v 

};

enum vmarg_t
{
    label_a=0,
    global_a=1,
    formal_a=2,
    local_a=3,
    number_a=4,
    string_a=5,
    bool_a=6,
    nil_a =7,
    userfunc_a=8,
    libfunc_a=9,
    retval_a=10

};

typedef struct vmarg_s
{
    enum vmarg_t type;
    unsigned val;
} vmarg;

typedef struct instruction_t
{
    enum vmopcode op;
    vmarg result;
    vmarg arg1;
    vmarg arg2;
    unsigned srcline;
} instruction;

typedef struct program_mem_t{
    char** string;
    unsigned *str_len;
    unsigned *lib_len;
    int *user_len;
    double* num;
    char** user_function;
    char** library_function;
    unsigned* user_func_line;
    unsigned* func_locals;
    instruction *code;
}program_mem;


struct userfunc{
    unsigned address;
    unsigned localSize;
    char* id;
};

struct incomplete_jump{
    unsigned instrNo;
    unsigned iadress;
    struct incomplete_jump* next;
};



typedef void (*generator_func_t)(quad *q, stack_Head *s_head);

instruction new_inst();

instruction new_instruction(enum vmopcode op,quad* q);


void add_incomplete_jump(unsigned instrNo, quad* q);

void backpatch_jump(instruction *code, int *backpatch_labels);

void make_numberoperand(vmarg* arg, double val);

void make_booloperand(vmarg* arg, unsigned val);

void make_retvaloperand(vmarg* arg);

void generate_relational(enum iocode op, quad *q);

void emit_t(instruction q);

// unsigned consts_newstring(char* s);

// unsigned consts_newnumber (double n);

// unsigned user_funcs_newused (char* n);

// unsigned library_funcs_newused (char* s);

void print_target();

void make_operand(expr* e, vmarg* arg);

void generate(int n, int s, int f, int l);

void generate_avm_file();

void generate_ADD(quad *q, stack_Head* s_head);
void generate_SUB(quad *q, stack_Head* s_head);
void generate_MUL(quad *q, stack_Head* s_head);
void generate_DIV(quad *q, stack_Head* s_head);
void generate_MOD(quad *q, stack_Head* s_head);
void generate_NEWTABLE(quad *q, stack_Head* s_head);
void generate_TABLEGETELEM(quad *q, stack_Head* s_head);
void generate_TABLESETELEM(quad *q, stack_Head *s_head);
void generate_ASSIGN(quad *q, stack_Head* s_head);
void generate_NOOP(quad *q, stack_Head* s_head);
void generate_JUMP(quad *q, stack_Head* s_head);
void generate_IF_EQ(quad *q, stack_Head *s_head);
void generate_IF_NOTEQ(quad *q, stack_Head* s_head);
void generate_IFGREATER(quad *q, stack_Head* s_head);
void generate_IFGREATEREQ(quad *q, stack_Head* s_head);
void generate_IF_LESS(quad *q, stack_Head* s_head);
void generate_IF_LESSEQ(quad *q, stack_Head* s_head);
void generate_PARAM(quad *q, stack_Head* s_head);
void generate_CALL(quad *q, stack_Head* s_head);
void generate_GETRETVAL(quad *q, stack_Head* s_head);
void generate_FUNCSTART(quad *q, stack_Head* s_head);
void generate_RETURN(quad *q, stack_Head* s_head);
void generate_FUNCEND(quad *q, stack_Head* s_head);
void generate_UMINUS(quad* q, stack_Head* s_head);

#endif