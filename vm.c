#include "vm.h"
#include "stack.h"

#define AVM_MAX_INSTRUCTIONS 6400
#define BASE_OFFSET 5          
#define STACK_SIZE 6400         
#define AVM_SAVEDTOP_OFFSET 2   //placeholder
#define AVM_SAVEDPC_OFFSET 3    //placeholder
#define AVM_SAVEDTOPSP_OFFSET 3 //placeholder
#define MAX_INPUT 6400

FILE *f;
int global_c;
unsigned char executionFinished = 0;
unsigned pc = 0;
unsigned currLine = 0;
unsigned codeSize = 0;
avm_memcell ax, bx, cx, dx, retval;

unsigned top;
unsigned topsp;
unsigned totalActuals;

unsigned *user_func_instr;
unsigned *user_func_locals;
avm_memcell *string_vm;
avm_memcell *libfunc_vm;
avm_memcell *userfunc_vm;
avm_memcell *num_vm;
instruction *code_vm = (instruction *)0;

avm_memcell *stack;
//avm_memcell stack[AVM_STACKSIZE];

ScopeList_Node* head_scope;
int yylex(void);
int yyerror (char* yaccProvidedMessage);
extern FILE* yyin;
int nameless_id, t_id, assign_counter, inop, op_flag, not_counter;
char* id_temp;
expr* last_temp;
char* last_name;
expr* last_arg;
expr* lvalue_head, *rvalue_head;
expr_head* list_head;
int list_create;
int global_offset;
int inFunction;
stack_Head *offset_Stack;
int call_arg_func;
int* backpatch_labels;
FILE* f;

struct alpha_token_t {
    unsigned int numline;
    unsigned int numToken;
    unsigned int quadline;
    char *content;
    const char *type;
    expr* expression;
    expr* prev_expr;
};

SymTable_T* symbol_table;
char* funtionName;
char* last_func_name;
extern int line;
int scope, varInFunc=0;

quad *quads;
unsigned total;
unsigned int currQuad;
int global_counter;
int cont_break_scope;


int func_call;


unsigned int currInst;



stack_Head *offset_Stack;
stack_Head* funcname_Stack;
//program_mem memory;
int s_counter, n_counter, f_counter, l_counter;
memclear_func_t memclearFuncs[] = {
    0,
    memclear_string,
    0,
    memclear_table,
    0,
    0,
    0,
    0};

unsigned int number_tobool(avm_memcell *m) { return m->data.numVal != 0; }
unsigned int string_tobool(avm_memcell *m) { return m->data.strVal[0] != 0; }
unsigned int bool_tobool(avm_memcell *m) { return m->data.boolVal; }
unsigned int table_tobool(avm_memcell *m) { return 1; }
unsigned int userfunc_tobool(avm_memcell *m) { return 1; }
unsigned int libfunc_tobool(avm_memcell *m) { return 1; }
unsigned int nil_tobool(avm_memcell *m) { return 0; }
unsigned int undef_tobool(avm_memcell *m)
{
    assert(0);
    return 0;
}


static unsigned int avm_table_hash(const char *pcKey)
{
  size_t ui,length;
  unsigned int uiHash = 0U;
  
  length = strlen(pcKey);
  for (ui = 0U; ui < length; ui++)
    uiHash = uiHash * 65599 + pcKey[ui];
  return uiHash % AVM_TABLE_HASHSIZE;
}


static unsigned int avm_table_numhash(int pcKey)
{
  /*size_t ui,length;
  unsigned int uiHash = 0U;
  
  length = strlen(pcKey);
  for (ui = 0U; ui < length; ui++)
    uiHash = uiHash * 65599 + avm_table_numhash;*/
  return pcKey % AVM_TABLE_HASHSIZE;
}


tobool_func_t toboolFuncs[] = {
    number_tobool,
    string_tobool,
    bool_tobool,
    table_tobool,
    userfunc_tobool,
    libfunc_tobool,
    nil_tobool,
    undef_tobool};

unsigned char avm_tobool(avm_memcell *m)
{
    assert(m->type >= 0 && m->type < undef_m);
    return (*toboolFuncs[m->type])(m);
}

char *typeStrings[] = {
    "number",
    "string",
    "bool",
    "table",
    "userfunc",
    "libfunc",
    "nil",
    "undef"
};

void avm_memcellclear(avm_memcell *m)
{
    if (m->type != undef_m)
    {
        memclear_func_t f = memclearFuncs[m->type];
        if (f)
            (*f)(m);
        m->type = undef_m;
    }
}

extern void memclear_string(avm_memcell *m)
{
    assert(m->data.tableVal);
    //avm_tabledecrefcounter(m->data.tableVal);
}

void memclear_table(avm_memcell *m)
{
   // assert(NULL);
}

avm_memcell *const_getnum(unsigned i)
{
    return num_vm + i;
}

avm_memcell *const_getstring(unsigned i)
{
    return string_vm + i;
}

avm_memcell *avm_translate_operand(vmarg *arg, avm_memcell *reg)
{
    switch (arg->type)
    {
    case global_a:
        return (stack + (STACK_SIZE - arg->val));
        break;
    case local_a:
        return (stack + (topsp - arg->val));
        break;
    case formal_a:
        return (stack + (topsp + BASE_OFFSET + (stack[topsp + 4].data.funcVal - 1 - arg->val)));
        break;
    case retval_a:
        return &retval;
        break;
    case number_a:
        return const_getnum(arg->val);
        //printf("Im innnnnnnn num\n");
        break;
    case string_a:
        return const_getstring(arg->val);
        //printf("Im innnnnnnn\n");
        break;
    case bool_a:
        reg->type = bool_m;
        reg->data.boolVal = arg->val;
        return reg;
        break;
    case nil_a:
        reg->type = nil_m;
        return reg;
        break;
    case userfunc_a:
        reg->type = userfunc_m;
        reg->data.funcVal = arg->val;
        return reg;
        break;
    case libfunc_a:
        reg->type = libfunc_m;
        reg->data.funcVal = arg->val;
        return reg;
        break;  
    default:
        break;
    }
}

void avm_assign(avm_memcell *lv, avm_memcell *rv)
{

    /**/

    if (rv->type == undef_m)
    {
        /*warning*/
    }

    avm_memcellclear(lv);

    if (rv->type == string_m)
    {
        //lv->data.strVal = malloc(strlen(rv->data.strVal));
        lv->type = string_m;
        memcpy(lv, rv, sizeof(avm_memcell)); 
        // lv->data.strVal=strdup(rv->data.strVal);
    }
    else if (rv->type == number_m)
    {
        memcpy(lv, rv, sizeof(avm_memcell));
    }
    else if (rv->type == bool_m)
    {
        lv->data.boolVal = rv->data.boolVal;
        lv->type = bool_m;
        memcpy(lv, rv, sizeof(avm_memcell));
    }
    else if (rv->type == nil_m)
    {
        lv->type = nil_m;
        memcpy(lv, rv, sizeof(avm_memcell));
    }
    else if (rv->type == userfunc_m)
    {
        lv->type = userfunc_m;
        memcpy(lv, rv, sizeof(avm_memcell));
    }
    else if (rv->type == libfunc_m)
    {
        lv->type = libfunc_m;
        memcpy(lv, rv, sizeof(avm_memcell));
    }
    else if (rv->type == table_m)
    {
        lv->type = table_m;
        memcpy(lv, rv, sizeof(avm_memcell));
    }
    // else if(lv->type==num)
}

void avm_dec_top(void)
{
    if (!top)
    {
        printf("stack overflow!\n");
        executionFinished = 1;
    }
    else
        --top;
}

void avm_push_value(unsigned i)
{
    stack[top].type = undef_m;
    stack[top].data.funcVal = i;
    avm_dec_top();
}

void avm_callsaveenvironment(void)
{
    avm_push_value(totalActuals);
    avm_push_value(pc + 1);
    avm_push_value(top + totalActuals + 2);
    avm_push_value(topsp);
    topsp = top;
}

unsigned avm_get_envvalue(unsigned i)
{

    return stack[i].data.stackVal;
}

void avm_print()
{
    unsigned i;
    double n;
    for (i = 0; i < stack[topsp + 4].data.funcVal; i++)
    {
        if (stack[topsp + BASE_OFFSET + i].type == number_m){
            n = stack[topsp + BASE_OFFSET + i].data.numVal;
            printf("%0.3lf", n);
        }
        else if (stack[topsp + BASE_OFFSET + i].type == string_m)
            printf("%s", stack[topsp + BASE_OFFSET + i].data.strVal);
        else if (stack[topsp + BASE_OFFSET + i].type == bool_m)
        {
            if (stack[topsp + BASE_OFFSET + i].data.boolVal == 1)
            {
                printf("true");
            }
            else
            {
                printf("false");
            }
        }
        else if (stack[topsp + BASE_OFFSET + i].type == nil_m)
        {
            printf("nil");
        }
        else if(stack[topsp + BASE_OFFSET + i].type == userfunc_m)
        {
            printf("user function %d", user_func_instr[stack[topsp + BASE_OFFSET + i].data.funcVal]);
        }
        else if(stack[topsp + BASE_OFFSET + i].type == libfunc_m)
        {
            printf("lib function %s", libfunc_vm[stack[topsp + BASE_OFFSET + i].data.funcVal].data.strVal);
        }
        // else
        //     printf("Undefined type: %d\n", stack[topsp + BASE_OFFSET + i].type);
    }
    retval.type = number_m;
    retval.data.numVal = 0;
}

void avm_typeof()
{

    retval.type = string_m;
    if (stack[topsp + 4].data.funcVal > 1)
    {
        printf("Error: too many arguments in function typeof()");
        executionFinished = 1;
        return;
    }
    else if (stack[topsp + 4].data.funcVal == 0)
    {
        printf("Error: no arguments in function typeof()");
        executionFinished = 1;
        return;
    }
    else
    {
        switch (stack[topsp + BASE_OFFSET].type)
        {
        case number_m:
            //retval.data.strVal=malloc(9);
            retval.data.strVal = "number";
            break;
        case string_m:
            retval.data.strVal = "string";
            break;
        case bool_m:
            retval.data.strVal = "bool";
            break;
        case table_m:
        case userfunc_m:
        case libfunc_m:
        case nil_m:
        case undef_m:
        };
    }
}

void totalarguments()
{
    avm_memcellclear(&retval);
    if (topsp == 0)
    {
        printf("totalarguments called outside of function");
        retval.type = nil_m;
    }
    else
    {
        retval.type = number_m;
        retval.data.numVal = stack[(stack[topsp + 1].data.funcVal)+4].data.funcVal;
    }
}

void argument()
{
    if (stack[topsp + 4].data.funcVal > 1)
    {
        printf("Error: too many arguments in function typeof()");
        executionFinished = 1;
        return;
    }
    else if (stack[topsp + 4].data.funcVal == 0)
    {
        printf("Error: no arguments in function typeof()");
        executionFinished = 1;
        return;
    }
    
    if (stack[topsp + BASE_OFFSET].type != number_m)
    {
        printf("Cannot get argument of non numerical\n");
        exit(-1);
    }

    if(stack[topsp+BASE_OFFSET].data.funcVal>stack[(stack[topsp + 1].data.funcVal)+4].data.funcVal){
        printf("Function doesn't have that many arguments");
        exit(-1);
    }
    switch(stack[(stack[topsp + 1].data.funcVal) + BASE_OFFSET + (int)(stack[topsp + BASE_OFFSET].data.numVal)].type){
        case number_m:
            retval.data.numVal = stack[(stack[topsp + 1].data.funcVal) + BASE_OFFSET + (int)(stack[topsp + BASE_OFFSET].data.numVal)].data.numVal;
	        retval.type = number_m;
            break;
        case bool_m:
			retval.data.boolVal=stack[(stack[topsp + 1].data.funcVal) + BASE_OFFSET + (int)(stack[topsp + BASE_OFFSET].data.numVal)].data.boolVal;
			retval.type = bool_m;
            break;
        case string_m:
			//free(retval.data.strVal);
			retval.data.strVal = (char *)malloc(strlen(stack[(stack[topsp + 1].data.funcVal) + BASE_OFFSET + (int)(stack[topsp + BASE_OFFSET].data.numVal)].data.strVal));
			strcpy(retval.data.strVal,stack[(stack[topsp + 1].data.funcVal) + BASE_OFFSET + (int)(stack[topsp + BASE_OFFSET].data.numVal)].data.strVal);
            retval.type = string_m;
			break;
		}
}

void sqrt_()
{
    if (stack[topsp + BASE_OFFSET].type != number_m)
    {
        printf("Cannot get sin of non numerical\n");
        exit(-1);
    }
    retval.type = number_m;
    retval.data.numVal = sqrt(stack[topsp + BASE_OFFSET].data.numVal);
}

void sin_()
{
    if (stack[topsp + BASE_OFFSET].type != number_m)
    {
        printf("Cannot get sin of non numerical\n");
        exit(-1);
    }
    retval.type = number_m;
    retval.data.numVal = sin(stack[topsp + BASE_OFFSET].data.numVal);
}

void cos_()
{
    if (stack[topsp + BASE_OFFSET].type != number_m)
    {
        printf("Cannot get sin of non numerical\n");
        exit(-1);
    }
    retval.type = number_m;
    retval.data.numVal = cos(stack[topsp + BASE_OFFSET].data.numVal);
}

void strtonum(){
    if (stack[topsp + BASE_OFFSET].type !=string_m)
    {
        printf("Argument not a number\n");
        exit(-1);
    }

    retval.type=number_m;
    retval.data.numVal = atoi(stack[topsp + BASE_OFFSET].data.strVal);
}

void input()
{
    unsigned i, f, c, length;
    char s[MAX_INPUT];
    scanf("%s", s);
    if(!strcmp(s,"nil")){
        retval.type = nil_m;
        retval.data.boolVal = 0;
        return;
    }
    else if(!strcmp(s,"true")){
        retval.type = bool_m;
        retval.data.boolVal = 1;
        return;
    }
    else if(!strcmp(s,"false")){
        retval.type = bool_m;
        retval.data.boolVal = 0;
        return;
    }
    length = strlen(s);
    f = 0;
    for(i = 0; i< length; i++){
        c = (unsigned) s[i]; 
        if(!((c >= 48 && c <= 57) || ((c == 46) && (i != length-1)))){
            f = 1;
        }
    }
    if(f == 0){
        retval.type = number_m;
        retval.data.numVal = atof(s);
    }
    else    
    {
        retval.type = string_m;
        retval.data.strVal = malloc(length);
        strcpy(retval.data.strVal,s);
    }
}

avm_table* avm_tablenew(void) {
	avm_table* t= (avm_table*) malloc (sizeof(avm_table));
	AVM_WIPEOUT(*t);

	t->refCounter=t->total=0;
	avm_tablebucketsinit(t->numIndexed);
	avm_tablebucketsinit(t->strIndexed);
	/*avm_tablebucketsinit(t->boolIndexed);
	avm_tablebucketsinit(t->userfuncIndexed);
	avm_tablebucketsinit(t->libfuncIndexed);*/

	return t;
}

void avm_tableincrefcounter (avm_table* t)
	{ ++t->refCounter;}


void avm_tabledecrefcounter (avm_table* t) {
	assert(t->refCounter > 0);
	if (!--t->refCounter)
		avm_tabledestroy(t);
}

void avm_tablebucketsinit(avm_table_bucket** p){
	for (unsigned i=0; i<AVM_TABLE_HASHSIZE;++i)
		p[i] = (avm_table_bucket*) 0;
}


void execute_assign(instruction *i)
{
    avm_memcell *lv = avm_translate_operand(&i->result, &ax);
    avm_memcell *rv = avm_translate_operand(&i->arg1, &bx);
    assert(lv && rv);
    avm_assign(lv, rv);
}

void execute_arithmetic(instruction *i)
{
    avm_memcell *lv = avm_translate_operand(&i->result, &ax);
    avm_memcell *rv1 = avm_translate_operand(&i->arg1, &bx);
    avm_memcell *rv2 = avm_translate_operand(&i->arg2, &cx);
    assert(lv && rv1 && rv2);

    avm_memcellclear(lv);

    lv->type = number_m;
    switch (i->op)
    {
    case add_v:
        lv->data.numVal = rv1->data.numVal + rv2->data.numVal;
        break;
    case sub_v:
        lv->data.numVal = rv1->data.numVal - rv2->data.numVal;
        break;
    case mul_v:
        lv->data.numVal = rv1->data.numVal * rv2->data.numVal;
        break;
    case div_v:
        //Error check
        if (rv2->data.numVal == (double)0)
        {
            printf("VM ERROR: Cannot divide by 0\n");
            executionFinished = 1;
            break;
        }
        lv->data.numVal = rv1->data.numVal / rv2->data.numVal;
        break;
    case mod_v:
        //Error check
        if (rv2->data.numVal == (double)0)
        {
            printf("VM ERROR: There is no modulus of 0\n");
            executionFinished = 1;
            break;
        }
        lv->data.numVal = (unsigned)rv1->data.numVal % (unsigned)rv2->data.numVal;
        break;
    default:
        assert(NULL);
        break;
    };
}


void execute_jeq(instruction *instr)
{

    if (instr->op == jump_v)
    {
        if (!executionFinished)
            pc = instr->srcline;
        return;
    }

    unsigned char result = 0;

    //assert (instr->result.type == label_a);

    avm_memcell *rv1 = avm_translate_operand(&instr->arg1, &ax);
    avm_memcell *rv2 = avm_translate_operand(&instr->arg2, &bx);

    assert(rv1 && rv2);

    if (rv1->type == undef_m || rv2->type == undef_m)
        printf("'undef' involved in equality!\n");
    else if (rv1->type == nil_m || rv2->type == nil_m)
        result = rv1->type == nil_m && rv2->type == nil_m;
    else if (rv1->type == bool_m || rv2->type == bool_m)
        result = (avm_tobool(rv1) == avm_tobool(rv2));
    else if (rv1->type != rv2->type)
        printf("%s == %s is illegal!\n", typeStrings[rv1->type], typeStrings[rv2->type]);
    else
    {
        /* Equality check with dispatching */
        switch (rv1->type)
        {
        case string_m:
            result = (!strcmp(rv1->data.strVal, rv2->data.strVal));
            break;
        case number_m:
            result = (rv1->data.numVal == rv2->data.numVal);
            break;
        case libfunc_m:
            /*code here*/
            break;
        case userfunc_m:
            /*code here*/
            break;
        case table_m:
            /*code here*/
            break;
        }
    }
    if (!executionFinished && result)
        pc = instr->srcline;
}



void execute_jne(instruction *instr)
{

    //assert(instr->result.type == label_a);

    avm_memcell *rv1 = avm_translate_operand(&instr->arg1, &ax);
    avm_memcell *rv2 = avm_translate_operand(&instr->arg2, &bx);

    assert(rv1 && rv2);

    unsigned char result = 0;

    if (rv1->type == undef_m || rv2->type == undef_m)
        printf("'undef' involved in equality!\n");
    else if (rv1->type == nil_m || rv2->type == nil_m)
        result = rv1->type != nil_m || rv2->type != nil_m;
    else if (rv1->type == bool_m || rv2->type == bool_m)
        result = (avm_tobool(rv1) != avm_tobool(rv2));
    else if (rv1->type != rv2->type)
        printf("%s == %s is illegal!\n", typeStrings[rv1->type], typeStrings[rv2->type]);
    else
    {
        /* Equality check with dispatching */
        switch (rv1->type)
        {
        case string_m:
            result = (strcmp(rv1->data.strVal, rv2->data.strVal) != 0);
            break;
        case number_m:
            result = (rv1->data.numVal != rv2->data.numVal);
            break;
        case libfunc_m:
            /*code here*/
            break;
        case userfunc_m:
            /*code here*/
            break;
        case table_m:
            /*code here*/
            break;
        }
    }
    if (!executionFinished && result)
        pc = instr->srcline;
}
void execute_jle(instruction *instr)
{
    //assert(instr->result.type == label_a);

    avm_memcell *rv1 = avm_translate_operand(&instr->arg1, &ax);
    avm_memcell *rv2 = avm_translate_operand(&instr->arg2, &bx);

    assert(rv1 && rv2);

    unsigned char result = 0;

    if (rv1->type == undef_m || rv2->type == undef_m)
    {
        printf("'undef' involved in equality!\n");
        executionFinished = 1;
    }
    else if (rv1->type == nil_m || rv2->type == nil_m)
    {
        printf("Cannot compare nil\n");
        executionFinished = 1;
    }
    else if (rv1->type == bool_m || rv2->type == bool_m)
    {
        printf("Cannot compare booleans\n");
        executionFinished = 1;
    }
    else if (rv1->type != rv2->type)
    {
        printf("%s <= %s is illegal!\n", typeStrings[rv1->type], typeStrings[rv2->type]);
        executionFinished = 1;
    }
    else
    {
        /* Equality check with dispatching */
        switch (rv1->type)
        {
        case string_m:
            printf("Cannot compare strings");
            executionFinished = 1;
            break;
        case number_m:
            result = (rv1->data.numVal <= rv2->data.numVal);
            break;
        case libfunc_m:
            /*code here*/
            break;
        case userfunc_m:
            /*code here*/
            break;
        case table_m:
            /*code here*/
            break;
        }
    }
    if (!executionFinished && result)
        pc = instr->srcline;
}

void execute_newtable (instruction* instr){
	avm_memcell* lv = avm_translate_operand(&instr->result,(avm_memcell*)0);
	assert(lv);
	avm_memcellclear(lv);

	lv->type=table_m;
	lv->data.tableVal = avm_tablenew();
	avm_tableincrefcounter(lv->data.tableVal);
}


avm_memcell* avm_tablegetelem(avm_table* table,avm_memcell* index){
    unsigned int hash;
    avm_table_bucket* node;
    avm_memcell* result;
    result = NULL;
    
    if(index->type == number_m){
        hash = avm_table_numhash(index->data.numVal);
        for(node = table->numIndexed[hash]; node != NULL; node = node->next){
            if(node->key.data.numVal == index->data.numVal){
                return &(node->value);
            }
        }
        printf("Error: element with index %lf not found\n", index->data.numVal);
        executionFinished = 1;
    }
    else if(index->type == string_m){
        hash = avm_table_hash(index->data.strVal);
        for(node = table->strIndexed[hash]; node != NULL; node = node->next){
            if(strcmp(node->key.data.strVal,index->data.strVal)){
                return &(node->value);
            }
        }
        printf("Error: element with index %s not found\n", index->data.strVal);
        executionFinished = 1;
    }
    
    

    return result;
}


void avm_tablesetelem(avm_table* table,avm_memcell* index,avm_memcell* content){
    unsigned int hash;
    avm_table_bucket* node = (avm_table_bucket*) malloc(sizeof(avm_table_bucket));
    
    if(index->type == number_m){
        hash = avm_table_numhash(index->data.numVal);
        avm_assign(&(node->key),index);
        avm_assign(&(node->value),content);
        if(table->numIndexed[hash]!= NULL){ 
           node->next = table->numIndexed[hash];
        }    
        else{
            node->next = NULL;
        }
        table->numIndexed[hash] = node;

    }
    else if(index->type == string_m){
        hash = avm_table_hash(index->data.strVal);
        avm_assign(&(node->key),index);
        avm_assign(&(node->value),content);
        if(table->strIndexed[hash]!= NULL){ 
           node->next = table->strIndexed[hash];
        }    
        else{
            node->next = NULL;
        }
        table->strIndexed[hash] = node;

    }
    
}



void execute_jge(instruction *instr)
{
    avm_memcell *rv1 = avm_translate_operand(&instr->arg1, &ax);
    avm_memcell *rv2 = avm_translate_operand(&instr->arg2, &bx);

    assert(rv1 && rv2);

    unsigned char result = 0;

    if (rv1->type == undef_m || rv2->type == undef_m)
    {
        printf("'undef' involved in equality!\n");
        executionFinished = 1;
    }
    else if (rv1->type == nil_m || rv2->type == nil_m)
    {
        printf("Cannot compare nil\n");
        executionFinished = 1;
    }
    else if (rv1->type == bool_m || rv2->type == bool_m)
    {
        printf("Cannot compare booleans\n");
        executionFinished = 1;
    }
    else if (rv1->type != rv2->type)
    {
        printf("%s <= %s is illegal!\n", typeStrings[rv1->type], typeStrings[rv2->type]);
        executionFinished = 1;
    }
    else
    {
        /* Equality check with dispatching */
        switch (rv1->type)
        {
        case string_m:
            printf("Cannot compare strings");
            executionFinished = 1;
            break;
        case number_m:
            result = (rv1->data.numVal >= rv2->data.numVal);
            break;
        case libfunc_m:
            /*code here*/
            break;
        case userfunc_m:
            /*code here*/
            break;
        case table_m:
            /*code here*/
            break;
        }
    }
    if (!executionFinished && result)
        pc = instr->srcline;
}

void execute_pusharg(instruction *i)
{
    avm_memcell *p = avm_translate_operand(&i->result, &ax);
    assert(p);
    avm_assign((stack + top), p);
    avm_dec_top();
    totalActuals++;
}

void execute_jlt(instruction *instr)
{
    avm_memcell *rv1 = avm_translate_operand(&instr->arg1, &ax);
    avm_memcell *rv2 = avm_translate_operand(&instr->arg2, &bx);

    assert(rv1 && rv2);

    unsigned char result = 0;

    if (rv1->type == undef_m || rv2->type == undef_m)
    {
        printf("'undef' involved in equality!\n");
        executionFinished = 1;
    }
    else if (rv1->type == nil_m || rv2->type == nil_m)
    {
        printf("Cannot compare nil\n");
        executionFinished = 1;
    }
    else if (rv1->type == bool_m || rv2->type == bool_m)
    {
        printf("Cannot compare booleans\n");
        executionFinished = 1;
    }
    else if (rv1->type != rv2->type)
    {
        printf("%s <= %s is illegal!\n", typeStrings[rv1->type], typeStrings[rv2->type]);
        executionFinished = 1;
    }
    else
    {
        /* Equality check with dispatching */
        switch (rv1->type)
        {
        case string_m:
            printf("Cannot compare strings");
            executionFinished = 1;
            break;
        case number_m:
            result = (rv1->data.numVal < rv2->data.numVal);
            break;
        case libfunc_m:
            /*code here*/
            break;
        case userfunc_m:
            /*code here*/
            break;
        case table_m:
            /*code here*/
            break;
        }
    }
    if (!executionFinished && result)
        pc = instr->srcline;
}

void execute_jgt(instruction *instr)
{
    avm_memcell *rv1 = avm_translate_operand(&instr->arg1, &ax);
    avm_memcell *rv2 = avm_translate_operand(&instr->arg2, &bx);

    assert(rv1 && rv2);

    unsigned char result = 0;

    if (rv1->type == undef_m || rv2->type == undef_m)
    {
        printf("'undef' involved in equality!\n");
        executionFinished = 1;
    }
    else if (rv1->type == nil_m || rv2->type == nil_m)
    {
        printf("Cannot compare nil\n");
        executionFinished = 1;
    }
    else if (rv1->type == bool_m || rv2->type == bool_m)
    {
        printf("Cannot compare booleans\n");
        executionFinished = 1;
    }
    else if (rv1->type != rv2->type)
    {
        printf("%s <= %s is illegal!\n", typeStrings[rv1->type], typeStrings[rv2->type]);
        executionFinished = 1;
    }
    else
    {
        /* Equality check with dispatching */
        switch (rv1->type)
        {
        case string_m:
            printf("Cannot compare strings");
            executionFinished = 1;
            break;
        case number_m:
            result = (rv1->data.numVal > rv2->data.numVal);
            break;
        case libfunc_m:
            /*code here*/
            break;
        case userfunc_m:
            /*code here*/
            break;
        case table_m:
            /*code here*/
            break;
        }
    }
    if (!executionFinished && result)
        pc = instr->srcline;
}

void avm_call_libfunc(unsigned i, instruction *in)
{
    if (strcmp(libfunc_vm[i].data.strVal, "print") == 0)
    {
        avm_print();
    }
    else if (strcmp(libfunc_vm[i].data.strVal, "typeof") == 0)
    {
        avm_typeof();
    }
    else if (strcmp(libfunc_vm[i].data.strVal, "totalarguments") == 0)
    {
        totalarguments();
    }
    else if (strcmp(libfunc_vm[i].data.strVal, "argument") == 0)
    {
        argument();
    }
    else if (strcmp(libfunc_vm[i].data.strVal, "sin") == 0)
    {
        sin_();
    }
    else if (strcmp(libfunc_vm[i].data.strVal, "cos") == 0)
    {
        cos_();
    }
    else if (strcmp(libfunc_vm[i].data.strVal, "sqrt") == 0)
    {
        sqrt_();
    }
    else if (strcmp(libfunc_vm[i].data.strVal, "strtonum")==0){
        strtonum();
    }
    else if(strcmp(libfunc_vm[i].data.strVal, "input")==0){
        input();
    }
    else
    {
        assert(NULL);
    }
    execute_funcexit(in);
}

void execute_call(instruction *i)
{
    avm_memcell *func = avm_translate_operand(&i->result, &ax);
    assert(func);

    avm_callsaveenvironment();
    totalActuals = 0;
    switch (func->type)
    {
    case userfunc_m:
        pc = user_func_instr[func->data.funcVal];
		//topsp = top;
		top -= user_func_locals[func->data.funcVal];
		break;
    case string_m:
        assert(NULL);
        break;
    case libfunc_m:
        avm_call_libfunc(func->data.funcVal, i);
        break;
    default:
        assert(NULL);
        break;
    }
}



void execute_tablegetelem(instruction* instr){ 
	avm_memcell* lv = avm_translate_operand(&instr->result,(avm_memcell*)0);
	avm_memcell* t = avm_translate_operand(&instr->arg1,(avm_memcell*)0);
	avm_memcell* i = avm_translate_operand(&instr->arg2,&ax);
	
	assert(lv);
	assert(t);
	assert(i);

	if(t->type != table_m) {
		printf("illegal use of type %s as table!\n" ,typeStrings[t->type]); executionFinished = 0;
    }    
	else{
		avm_memcell* content = avm_tablegetelem(t->data.tableVal, i);
		if(content)
			avm_assign(lv,content);
		else{
			/*char *ts = avm_tostring(t);
			char *is = avm_tostring(i);
			printf("%s[%s] not found!\n", ts ,is);
			free(ts);
			free(is);*/
			}    
	    }
}


void execute_tablesetelem(instruction *instr)
{
	avm_memcell* t = avm_translate_operand(&instr->result,(avm_memcell*)0);
	avm_memcell* i = avm_translate_operand(&instr->arg1,&ax);
	avm_memcell* c = avm_translate_operand(&instr->arg2,&bx);

	assert(i);
	assert(t && c);
	
	if(t->type!=table_m){
		printf("illegal use of type %s as table!\n" ,typeStrings[t->type]); executionFinished = 0;
    }
    else{
		avm_tablesetelem(t->data.tableVal, i,c);
    }
}



void execute_nop(instruction *i)
{
    avm_memcell *lv = avm_translate_operand(&i->result, &ax);
    avm_memcell *rv = avm_translate_operand(&i->arg1, &bx);
    assert(lv && rv);
    avm_assign(lv, rv);
}

void execute_funcenter(instruction *i)
{

}

void execute_funcexit(instruction *i)
{
    unsigned oldTop = top;
    top = avm_get_envvalue(topsp + 2);
    pc = avm_get_envvalue(topsp + 3);
    topsp = avm_get_envvalue(topsp + 1);

    while (++oldTop <= top) /*Intentionally ignoring first. */
        avm_memcellclear(&stack[oldTop]);
}

execute_func_t *executeFuncs[] = {
    execute_assign,
    execute_arithmetic,
    execute_arithmetic,
    execute_arithmetic,
    execute_arithmetic,
    execute_arithmetic,
    execute_nop, //uminus??
    execute_nop,
    execute_nop,
    execute_nop,
    execute_jeq,
    execute_jne,
    execute_jle,
    execute_jge,
    execute_jlt,
    execute_jgt,
    execute_call,
    execute_pusharg,
    execute_funcenter,
    execute_funcexit,
    execute_newtable,
    execute_tablegetelem,
    execute_tablesetelem,
    execute_nop,
    execute_jeq};

void execute_cycle()
{
    instruction *instr;
    unsigned oldPC;
    if (executionFinished)
        return;
    else if (pc == codeSize)
    {
        executionFinished = 1;
        return;
    }
    else
    {
        assert(pc < (codeSize + 1));
        instr = code_vm + pc;
        assert(
            instr->op >= 0 &&
            instr->op <= AVM_MAX_INSTRUCTIONS);
        /*if(instr->srcline)
		currLine=instr->srcline;*/
        oldPC = pc;
        (*executeFuncs[instr->op])(instr);
        if (pc == oldPC)
            ++pc;
    }
}

/*avm_assign(&stack[top] ,arg);
++totalActuals;
avm_dec_top();*/

void execute_code()
{
    assert(f);
    unsigned num_size, str_size, lib_size, f_size, c_size, mag_num, i;
    int t;
    char *temp_s;
    unsigned *user_len, *lib_len, *str_len;
    fread(&mag_num, sizeof(unsigned), 1, f);
    fread(&global_c, sizeof(int), 1, f);
    fread(&str_size, sizeof(unsigned), 1, f);

    i = 0;

    string_vm = malloc(str_size * sizeof(avm_memcell));
    // while (i < str_size)
    // {
    //     fread(&t, sizeof(int), 1, f);
    //     string_vm[i].type = string_m;
    //     string_vm[i].data.strVal = malloc(t);
    //     i++;
    // }
    // i = 0;
    // while (i < str_size)
    // {
    //     string_vm[i].type = string_m;
    //     fread(string_vm[i].data.strVal, sizeof(char), strlen(string_vm[i].data.strVal), f);
    //     i++;
    // }

    

    str_len = malloc(sizeof(unsigned) * str_size);
    fread(str_len, sizeof(unsigned), str_size, f);

    for (i = 0; i < str_size; i++)
    {
        string_vm[i].data.strVal = malloc(sizeof(char) * str_len[i]);
        string_vm[i].type = string_m;
        fread(string_vm[i].data.strVal, sizeof(char), str_len[i], f);
        //string_vm[i].data.strVal[str_len[i]]='\0';
    }

    fread(&num_size, sizeof(unsigned), 1, f);

    i = 0;

    num_vm = malloc(sizeof(avm_memcell) * num_size);
    while (i < num_size)
    {
        num_vm[i].type = number_m;

        fread(&num_vm[i].data.numVal, sizeof(double), 1, f);
        i++;
    }

    //f number
    fread(&f_size, sizeof(unsigned), 1, f);
    //lengths
    userfunc_vm = malloc(f_size * sizeof(avm_memcell));

    user_len = malloc(sizeof(unsigned) * f_size);
    fread(user_len, sizeof(unsigned), f_size, f);
    //names

    for (i = 0; i < f_size; i++)
    {
        userfunc_vm[i].data.strVal = malloc(sizeof(char) * user_len[i]);
        userfunc_vm[i].type = libraryfunc_e;
        fread(userfunc_vm[i].data.strVal, sizeof(char), user_len[i], f);
    }

    user_func_instr = malloc(sizeof(unsigned)* f_size);
    fread(user_func_instr, sizeof(unsigned), f_size, f);

    user_func_locals = malloc(sizeof(unsigned)* f_size);
    fread(user_func_locals, sizeof(unsigned), f_size, f);

    fread(&lib_size, sizeof(unsigned), 1, f);

    libfunc_vm = malloc(lib_size * sizeof(avm_memcell));

    lib_len = malloc(sizeof(unsigned) * lib_size);
    fread(lib_len, sizeof(unsigned), lib_size, f);

    for (i = 0; i < lib_size; i++)
    {
        libfunc_vm[i].data.strVal = malloc(sizeof(char) * lib_len[i]);
        libfunc_vm[i].type = libraryfunc_e;
        fread(libfunc_vm[i].data.strVal, sizeof(char), lib_len[i], f);
    }

    


    fread(&c_size, sizeof(unsigned), 1, f);

    code_vm = malloc(c_size * sizeof(instruction));
    fread(code_vm, sizeof(instruction), c_size, f);
    codeSize = c_size;

    stack = malloc(sizeof(avm_memcell) * STACK_SIZE);

    for (i = 0; i < STACK_SIZE; i++)
    {
        stack[i].data.stackVal = 0;
        stack[i].type = undef_m;
    }
    topsp = STACK_SIZE - 1;
    top = STACK_SIZE - global_c - 1;
    printf("\n");
    while (executionFinished == 0)
        execute_cycle();
    printf("\n");
}

static void avm_initstack(void) {
	for(unsigned i=0; i<AVM_STACKSIZE; ++i){
		AVM_WIPEOUT(stack[i]); 
		stack[i].type = undef_m;
	}
}

/*extern void memclear_string (avm_memcell* m){
	assert(m->data.tableVal);
	avm_tabledecrefcounter(m->data.tableVal);
}*/

void avm_tablebucketsdestroy(avm_table_bucket** p){
	unsigned i;
	avm_table_bucket *del,*b;
	for (i = 0; i < AVM_TABLE_HASHSIZE; ++i, ++p)
	{
		for(b=*p;b;){
 			del=b;
 			b=b->next;
 			avm_memcellclear(&del->key);
 			avm_memcellclear(&del->value);
 			free(del);
	     }
		p[i]=(avm_table_bucket*)0;
 	}
 }

void avm_tabledestroy(avm_table *t) {
 	avm_tablebucketsdestroy(t->strIndexed);
 	avm_tablebucketsdestroy(t->numIndexed);
 	avm_tablebucketsdestroy(t->boolIndexed);
 	avm_tablebucketsdestroy(t->libfuncIndexed);
 	avm_tablebucketsdestroy(t->userfuncIndexed);
 	free(t);
}

int main(){
    f=fopen("bin.abc", "rb");
    
    execute_code();
}