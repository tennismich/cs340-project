#ifndef STACK_H_
#define STACK_H_

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

struct L_Binding
{
	int value;
	struct L_Binding *next;
} typedef L_Binding;
struct S_Binding
{
	int value;
	char *s;
	L_Binding *list;
	struct S_Binding *next;
} typedef S_Binding;

typedef struct stack_Head
{
	S_Binding *head;
} stack_Head;

stack_Head *New_Stack();

int push(stack_Head* head, int value);

int pop(stack_Head* head);

void add_to_l(stack_Head *s_head, int value);

int push_l(stack_Head* head, char *function);

L_Binding *pop_l(stack_Head* head, char **function);

#endif
